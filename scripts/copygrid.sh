#!/bin/bash

# supply new grid name
if [ "$1" == "" ] ; then
  echo "Usage $0 <new grid name> [old grid name]"
  exit 1
fi

# old grid name
oldgrid=$2
if [ "$oldgrid" == "" ] ; then
  oldgrid=idgrid
fi

# check that we are in the correct directory
if ! test -d $oldgrid ; then
  echo "Change directory to dune-metagrid/dune/grid and run again"
  exit 1
fi

# make camel case name
camelCase()
{
  first=${1:0:1}
  FIRST=`echo $first | tr '[:lower:]' '[:upper:]'`
  MyGrid="$FIRST"`echo ${1:1}`
  ResultGrid=`echo ${MyGrid/grid/Grid}`
  echo $ResultGrid
}

# make upper lower and Camel case names
oldgrid=`echo $oldgrid | tr '[:upper:]' '[:lower:]'`
OLDGRID=`echo $oldgrid | tr '[:lower:]' '[:upper:]'`
OldGrid=`(camelCase "$oldgrid")`

# make upper lower and Camel case names
newgrid=`echo $1 | tr '[:upper:]' '[:lower:]'`
NEWGRID=`echo $1 | tr '[:lower:]' '[:upper:]'`
NewGrid=`(camelCase "$newgrid")`

# make new directory
mkdir $newgrid

# copy header
cp $oldgrid".hh" $newgrid".hh"
cp $oldgrid/*.hh $newgrid/
cp $oldgrid/CMakeLists.txt $newgrid/
cp "test/test-"$oldgrid".cc" "test/test-"$newgrid".cc"

# replace IdGrid with the appropriate names
find . -name $newgrid".hh" -exec sed -i -e "s@$oldgrid@$newgrid@g" {} \;
find . -name $newgrid".hh" -exec sed -i -e "s@$OldGrid@$NewGrid@g" {} \;
find . -name $newgrid".hh" -exec sed -i -e "s@$OLDGRID@$NEWGRID@g" {} \;
find $newgrid/ -name "*.hh" -exec sed -i -e "s@$oldgrid@$newgrid@g" {} \;
find $newgrid/ -name "*.hh" -exec sed -i -e "s@$OldGrid@$NewGrid@g" {} \;
find $newgrid/ -name "*.hh" -exec sed -i -e "s@$OLDGRID@$NEWGRID@g" {} \;
find $newgrid/ -name "*.am" -exec sed -i -e "s@$oldgrid@$newgrid@g" {} \;
find $newgrid/ -name "*.am" -exec sed -i -e "s@$OldGrid@$NewGrid@g" {} \;
find $newgrid/ -name "*.am" -exec sed -i -e "s@$OLDGRID@$NEWGRID@g" {} \;
find $newgrid/ -name "*.txt" -exec sed -i -e "s@$oldgrid@$newgrid@g" {} \;
find $newgrid/ -name "*.txt" -exec sed -i -e "s@$OldGrid@$NewGrid@g" {} \;
find $newgrid/ -name "*.txt" -exec sed -i -e "s@$OLDGRID@$NEWGRID@g" {} \;
find "test/" -name "test-"$newgrid".cc" -exec sed -i -e "s@$oldgrid@$newgrid@g" {} \;
find "test/" -name "test-"$newgrid".cc" -exec sed -i -e "s@$OldGrid@$NewGrid@g" {} \;
find "test/" -name "test-"$newgrid".cc" -exec sed -i -e "s@$OLDGRID@$NEWGRID@g" {} \;
