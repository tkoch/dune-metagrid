dune_define_gridtype(METAMETAGRID_CONFIG_H_BOTTOM
                     GRIDTYPE MULTISPGRID
                     ASSERTION "GRIDDIM == WORLDDIM"
                     DUNETYPE "Dune::MultiSPGrid< double, dimgrid >"
                     HEADERS "dune/grid/multispgrid/grid.hh" "dune/grid/multispgrid/dgfparser.hh")
dune_define_gridtype(METAGRID_CONFIG_H_BOTTOM
                     GRIDTYPE PARALLELGRID_SPGRID
                     DUNETYPE "Dune::ParallelGrid< Dune::SPGrid< double, dimgrid, SPIsotropicRefinement, No_Comm > >"
                     HEADERS "dune/grid/spgrid/dgfparser.hh" "dune/grid/parallelgrid/dgfparser.hh")
dune_define_gridtype(METAGRID_CONFIG_H_BOTTOM
                     GRIDTYPE "CACHEITGRID_PARALLELGRID_SPGRID"
                     ASSERTION "GRIDDIM == WORLDDIM"
                     DUNETYPE "Dune::CacheItGrid< Dune::ParallelGrid< Dune::SPGrid< double, dimgrid, SPIsotropicRefinement, No_Comm > > >"
                     HEADERS "dune/grid/spgrid/dgfparser.hh" "dune/grid/parallelgrid/dgfparser.hh" "dune/grid/cacheitgrid/dgfparser.hh")
dune_define_gridtype(METAGRID_CONFIG_H_BOTTOM
                     GRIDTYPE PARALLELGRID_ALUGRID_CUBE
                     DUNETYPE "Dune::ParallelGrid< Dune::ALUGrid< dimgrid, dimworld, cube, nonconforming, No_Comm > >"
                     HEADERS "dune/alugrid/dgf.hh" "dune/grid/parallelgrid/dgfparser.hh")
dune_define_gridtype(METAGRID_CONFIG_H_BOTTOM
                     GRIDTYPE PARALLELGRID_ALUGRID_SIMPLEX
                     DUNETYPE "Dune::ParallelGrid< Dune::ALUGrid< dimgrid, dimworld, simplex, nonconforming, No_Comm > >"
                     HEADERS "dune/alugrid/dgf.hh" "dune/grid/parallelgrid/dgfparser.hh")
dune_define_gridtype(METAGRID_CONFIG_H_BOTTOM
                     GRIDTYPE PARALLELGRID_ALUGRID_CONFORM
                     DUNETYPE "Dune::ParallelGrid< Dune::ALUGrid< dimgrid, dimworld, simplex, conforming, No_Comm > >"
                     HEADERS "dune/alugrid/dgf.hh" "dune/grid/parallelgrid/dgfparser.hh")
dune_define_gridtype(METAGRID_CONFIG_H_BOTTOM
                     GRIDTYPE PARALLELGRID_ALBERTAGRID
                     DUNETYPE "Dune::ParallelGrid< Dune::AlbertaGrid< dimgrid, dimworld > >"
                     HEADERS "dune/grid/io/file/dgfparser/dgfalberta.hh" "dune/grid/parallelgrid/dgfparser.hh")
dune_define_gridtype(METAGRID_CONFIG_H_BOTTOM
                     GRIDTYPE PARALLELGRID_CPGRID
                     ASSERTION "GRIDDIM == WORLDDIM && GRIDDIM == 3"
                     DUNETYPE "Dune::ParallelGrid< Dune::CpGrid >"
                     HEADERS "dune/grid/cpgrid/dgfparser.hh" "dune/grid/parallelgrid/dgfparser.hh")
dune_define_gridtype(METAGRID_CONFIG_H_BOTTOM
                     GRIDTYPE CARTESIANGRID_ALUGRID_CUBE
                     ASSERTION "GRIDDIM == WORLDDIM"
                     DUNETYPE "Dune::CartesianGrid< Dune::ALUGrid< dimgrid, dimworld, cube, nonconforming > >"
                     HEADERS "dune/alugrid/dgf.hh" "dune/grid/cartesiangrid/dgfparser.hh")
dune_define_gridtype(METAGRID_CONFIG_H_BOTTOM
                     GRIDTYPE CARTESIANGRID_UGGRID
                     ASSERTION "GRIDDIM == WORLDDIM"
                     DUNETYPE "Dune::CartesianGrid< Dune::UGGrid< dimgrid > >"
                     HEADERS "dune/grid/io/file/dgfparser/dgfug.hh" "dune/grid/cartesiangrid/dgfparser.hh")
