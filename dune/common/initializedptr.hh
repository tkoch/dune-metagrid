#ifndef DUNE_COMMON_INITIALIZEDPOINTER_HH
#define DUNE_COMMON_INITIALIZEDPOINTER_HH

#include <dune/common/nullptr.hh>
#include <dune/common/typetraits.hh>

namespace Dune
{

  // InitializedPtr
  // --------------

  template< class T >
  class InitializedPtr
  {
    template< class > friend class InitializedPtr;

  public:
    InitializedPtr ( T *ptr = nullptr ) : ptr_( ptr ) {}

    template< class U >
    InitializedPtr ( const InitializedPtr< U > &other )
    : ptr_( static_cast< T * >( other.ptr_ ) )
    {}

    operator bool () const { return bool( ptr_ ); }
    operator T * () const { return ptr_; }

    T &operator* () const { return *ptr_; }
    T *operator-> () const { return ptr_; }

    template< class U >
    bool operator== ( const InitializedPtr< U > &other ) const { return (ptr_ == other.ptr_); }

    template< class U >
    bool operator!= ( const InitializedPtr< U > &other ) const { return (ptr_ != other.ptr_); }

  private:
    T *ptr_;
  };

} // namespace Dune

#endif // #ifndef DUNE_COMMON_INITIALIZEDPOINTER_HH
