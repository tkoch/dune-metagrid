#ifndef DUNE_MULTISPGRID_IDSET_HH
#define DUNE_MULTISPGRID_IDSET_HH

#include <cassert>

#include <dune/common/tuples.hh>
#include <dune/common/typetraits.hh>

#include <dune/grid/common/indexidset.hh>

namespace Dune
{

  // Forward declarations
  // --------------------

  template< class > class MultiSPGlobalIdSet;
  template< class > class MultiSPLocalIdSet;



  // MultiSPIdSet
  // ------------

  /** \brief Base class for id sets
   *
   *  \ingroup IndexIdSets
   */
  template< class Traits >
  struct MultiSPIdSet
  : public Dune::IdSet< const typename Traits::Grid, MultiSPIdSet< Traits >, typename Traits::IdType >

  {
    // this type
    typedef MultiSPIdSet< Traits > This;
    // base type
    typedef Dune::IdSet< const typename Traits::Grid, This, typename Traits::IdType > Base;

  public:
    //! \brief grid type
    typedef typename Traits::Grid Grid;
    //! \brief id type
    typedef typename Base::IdType IdType;

    //! \brief entity type
    template< int codim >
    struct Codim
    {
      typedef typename Grid::template Codim< codim >::Entity Entity;
    };

    //! \brief host id set type
    typedef typename Traits::HostIdSet HostIdSet;

    //! \brief constructor
    explicit MultiSPIdSet ( const Grid &grid )
    : grid_( grid )
    {}

    //! \brief return id
    template< class Entity >
    IdType id ( const Entity &entity ) const
    {
      return id< Entity::codimension >( entity );
    }

    //! \brief return id
    template< int codim >
    IdType id ( const typename Codim< codim >::Entity &entity ) const
    {
      const int node = entity.impl().node();
      const typename HostIdSet::IdType hostId = asImp().hostIdSet( node ).id( entity.impl().hostEntity() );
      return IdType( node, hostId );
    }

    //! \brief return sub id
    template< class Entity >
    IdType subId ( const Entity &entity, int i, unsigned int codim ) const
    {
      return subId< Entity::codimension >( entity, i, codim );
    }

    //! \brief return sub id
    template< int cd >
    IdType subId ( const typename Codim< cd >::Entity &entity, int i, unsigned int codim ) const
    {
      const int node = entity.impl().node();
      const typename HostIdSet::IdType hostId = asImp().hostIdSet( node ).template subId( entity.impl().hostEntity(), i, codim );
      return IdType( node, hostId );
    }

    //! \brief return reference to grid
    const Grid &grid () const
    {
      return grid_;
    }

  private:
    typename Traits::Implementation &asImp ()
    {
      return static_cast< typename Traits::Implementation & >( *this );
    }

    const typename Traits::Implementation &asImp () const
    {
      return static_cast< const typename Traits::Implementation & >( *this );
    }

    const Grid &grid_;
  };



  // MultiSPGlobalIdSetTraits
  // ------------------------

  template< class GridImp >
  class MultiSPGlobalIdSetTraits
  {
    // grid traits
    typedef typename Dune::remove_const< GridImp >::type::Traits Traits;

  public:
    //! \brief grid type
    typedef typename Traits::Grid Grid;

    //! \brief implementation type
    typedef MultiSPGlobalIdSet< const Grid > Implementation;

    //! \brief host grid type
    typedef typename Traits::HostGrid HostGrid;
    //! \brief host id set type
    typedef typename HostGrid::GlobalIdSet HostIdSet;

    //! \brief id type
    typedef Dune::tuple< int, typename HostIdSet::IdType > IdType;
  };



  // MultiSPGlobalIdSet
  // ------------------

  /** \brief Global id sets
   *
   *  \ingroup IndexIdSets
   */
  template< class Grid >
  class MultiSPGlobalIdSet
  : public MultiSPIdSet< MultiSPGlobalIdSetTraits< Grid > >
  {
    // this type
    typedef MultiSPGlobalIdSet< Grid > This;
    // base type
    typedef MultiSPIdSet< MultiSPGlobalIdSetTraits< Grid > > Base;

  public:
    using Base::grid;

    //! \brief constructor
    explicit MultiSPGlobalIdSet ( const typename Base::Grid &grid )
    : Base( grid )
    {}

    //! \brief return host id set
    const typename Base::HostIdSet &hostIdSet ( int node ) const
    {
      return grid().hostGrid( node ).globalIdSet();
    }
  };



  // MultiSPLocalIdSetTraits
  // -----------------------

  template< class GridImp >
  class MultiSPLocalIdSetTraits
  {
    // grid traits
    typedef typename Dune::remove_const< GridImp >::type::Traits Traits;

  public:
    //! \brief grid type
    typedef typename Traits::Grid Grid;

    //! \brief implementation type
    typedef MultiSPLocalIdSet< const Grid > Implementation;

    //! \brief host grid type
    typedef typename Traits::HostGrid HostGrid;
    //! \brief host id set type
    typedef typename HostGrid::LocalIdSet HostIdSet;

    //! \brief id type
    typedef Dune::tuple< int, typename HostIdSet::IdType > IdType;
  };



  // MultiSPLocalIdSet
  // ------------------

  /** \brief Local id sets
   *
   *  \ingroup IndexIdSets
   */
  template< class Grid >
  class MultiSPLocalIdSet
  : public MultiSPIdSet< MultiSPLocalIdSetTraits< Grid > >
  {
    // this type
    typedef MultiSPLocalIdSet< Grid > This;
    // base type
    typedef MultiSPIdSet< MultiSPLocalIdSetTraits< Grid > > Base;

  public:
    using Base::grid;

    //! \brief constructor
    explicit MultiSPLocalIdSet ( const typename Base::Grid &grid )
    : Base( grid )
    {}

    //! \brief return host id set
    const typename Base::HostIdSet &hostIdSet ( int node ) const
    {
      return grid().hostGrid( node ).localIdSet();
    }
  };

}  // namespace Dune

#endif // #ifndef DUNE_MULTISPGRID_IDSET_HH
