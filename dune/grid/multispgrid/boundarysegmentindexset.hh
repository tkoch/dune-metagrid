#ifndef DUNE_MULTISPGRID_BOUNDARYSEGMENTINDEXSET_HH
#define DUNE_MULTISPGRID_BOUNDARYSEGMENTINDEXSET_HH

#include <cassert>
#include <limits>
#include <vector>

#include <dune/common/exceptions.hh>
#include <dune/common/typetraits.hh>

#include <dune/grid/common/intersection.hh>

#include "intersection.hh"

namespace Dune
{

  // Forward declaration
  // -------------------

  template< class > class MultiSPIntersection;



  // MultiSPBoundarySegmentIndexSet
  // ------------------------------

  template< class Grid >
  class MultiSPBoundarySegmentIndexSet
  {
    // this type
    typedef MultiSPBoundarySegmentIndexSet< Grid > This;
    // grid traits
    typedef typename remove_const< Grid >::type::Traits Traits;
    // host grid type
    typedef typename Traits::HostGrid HostGrid;

  public:
    //! \brief index type
    typedef size_t IndexType;

    //! \brief empty constructor
    explicit MultiSPBoundarySegmentIndexSet ()
    : indices_( 0 ),
      size_( 0 )
    {}

    //! \brief constructor
    explicit MultiSPBoundarySegmentIndexSet ( const Grid &grid )
    : indices_( grid.nodes() )
    {
      // get number of nodes
      const size_t nodes = grid.nodes();

      // index counter
      IndexType newIndex = 0;

      // iterate over all host (macro) grids
      for( size_t node = 0; node < nodes; ++node )
      {
        // get host grid and resize index vector
        const HostGrid &hostGrid = grid.hostGrid( node );
        indices_[ node ].resize( hostGrid.numBoundarySegments(), invalidIndex() );

        // get macro grid view
        const int level = 0;
        typedef typename HostGrid::LevelGridView HostGridView;
        HostGridView hostGridView = hostGrid.levelGridView( level );

        // get boundary segment iterator
        typedef typename HostGridView::Implementation::BoundarySegmentIterator BoundarySegmentIterator;
        const BoundarySegmentIterator bend = hostGridView.impl().boundarySegmentEnd();
        for( BoundarySegmentIterator bit = hostGridView.impl().boundarySegmentBegin(); bit != bend; ++bit )
        {
          // get boundary intersection
          typedef typename BoundarySegmentIterator::Intersection Intersection;
          const Intersection &intersection = *bit;
          assert( intersection.boundary() );

          // check if we have no neighboring node
          const int indexInInside = intersection.indexInInside();
          if( grid.macroConnectivity().boundary( indexInInside, node ) )
          {
            // get boundary segment index and assign new index
            IndexType index = intersection.boundarySegmentIndex();
            indices_[ node ][ index ] = newIndex++;
          }
        }
      }
      // we have counted the number of boundary segments at the same time
      size_ = newIndex;

      // we don't support empty grids
      if( size_ == 0 )
        DUNE_THROW( InvalidStateException, "No boundary intersections found" );
    }

    //! \brief number of boundary segments
    IndexType size () const
    {
      return size_;
    }

    //! \brief boundary segment index
    template< class GridImp, class IntersectionImp >
    IndexType index ( const Dune::Intersection< GridImp, IntersectionImp > &intersection )
    {
      return index( intersection.impl() );
    }

    //! \brief boundary segment index
    template< class Traits >
    IndexType index ( const MultiSPIntersection< Traits > &intersection ) const
    {
      const int inner = MultiSPIntersection< Traits >::inner;
      assert( contains( intersection ) );
      const int node = intersection.node( inner );
      const IndexType index = intersection.hostIntersection( inner ).boundarySegmentIndex();
      return indices_[ node ][ index ];
    }

    //! \brief return true if intersection is on boundary
    template< class GridImp, class IntersectionImp >
    bool contains ( const Dune::Intersection< GridImp, IntersectionImp > &intersection )
    {
      return contains( intersection.impl() );
    }

    //! \brief return true if intersection is on boundary
    template< class Traits >
    bool contains ( const MultiSPIntersection< Traits > &intersection ) const
    {
      typedef typename MultiSPIntersection< Traits >::HostIntersection HostIntersection;
      const int inner = MultiSPIntersection< Traits >::inner;
      const HostIntersection &hostIntersection = intersection.hostIntersection( inner );
      const int node = intersection.node( inner );
      return contains( hostIntersection, node );
    }

    //! \brief return true if host intersection with boundary segment index is on boundary
    template< class HostIntersection >
    bool contains ( const HostIntersection &hostIntersection, const int node ) const
    {
      if( !hostIntersection.boundary() )
        return false;
      const IndexType hostBndSegIndex = hostIntersection.boundarySegmentIndex();
      return ( indices_[ node ][ hostBndSegIndex ] != invalidIndex() );
    }

  private:
    // return invalid index
    IndexType invalidIndex () const
    {
      return std::numeric_limits< IndexType >::max();
    }

    std::vector< std::vector< IndexType > > indices_;
    IndexType size_;
  };

} // namespace Dune

#endif // #ifndef DUNE_MULTISPGRID_BOUNDARYSEGMENTINDEXSET_HH
