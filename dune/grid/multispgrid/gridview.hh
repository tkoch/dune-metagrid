#ifndef DUNE_MULTISPGRID_GRIDVIEW_HH
#define DUNE_MULTISPGRID_GRIDVIEW_HH

#include <cassert>
#include <vector>

#include <dune/common/nullptr.hh>
#include <dune/common/typetraits.hh>

#include <dune/grid/common/gridview.hh>

namespace Dune
{

  // Forward declarations
  // --------------------

  template< PartitionIteratorType, class > class MultiSPLeafGridView;
  template< PartitionIteratorType, class > class MultiSPLevelGridView;



  // MultiSPGridView
  // ---------------

  /** \brief Base class for grid views
   *
   *  \ingroup GridView
   */
  template< class ViewTraits >
  class MultiSPGridView
  {
    // this type
    typedef MultiSPGridView< ViewTraits > This;

  public:
    //! \brief grid type
    typedef typename ViewTraits::Grid Grid;
    //! \brief collective communication type
    typedef typename ViewTraits::CollectiveCommunication CollectiveCommunication;

    //! \brief constructor
    explicit MultiSPGridView ( const Grid &grid )
    : grid_( &grid )
    {}

    //! \brief return const reference to grid
    const Grid &grid () const
    {
      assert( grid_ );
      return *grid_;
    }

    //! \brief obtain collective communication object
    const CollectiveCommunication &comm () const
    {
      return grid().comm();
    }

  private:
    const Grid *grid_;
  };



  // MultiSPLeafGridViewTraits
  // -------------------------

  template< PartitionIteratorType pitype, class GridImp >
  class MultiSPLeafGridViewTraits
  {
    // grid traits
    typedef typename remove_const< GridImp >::type::Traits Traits;

  public:
    //! \brief grid view type
    typedef MultiSPLeafGridView< pitype, GridImp > GridViewImp;

    //! \brief grid type
    typedef typename remove_const< GridImp >::type Grid;
    //! \brief host grid type
    typedef typename Traits::HostGrid HostGrid;

    //! \brief index set
    typedef typename Traits::LeafIndexSet IndexSet;
    //! \brief intersection
    typedef typename Traits::LeafIntersection Intersection;
    //! \brief intersection iterator type
    typedef typename Traits::LeafIntersectionIterator IntersectionIterator;

    //! \brief collective communication type
    typedef typename Traits::CollectiveCommunication CollectiveCommunication;

    template< int codim >
    struct Codim
    {
      //! \brief iterator type
      typedef typename Traits::template Codim< codim >::template Partition< pitype >::LeafIterator Iterator;

      //! \brief entity type
      typedef typename Traits::template Codim< codim >::Entity Entity;
      //! \brief entity pointer type
      typedef typename Traits::template Codim< codim >::EntityPointer EntityPointer;

      //! \brief geometry type
      typedef typename Traits::template Codim< codim >::Geometry Geometry;
      //! \brief local geometry type
      typedef typename Traits::template Codim< codim >::LocalGeometry LocalGeometry;

      template <PartitionIteratorType pit >
      struct Partition
      {
        //! \brief iterator type
        typedef typename Traits::template Codim< codim >::template Partition< pit >::LeafIterator Iterator;
      };
    };

    static const bool conforming = false;

    template< PartitionIteratorType pit >
    struct Partition
    {
      //! \brief host grid view type
      typedef typename HostGrid::template Partition< pit >::LeafGridView HostGridView;
      //! \brief leaf connectivity
      typedef typename Traits::template Partition< pit >::LeafConnectivity Connectivity;
    };
    //! \brief host grid view type
    typedef typename Partition< pitype >::HostGridView HostGridView;
    //! \brief leaf connectivity
    typedef typename Partition< pitype >::Connectivity Connectivity;
  };




  // MultiSPLeafGridView
  // -------------------

  /** \brief Leaf grid views
   *
   *  \ingroup GridView
   */
  template< PartitionIteratorType pitype, class Grid >
  class MultiSPLeafGridView
  : public MultiSPGridView< MultiSPLeafGridViewTraits< pitype, Grid > >
  {
    // this type
    typedef MultiSPLeafGridView< pitype, Grid > This;
    // vies traits
    typedef MultiSPLeafGridViewTraits< pitype, Grid > Traits;
    // base type
    typedef MultiSPGridView< Traits > Base;

  public:
    //! \brief host grid type
    typedef typename Traits::HostGrid HostGrid;

    //! \brief index set
    typedef typename Traits::IndexSet IndexSet;
    //! \brief intersection
    typedef typename Traits::Intersection Intersection;
    //! \brief intersection iterator type
    typedef typename Traits::IntersectionIterator IntersectionIterator;

    template< int codim >
    struct Codim
    : public Traits::template Codim< codim >
    {};

    template< PartitionIteratorType pit >
    struct Partition
    : public Traits::template Partition< pit >
    {};

    //! \brief host grid view
    typedef typename Traits::HostGridView HostGridView;
    //! \brief connectivity
    typedef typename Traits::Connectivity Connectivity;

  public:
    using Base::grid;

    //! \brief constructor
    explicit MultiSPLeafGridView ( const Grid &grid )
    : Base( grid )
    {}

    //! \brief return index set
    const IndexSet &indexSet () const
    {
      return grid().leafIndexSet();
    }

    //! \brief return number of entities for given codimension in grid view
    int size ( const int codim ) const
    {
      return grid().size( codim );
    }

    //! \brief return number of entities for given geometry type in grid view
    int size ( const GeometryType &type ) const
    {
      return grid().size( type );
    }

    //! \brief return begin iterator for this view
    template< int codim >
    typename Codim< codim >::Iterator begin () const
    {
      return begin< codim, pitype >();
    }

    //! \brief return begin iterator for this view
    template< int codim, PartitionIteratorType pit >
    typename Codim< codim >::template Partition< pit >::Iterator begin () const
    {
      return grid().template leafbegin< codim, pit >();
    }

    //! \brief return end iterator for this view
    template< int codim >
    typename Codim< codim >::Iterator end () const
    {
      return end< codim, pitype >();
    }

    //! \brief return end iterator for this view
    template< int codim, PartitionIteratorType pit >
    typename Codim< codim >::template Partition< pit >::Iterator end () const
    {
      return grid().template leafend< codim, pit >();
    }

    //! \brief obtain begin intersection iterator with respect to this view
    IntersectionIterator ibegin ( const typename Traits::template Codim< 0 >::Entity &entity ) const
    {
      if( !entity.isLeaf() )
        return iend( entity );
      typedef typename IntersectionIterator::Implementation IteratorImp;
      return IteratorImp::begin( entity.impl() );
    }

    //! \brief obtain end intersection iterator with respect to this view
    IntersectionIterator iend ( const typename Traits::template Codim< 0 >::Entity &entity ) const
    {
      typedef typename IntersectionIterator::Implementation IteratorImp;
      return IteratorImp::end( entity.impl() );
    }

    //! \brief return size of the overlap region for a given codim on the grid view
    int overlapSize ( int codim ) const
    {
      return grid().overlapSize( codim );
    }

    //! \brief return size of the ghost region for a given codim on the grid view
    int ghostSize ( int codim ) const
    {
      return grid().ghostSize( codim );
    }

    //! \brief communicate data on this view
    template< class DataHandleImp, class DataType >
    void communicate ( CommDataHandleIF< DataHandleImp, DataType > &data,
                       InterfaceType iftype,
                       CommunicationDirection dir ) const
    {
      return grid().communicate( data, iftype, dir );
    }

    //! \brief return connectivity
    template< PartitionType pit >
    typename Partition< pit >::Connectivity connectivity () const
    {
      return grid().template leafConnectivity< pit >();
    }

    //! \brief return connectivity
    Connectivity connectivity () const
    {
      return connectivity< pitype >();
    }

    //! \brief return host grid view
    template< PartitionType pit >
    const typename Partition< pit >::HostGridView hostGridView ( const int node ) const
    {
      assert( connectivity< pit >().contains( node ) );
      return connectivity< pit >().hostGrid( node );
    }

    //! \brief return host grid view
    const HostGridView hostGridView ( const int node ) const
    {
      return hostGridView< pitype >( node );
    }
  };



  // MultiSPLevelGridViewTraits
  // --------------------------

  template< PartitionIteratorType pitype, class GridImp >
  class MultiSPLevelGridViewTraits
  {
    // grid traits
    typedef typename remove_const< GridImp >::type::Traits Traits;

  public:
    //! \brief grid view type
    typedef MultiSPLevelGridView< pitype, GridImp > GridViewImp;

    // \brief grid type
    typedef typename remove_const< GridImp >::type Grid;
    //! \brief host grid type
    typedef typename Traits::HostGrid HostGrid;

    //! \brief index set
    typedef typename Traits::LevelIndexSet IndexSet;
    //! \brief intersection
    typedef typename Traits::LevelIntersection Intersection;
    //! \brief intersection iterator type
    typedef typename Traits::LevelIntersectionIterator IntersectionIterator;

    //! \brief collective communication type
    typedef typename Traits::CollectiveCommunication CollectiveCommunication;

    template< int codim >
    struct Codim
    {
      //! \brief iterator type
      typedef typename Traits::template Codim< codim >::template Partition< pitype >::LevelIterator Iterator;

      //! \brief entity type
      typedef typename Traits::template Codim< codim >::Entity Entity;
      //! \brief entity pointer type
      typedef typename Traits::template Codim< codim >::EntityPointer EntityPointer;

      //! \brief geometry type
      typedef typename Traits::template Codim< codim >::Geometry Geometry;
      //! \brief local geometry type
      typedef typename Traits::template Codim< codim >::LocalGeometry LocalGeometry;

      template <PartitionIteratorType pit >
      struct Partition
      {
        //! \brief iterator type
        typedef typename Traits::template Codim< codim >::template Partition< pit >::LevelIterator Iterator;
      };
    };

    static const bool conforming = false;

    template< PartitionIteratorType pit >
    struct Partition
    {
      //! \brief host grid view type
      typedef typename HostGrid::template Partition< pit >::LevelGridView HostGridView;
      //! \brief connectivity
      typedef typename Traits::template Partition< pit >::LevelConnectivity Connectivity;
    };
    //! \brief host grid view type
    typedef typename Partition< pitype >::HostGridView HostGridView;
    //! \brief connectivity
    typedef typename Partition< pitype >::Connectivity Connectivity;
  };



  // MultiSPLevelGridView
  // --------------------

  /** \brief Level grid views
   *
   *  \ingroup GridView
   */
  template< PartitionIteratorType pitype, class Grid >
  class MultiSPLevelGridView
  : public MultiSPGridView< MultiSPLevelGridViewTraits< pitype, Grid > >
  {
    // this type
    typedef MultiSPLevelGridView< pitype, Grid > This;
    // view traits
    typedef MultiSPLevelGridViewTraits< pitype, Grid > Traits;
    // base type
    typedef MultiSPGridView< Traits > Base;

  public:
    //! \brief host grid type
    typedef typename Traits::HostGrid HostGrid;

    //! \brief index set
    typedef typename Traits::IndexSet IndexSet;
    //! \brief intersection
    typedef typename Traits::Intersection Intersection;
    //! \brief intersection iterator type
    typedef typename Traits::IntersectionIterator IntersectionIterator;

    template< int codim >
    struct Codim
    : public Traits::template Codim< codim >
    {};

    template< PartitionIteratorType pit >
    struct Partition
    : public Traits::template Partition< pit >
    {};

    //! \brief host grid view
    typedef typename Traits::HostGridView HostGridView;
    //! \brief connectivity
    typedef typename Traits::Connectivity Connectivity;

  public:
    using Base::grid;

    //! \brief constructor
    MultiSPLevelGridView ( int level, const Grid &grid )
    : Base( grid ),
      level_( level )
    {}

    //! \brief return index set
    const IndexSet &indexSet () const
    {
      return grid().levelIndexSet( level_ );
    }

    //! \brief return number of entities for given codimension in grid view
    int size ( const int codim ) const
    {
      return grid().size( level_, codim );
    }

    //! \brief return number of entities for given geometry type in grid view
    int size ( const GeometryType &type ) const
    {
      return grid().size( level_, type );
    }

    //! \brief return begin iterator for this view
    template< int codim >
    typename Codim< codim >::Iterator begin () const
    {
      return begin< codim, pitype >();
    }

    //! \brief return begin iterator for this view
    template< int codim, PartitionIteratorType pit >
    typename Codim< codim >::template Partition< pit >::Iterator begin () const
    {
      return grid().template lbegin< codim, pit >( level_ );
    }

    //! \brief return end iterator for this view
    template< int codim >
    typename Codim< codim >::Iterator end () const
    {
      return end< codim, pitype >();
    }

    //! \brief return end iterator for this view
    template< int codim, PartitionIteratorType pit >
    typename Codim< codim >::template Partition< pit >::Iterator end () const
    {
      return grid().template lend< codim, pit >( level_ );
    }

    //! \brief obtain begin intersection iterator with respect to this view
    IntersectionIterator ibegin ( const typename Traits::template Codim< 0 >::Entity &entity ) const
    {
      typedef typename IntersectionIterator::Implementation IteratorImp;
      return IteratorImp::begin( entity.impl() );
    }

    //! \brief obtain end intersection iterator with respect to this view
    IntersectionIterator iend ( const typename Traits::template Codim< 0 >::Entity &entity ) const
    {
      typedef typename IntersectionIterator::Implementation IteratorImp;
      return IteratorImp::end( entity.impl() );
    }

    //! \brief return size of the overlap region for a given codim on the grid view
    int overlapSize ( int codim ) const
    {
      return grid().overlapSize( codim );
    }

    //! \brief return size of the ghost region for a given codim on the grid view
    int ghostSize ( int codim ) const
    {
      return grid().ghostSize( level_, codim );
    }

    //! \brief communicate data on this view
    template< class DataHandleImp, class DataType >
    void communicate ( CommDataHandleIF< DataHandleImp, DataType > &data,
                       InterfaceType iftype,
                       CommunicationDirection dir ) const
    {
      return grid().communicate( data, iftype, dir, level_ );
    }

    //! \brief return connectivity
    template< PartitionType pit >
    typename Partition< pit >::Connectivity connectivity () const
    {
      return grid().template levelConnectivity< pit >( level_ );
    }

    //! \brief return connectivity
    Connectivity connectivity () const
    {
      return connectivity< pitype >();
    }

    //! \brief return host grid view
    template< PartitionType pit >
    const typename Partition< pit >::HostGridView hostGridView ( const int node ) const
    {
      assert( connectivity< pit >().contains( node ) );
      return connectivity< pit >().hostGrid( node );
    }

    //! \brief return host grid view
    const HostGridView hostGridView ( const int node ) const
    {
      return hostGridView< pitype >( node );
    }

  private:
    int level_;
  };

} // namespace Dune

#endif // #ifndef DUNE_MULTISPGRID_GRIDVIEW_HH
