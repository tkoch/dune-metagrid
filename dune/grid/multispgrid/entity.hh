#ifndef DUNE_MULTISPGRID_ENTITY_HH
#define DUNE_MULTISPGRID_ENTITY_HH

#include <cassert>

#include <dune/common/exceptions.hh>
#include <dune/common/nullptr.hh>
#include <dune/common/typetraits.hh>

#include <dune/geometry/referenceelements.hh>

#include <dune/geometry/type.hh>
#include <dune/grid/common/gridenums.hh>

#include "geometry.hh"

namespace Dune
{

  // Forward declarations
  // --------------------

  template< int, int, class > class MultiSPEntity;
  template< int, int, class > class MultiSPGeometry;
  template< int, int, class > class MultiSPLocalGeometry;



  // MultiSPEntityBase
  // -----------------

  /** \brief Base class for entities
   *
   *  \ingroup Entity
   */
  template< int codim, int dim, class Grid >
  class MultiSPEntityBase
  {
    // this type
    typedef MultiSPEntityBase< codim, dim, Grid > This;
    // grid traits
    typedef typename remove_const< Grid >::type::Traits Traits;
    //entity implementation
    typedef MultiSPEntity< codim, dim, const Grid > EntityImp;

  public:
    //! \brief type of host grid
    typedef typename Traits::HostGrid HostGrid;

    //! \brief codimension
    static const int codimension = codim;
    //! \brief grid dimension
    static const int dimension = Traits::dimension;
    //! \brief world dimension
    static const int dimensionworld = Traits::dimensionworld;
    //! \brief my dimension
    static const int mydimension = dimension-codimension;

    //! \brief single coordinate type
    typedef typename Traits::ctype ctype;

    //! \brief geometry type
    typedef typename Traits::template Codim< codimension >::Geometry Geometry;
    //! \brief type of entity seed
    typedef typename Traits::template Codim< codimension >::EntitySeed EntitySeed;

    //! \brief host entity type
    typedef typename HostGrid::template Codim< codimension >::Entity HostEntity;

  protected:
    // geometry implementation
    typedef MultiSPGeometry< mydimension, dimension, Grid > GeometryImp;

  public:
    //! \brief constructor
    explicit MultiSPEntityBase ( const Grid &grid )
    : grid_( &grid ),
      node_( -1 ),
      hostEntity_( nullptr )
    {}

    //! \brief constructor
    MultiSPEntityBase ( const HostEntity &hostEntity,
                      const int &node, const Grid &grid )
    : grid_( &grid ),
      node_( node ),
      hostEntity_( &hostEntity )
    {}

    //! \brief destructor
    virtual ~MultiSPEntityBase ()
    {}

    //! \brief cast to bool
    operator bool () const
    {
      assert( hostEntity_ ? (node_ >= 0) : (node_ == -1) );
      return bool( hostEntity_ );
    }

    //! \brief return level
    int level () const
    {
      return hostEntity().level();
    }

    //! \brief return partition type
    PartitionType partitionType () const
    {
      return InteriorEntity;
    }

    //! \brief return geometry
    Geometry geometry () const
    {
      return Geometry( GeometryImp( hostEntity().geometry() ) );
    }

    //! \brief return geometry type
    GeometryType type () const
    {
      return hostEntity().type();
    }

    //! \brief return entity seed
    EntitySeed seed () const
    {
      assert( *this );
      return EntitySeed( static_cast< const EntityImp & >( *this ) );
    }

    //! \brief return grid
    const Grid &grid () const
    {
      assert( grid_ );
      return *grid_;
    }

    //! \brief return node
    int node () const
    {
      return node_;
    }

    //! \brief return host entity
    const HostEntity &hostEntity () const
    {
      assert( *this );
      return *hostEntity_;
    }

  private:
    const Grid *grid_;
    int node_;
    const HostEntity *hostEntity_;
  };



  // MultiSPEntity, codim == 0
  // -------------------------

  /** \brief Template specialization for codim 0 entities
   *
   *  \ingroup Entity
   */
  template< int dim, class Grid >
  class MultiSPEntity< 0, dim, Grid >
  : public MultiSPEntityBase< 0, dim, Grid >
  {
    // this type
    typedef MultiSPEntity< 0, dim, Grid > This;
    // base type
    typedef MultiSPEntityBase< 0, dim, Grid > Base;

    // grid traits
    typedef typename remove_const< Grid >::type::Traits Traits;

  public:
    //! \brief single coordinate type
    typedef typename Base::ctype ctype;

    //! \brief geometry type
    typedef typename Base::Geometry Geometry;
    //! \brief local geometry type
    typedef typename Traits::template Codim< 0 >::LocalGeometry LocalGeometry;

    template< int cd >
    struct Codim
    {
      //! \brief type of entity pointer
      typedef typename Traits::template Codim< cd >::EntityPointer EntityPointer;
    };

    //! \brief entity pointer
    typedef typename Codim< 0 >::EntityPointer EntityPointer;

    //! \brief hierarchic iterator
    typedef typename Traits::HierarchicIterator HierarchicIterator;
    //! \brief leaf intersection iterator
    typedef typename Traits::LeafIntersectionIterator LeafIntersectionIterator;
    //! \brief level intersection iterator
    typedef typename Traits::LevelIntersectionIterator LevelIntersectionIterator;
    //! \brief entity seed
    typedef typename Base::EntitySeed EntitySeed;

    //! \brief host entity type
    typedef typename Base::HostEntity HostEntity;

  private:
    // local geometry implementation
    typedef MultiSPLocalGeometry< Base::mydimension, Base::dimension, Grid > LocalGeometryImp;

  public:
    using Base::grid;
    using Base::hostEntity;
    using Base::level;
    using Base::node;

    //! \brief constructor
    explicit MultiSPEntity ( const Grid &grid )
    : Base( grid )
    {}

    //! \brief constructor
    MultiSPEntity ( const HostEntity &hostEntity,
                  const int &node, const Grid &grid )
    : Base( hostEntity, node, grid )
    {}

    //! \brief return number of sub entities of codimension cc
    template< int cc >
    int count () const
    {
      return hostEntity().template count< cc >();
    }

    //! \brief return pointer to sub entity
    template< int cd >
    typename Codim< cd >::EntityPointer subEntity ( int i ) const
    {
      typedef typename Codim< cd >::EntityPointer EntityPointer;
      typedef typename EntityPointer::Implementation EntityPointerImp;
      return EntityPointer( EntityPointerImp( hostEntity().template subEntity< cd >( i ), node(), grid() ) );
    }

    //! \brief return leaf intersection iterator
    LeafIntersectionIterator ileafbegin () const DUNE_DEPRECATED
    {
      if( !isLeaf() )
        return ileafend();
      typedef typename LeafIntersectionIterator::Implementation IteratorImp;
      return IteratorImp::begin( *this );
    }

    //! \brief return leaf intersection iterator
    LeafIntersectionIterator ileafend () const DUNE_DEPRECATED
    {
      typedef typename LeafIntersectionIterator::Implementation IteratorImp;
      return IteratorImp::end( *this );
    }

    //! \brief return level intersection iterator
    LevelIntersectionIterator ilevelbegin () const DUNE_DEPRECATED
    {
      typedef typename LevelIntersectionIterator::Implementation IteratorImp;
      return IteratorImp::begin( *this );
    }

    //! \brief return level intersection iterator
    LevelIntersectionIterator ilevelend() const DUNE_DEPRECATED
    {
      typedef typename LevelIntersectionIterator::Implementation IteratorImp;
      return IteratorImp::end( *this );
    }

    //! \brief return pointer to father
    EntityPointer father () const
    {
      typedef typename EntityPointer::Implementation EntityPointerImp;
      return EntityPointer( EntityPointerImp( hostEntity().father(), node(), grid() ) );
    }

    //! \brief return true if entity has father
    bool hasFather () const
    {
      return hostEntity().hasFather();
    }

    //! \brief return true, if entity is on leaf level
    bool isLeaf () const
    {
      return hostEntity().isLeaf();
    }

    //! \brief return true, if entity is regular in red-green refinement (always true here)
    bool isRegular () const
    {
      return hostEntity().isRegular();
    }

    //! \brief return geometry in father
    const LocalGeometry geometryInFather () const
    {
      assert( hasFather() );
      return LocalGeometry( LocalGeometryImp( hostEntity().geometryInFather() ) );
    }

    //! \brief return hierarchic iterator
    HierarchicIterator hbegin ( int maxLevel ) const
    {
      typedef typename HierarchicIterator::Implementation IteratorImp;
      return IteratorImp::begin( maxLevel, *this );
    }

    //! \brief return hierarchic iterator
    HierarchicIterator hend ( int maxLevel ) const
    {
      typedef typename HierarchicIterator::Implementation IteratorImp;
      return IteratorImp::end( maxLevel, *this );
    }

    //! \brief return true if entity was produced in last call to adapt()
    bool isNew () const
    {
      return grid().wasRefined( node() );
    }

    //! \brief return true if entity might vanish in next call to adapt()
    bool mightVanish () const
    {
      // MultiSPGrid::preAdapt() returns false
      assert( hostEntity().mightVanish() == false );
      return hostEntity().mightVanish();
    }

    //! \brief return true if entity has boundary intersections
    bool hasBoundaryIntersections () const
    {
      typedef typename Base::HostGrid HostGrid;
      const HostGrid &hostGrid = grid().hostGrid( node() );
      {
        typedef typename HostGrid::LevelGridView HostGridView;
        HostGridView hostGridView = hostGrid.levelGridView( level() );
        typedef typename HostGridView::IntersectionIterator Iterator;
        const Iterator iend = hostGridView.iend( hostEntity() );
        for( Iterator iit = hostGridView.ibegin( hostEntity() ); iit != iend; ++iit )
        {
          const typename Iterator::Intersection &hostIntersection = *iit;
          if( !hostIntersection.boundary() )
            continue;
          if( grid().boundarySegmentIndexSet().contains( hostIntersection, node() ) )
            return true;
        }
      }
      {
        typedef typename HostGrid::LeafGridView HostGridView;
        HostGridView hostGridView = hostGrid.leafGridView();
        typedef typename HostGridView::IntersectionIterator Iterator;
        const Iterator iend = hostGridView.iend( hostEntity() );
        for( Iterator iit = hostGridView.ibegin( hostEntity() ); iit != iend; ++iit )
        {
          const typename Iterator::Intersection &hostIntersection = *iit;
          if( !hostIntersection.boundary() )
            continue;
          if( grid().boundarySegmentIndexSet().contains( hostIntersection, node() ) )
            return true;
        }
      }
      return false;
    }
  };



  // MultiSPEntity, codim != 0
  // -------------------------

  /** \brief Entity
   *
   *  \ingroup Entity
   */
  template< int codim, int dim, class Grid >
  class MultiSPEntity
  : public MultiSPEntityBase< codim, dim, Grid >
  {
    // base type
    typedef MultiSPEntityBase< codim, dim, Grid > Base;

  public:
    //! \brief host entity type
    typedef typename Base::HostEntity HostEntity;

    //! \brief constructor
    explicit MultiSPEntity ( const Grid &grid )
    : Base( grid )
    {}

    //! \brief constructor
    MultiSPEntity ( const HostEntity &hostEntity,
                  const int &node, const Grid &grid )
    : Base( hostEntity, node, grid )
    {}
  };

} // namespace Dune

#endif // #ifndef DUNE_MULTISPGRID_ENTITY_HH
