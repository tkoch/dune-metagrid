#ifndef DUNE_MULTISPGRID_CAPABILITIES_HH
#define DUNE_MULTISPGRID_CAPABILITIES_HH

#include <dune/geometry/genericgeometry/topologytypes.hh>

#include <dune/grid/common/capabilities.hh>

#include "declaration.hh"

namespace Dune
{

  namespace Capabilities
  {

    template< class ct, int dim >
    struct hasSingleGeometryType< MultiSPGrid< ct, dim > >
    {
      static const bool v = true;
      static const int topologyId = GenericGeometry::CubeTopology< dim >::type::id;
    };

    template< class ct, int dim >
    struct isCartesian< MultiSPGrid< ct, dim > >
    {
      static const bool v = true;
    };

    template< class ct, int dim, int codim >
    struct hasEntity< MultiSPGrid< ct, dim >, codim >
    {
      static const bool v = true;
    };

    template< class ct, int dim >
    struct isParallel< MultiSPGrid< ct, dim > >
    {
      static const bool v = false;
    };

    template< class ct, int dim, int codim >
    struct canCommunicate< MultiSPGrid< ct, dim >, codim >
    {
      static const bool v = false;
    };

    template< class ct, int dim >
    struct isLevelwiseConforming< MultiSPGrid< ct, dim > >
    {
      static const bool v = false;
    };

    template< class ct, int dim >
    struct isLeafwiseConforming< MultiSPGrid< ct, dim > >
    {
      static const bool v = false;
    };

    template< class ct, int dim >
    struct hasBackupRestoreFacilities< MultiSPGrid< ct, dim > >
    {
      static const bool v = false;
    };

    template< class ct, int dim >
    struct threadSafe< MultiSPGrid< ct, dim > >
    {
      static const bool v = false;
    };

    template< class ct, int dim >
    struct viewThreadSafe< MultiSPGrid< ct, dim > >
    {
      static const bool v = false;
    };

  } // namespace Capabilities

} // namespace Dune

#endif // #ifndef DUNE_MULTISPGRID_CAPABILITIES_HH
