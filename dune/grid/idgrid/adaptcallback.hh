#ifndef DUNE_IDGRID_ADAPTCALLBACK_HH
#define DUNE_IDGRID_ADAPTCALLBACK_HH

#include <dune/common/typetraits.hh>

#include <dune/grid/common/adaptcallback.hh>

namespace Dune
{

  // IdGridAdaptDataHandle
  // ---------------------

  template< class Grid, class WrappedHandle >
  class IdGridAdaptDataHandle
    : public AdaptDataHandle< typename Grid::HostGrid, IdGridAdaptDataHandle< Grid, WrappedHandle > >
  {
  protected:
    typedef IdGridAdaptDataHandle< Grid, WrappedHandle > This;

    typedef typename std::remove_const< Grid >::type::Traits Traits;
    typedef typename Traits :: ExtraData  ExtraData;

    typedef typename Traits::HostGrid::template Codim< 0 >::Entity HostEntity;

    typedef typename Traits::template Codim< 0 >::EntityImpl  EntityImpl;

  public:
    typedef typename Traits::template Codim< 0 >::Entity      Entity;

  private:
    IdGridAdaptDataHandle ( const This & );
    This &operator= ( const This & );

  public:
    IdGridAdaptDataHandle( ExtraData data, WrappedHandle &handle )
    : wrappedHandle_( handle ),
      data_( data )
    {}

    void preAdapt ( unsigned int estimateAdditionalElements )
    {
      wrappedHandle_.preAdapt( estimateAdditionalElements );
    }

    void postAdapt () { wrappedHandle_.postAdapt(); }

    void preCoarsening ( const HostEntity &hostFather ) const
    {
      const Entity father( EntityImpl( data_, hostFather ) );
      wrappedHandle_.preCoarsening( father );
    }

    void postRefinement ( const HostEntity &hostFather ) const
    {
      const Entity father( EntityImpl( data_, hostFather ) );
      wrappedHandle_.postRefinement( father );
    }

  protected:
    WrappedHandle &wrappedHandle_;
    ExtraData data_;
  };

} // namespace Dune

#endif // #ifndef DUNE_IDGRID_ADAPTCALLBACK_HH
