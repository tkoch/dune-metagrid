#ifndef DUNE_IDGRID_ITERATOR_HH
#define DUNE_IDGRID_ITERATOR_HH

#include <dune/geometry/referenceelements.hh>

#include <dune/grid/common/entityiterator.hh>

namespace Dune
{

  // IdGridIterator
  // --------------

  template< class Grid, class HostIterator >
  class IdGridIterator
  {
    typedef IdGridIterator< Grid, HostIterator > This;

  protected:
    typedef typename std::remove_const< Grid >::type::Traits Traits;

    /** \brief world dimension */
    static const int codimension = HostIterator::Entity::codimension;

  public:
    /** \brief grid dimension */
    static const int dimension = std::remove_const< Grid >::type::dimension;
    /** \brief type of entity */
    typedef typename Traits::template Codim< codimension >::Entity Entity;

  protected:
    typedef typename Traits::ExtraData ExtraData;

    typedef typename Traits::template Codim< codimension > :: EntityImpl EntityImpl;

  public:
    IdGridIterator ( ExtraData data, const HostIterator &hostIterator )
    : data_( data ),
      hostIterator_( hostIterator )
    {}

    explicit IdGridIterator ( const EntityImpl &entity )
    : data_( entity.data() ),
      hostIterator_( entity.hostEntity() )
    {}

    IdGridIterator ( const This &other )
    : data_( other.data_ ),
      hostIterator_( other.hostIterator_ )
    {}

    template< class HI >
    explicit IdGridIterator ( const IdGridIterator< Grid, HI > &other )
    : data_( other.data_ ),
      hostIterator_( other.hostIterator_ )
    {}

    const This &operator= ( const This &other )
    {
      data_ = other.data_;
      hostIterator_ = other.hostIterator_;
      return *this;
    }

    template< class HI >
    const This &operator= ( const IdGridIterator< Grid, HI > &other )
    {
      data_ = other.data_;
      hostIterator_ = other.hostIterator_;
      return *this;
    }

    /** \brief check for equality */
    template< class HI >
    bool equals ( const IdGridIterator< Grid, HI > &other ) const
    {
      return (hostIterator() == other.hostIterator());
    }

    /** \brief dereference entity */
    Entity dereference () const
    {
      return Entity( EntityImpl( data(), *hostIterator() ) );
    }

    /** \brief increment */
    void increment ()
    {
      ++hostIterator_;
    }

    /** \brief obtain level */
    int level () const { return hostIterator().level(); }

    /** \brief obtain host iterator */
    const HostIterator &hostIterator() const { return hostIterator_; }

  protected:
    ExtraData data () const { return data_; }

  protected:
    ExtraData data_;
    HostIterator hostIterator_;
  };

} // namespace Dune

#endif // #ifndef DUNE_IDGRID_ITERATOR_HH
