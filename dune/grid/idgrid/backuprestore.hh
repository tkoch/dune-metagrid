#ifndef DUNE_IDGRID_BACKUPRESTORE_HH
#define DUNE_IDGRID_BACKUPRESTORE_HH

#include <dune/grid/common/forwardbackuprestore.hh>

#include <dune/grid/idgrid/capabilities.hh>

namespace Dune
{

  // BackupRestoreFacility for IdGrid
  // --------------------------------

  template< class HostGrid >
  struct BackupRestoreFacility< IdGrid< HostGrid > > :
    public ForwardBackupRestoreFacility< IdGrid< HostGrid > >
  {
  };

} // namespace Dune

#endif // #ifndef DUNE_IDGRID_BACKUPRESTORE_HH
