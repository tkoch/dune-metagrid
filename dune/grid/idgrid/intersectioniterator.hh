#ifndef DUNE_IDGRID_INTERSECTIONITERATOR_HH
#define DUNE_IDGRID_INTERSECTIONITERATOR_HH

#include <dune/grid/idgrid/intersection.hh>

namespace Dune
{

  // IdGridIntersectionIterator
  // --------------------------

  template< class Grid, class HostIntersectionIterator >
  class IdGridIntersectionIterator
  {
  protected:
    typedef IdGridIntersectionIterator< Grid, HostIntersectionIterator > This;

    typedef typename std::remove_const< Grid >::type::Traits Traits;

    static const bool isLeafIntersection =
      std::is_same< HostIntersectionIterator,
               typename Grid::HostGrid::Traits::LeafIntersectionIterator > :: value ;
  public:
    typedef typename std::conditional< isLeafIntersection,
                                  typename Traits :: LeafIntersection,
                                  typename Traits :: LevelIntersection > :: type  Intersection ;
    typedef typename Intersection :: Implementation IntersectionImpl ;

    typedef typename Traits :: ExtraData ExtraData;

    IdGridIntersectionIterator ()
     : hostIterator_(),
       data_()
    {}

    IdGridIntersectionIterator ( ExtraData data, const HostIntersectionIterator &hostIterator )
     : hostIterator_( hostIterator ),
       data_( data )
    {}

    IdGridIntersectionIterator ( const This &other )
     : hostIterator_( other.hostIterator_ ),
       data_( other.data() )
    {}

    bool equals ( const This &other ) const
    {
      return (hostIterator_ == other.hostIterator_);
    }

    void increment ()
    {
      ++hostIterator_;
    }

    Intersection dereference () const
    {
      return IntersectionImpl( data(), *hostIterator_ );
    }

    ExtraData data() const { return data_; }

  protected:
    HostIntersectionIterator hostIterator_;
    ExtraData data_;
  };

} // namespace Dune

#endif // #ifndef DUNE_IDGRID_INTERSECTIONITERATOR_HH
