#ifndef DUNE_IDGRID_DECLARATION_HH
#define DUNE_IDGRID_DECLARATION_HH

namespace Dune
{

  template< class HostGrid >
  class IdGrid;

} // namespace Dune

#endif // #ifndef DUNE_IDGRID_DECLARATION_HH
