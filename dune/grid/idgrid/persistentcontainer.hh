#ifndef DUNE_IDGRID_PERSISTENTCONTAINER_HH
#define DUNE_IDGRID_PERSISTENTCONTAINER_HH

#include <dune/grid/utility/persistentcontainerwrapper.hh>

#include <dune/grid/idgrid/declaration.hh>

namespace Dune
{

  // PersistentContainer for IdGrid
  // ------------------------------

  template< class HostGrid, class T >
  class PersistentContainer< IdGrid< HostGrid >, T >
  : public PersistentContainerWrapper< IdGrid< HostGrid >, T >
  {
    typedef PersistentContainerWrapper< IdGrid< HostGrid >, T > Base;

  public:
    typedef typename Base::Grid Grid;
    typedef typename Base::Value Value;

    PersistentContainer ( const Grid &grid, int codim, const Value &value = Value() )
    : Base( grid, codim, value )
    {}
  };

} // namespace Dune

#endif // #ifndef DUNE_IDGRID_PERSISTENTCONTAINER_HH
