#ifndef DUNE_CACHEITGRID_BACKUPRESTORE_HH
#define DUNE_CACHEITGRID_BACKUPRESTORE_HH

#include <dune/grid/common/backuprestore.hh>
#include <dune/grid/utility/grapedataioformattypes.hh>

#include <dune/grid/cacheitgrid/capabilities.hh>

namespace Dune
{

  // BackupRestoreFacilities
  // -----------------------

  template< class Grid, bool hasBackupRestoreFacilities = Capabilities::hasBackupRestoreFacilities< Grid > ::v >
  class CacheItGridBackupRestoreFacilities
  {};

  template< class Grid >
  class CacheItGridBackupRestoreFacilities< Grid, true >
  {
    typedef CacheItGridBackupRestoreFacilities< Grid, true > This;

  protected:
    CacheItGridBackupRestoreFacilities ()
    {}

  private:
    CacheItGridBackupRestoreFacilities ( const This & );
    This &operator= ( const This & );

  public:
    template< GrapeIOFileFormatType type >
    bool writeGrid ( const std::string &filename, double time ) const
    {
      return asImp().hostGrid().template writeGrid< type >( filename, time );
    }

    template< GrapeIOFileFormatType type >
    bool readGrid ( const std::string &filename, double &time )
    {
      const bool success
        = asImp().hostGrid().template readGrid< type >( filename, time );
      asImp().update();
      return success;
    }

  protected:
    const Grid &asImp () const
    {
      return static_cast< const Grid & >( *this );
    }

    Grid &asImp ()
    {
      return static_cast< Grid & >( *this );
    }
  };


  // BackupRestoreFacility for CacheItGrid
  // -------------------------------------

  template< class HostGrid >
  struct BackupRestoreFacility< CacheItGrid< HostGrid > >
  {
    typedef CacheItGrid< HostGrid > Grid;
    typedef BackupRestoreFacility< HostGrid > HostBackupRestoreFacility;

    static void backup ( const Grid &grid, const std::string &filename )
    {
      HostBackupRestoreFacility::backup( grid.hostGrid(), filename );
    }

    static void backup ( const Grid &grid, std::ostream &stream )
    {
      HostBackupRestoreFacility::backup( grid.hostGrid(), stream );
    }

    static Grid *restore ( const std::string &filename )
    {
      HostGrid* hostGrid = HostBackupRestoreFacility::restore( filename );
      if( ! hostGrid )
        DUNE_THROW(IOError,"HostGrid was not created!");
      return new Grid( hostGrid );
    }

    static Grid *restore ( std::istream &stream )
    {
      HostGrid* hostGrid = HostBackupRestoreFacility::restore( stream );
      if( ! hostGrid )
        DUNE_THROW(IOError,"HostGrid was not created!");
      return new Grid( hostGrid );
    }
  };

} // namespace Dune

#endif // #ifndef DUNE_CACHEITGRID_BACKUPRESTORE_HH
