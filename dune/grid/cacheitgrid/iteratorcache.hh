#ifndef DUNE_CACHEITGRID_ITERATORCACHE_HH
#define DUNE_CACHEITGRID_ITERATORCACHE_HH

#include <cassert>
#include <memory>

#include <dune/grid/common/gridenums.hh>

namespace Dune
{

  // PartitionIteratorInfo
  // ---------------------

  template< PartitionIteratorType pitype >
  struct PartitionIteratorInfo;

  template<>
  struct PartitionIteratorInfo< Interior_Partition >
  {
    static const PartitionType begin = InteriorEntity;
    static const PartitionType end = InteriorEntity;
  };

  template<>
  struct PartitionIteratorInfo< InteriorBorder_Partition >
  {
    static const PartitionType begin = InteriorEntity;
    static const PartitionType end = BorderEntity;
  };

  template<>
  struct PartitionIteratorInfo< Overlap_Partition >
  {
    static const PartitionType begin = InteriorEntity;
    static const PartitionType end = OverlapEntity;
  };

  template<>
  struct PartitionIteratorInfo< OverlapFront_Partition >
  {
    static const PartitionType begin = InteriorEntity;
    static const PartitionType end = FrontEntity;
  };

  template<>
  struct PartitionIteratorInfo< All_Partition >
  {
    static const PartitionType begin = InteriorEntity;
    static const PartitionType end = GhostEntity;
  };

  template<>
  struct PartitionIteratorInfo< Ghost_Partition >
  {
    static const PartitionType begin = GhostEntity;
    static const PartitionType end = GhostEntity;
  };



  // CacheItGridIteratorCache
  // ------------------------

  template< class HostGrid, int codim, class Allocator = std::allocator< void > >
  class CacheItGridIteratorCache
  {
    typedef CacheItGridIteratorCache< HostGrid, codim, Allocator > This;

    // ChostEntity+1 is the invalid tpye (needed by ParallelGrid)
    // therefore, we use GhostEntity+2 partition types
    static const unsigned int numPartitionTypes = GhostEntity+2;

  public:
    static const int codimension = codim;

    typedef typename HostGrid::template Codim< codimension >::EntitySeed HostEntitySeed;

    explicit CacheItGridIteratorCache ( const Allocator &allocator = Allocator() )
    : allocator_( allocator ),
      seeds_( 0 )
    {
      std::fill( offsets_, offsets_ + (numPartitionTypes+1), 0 );
    }

    CacheItGridIteratorCache ( const This &other )
    : allocator_( other.allocator_ ),
      seeds_( 0 )
    {
      *this = other;
    }

    const This &operator= ( const This &other )
    {
      if( this != &other )
      {
        if( other )
        {
          if( seeds_ )
            deallocate();

          std::copy( other.offsets_, other.offsets_ + (numPartitionTypes+1), offsets_ );
          seeds_ = allocator_.allocate ( offsets_[ numPartitionTypes ] );
          for( unsigned int i = 0; i < offsets_[ numPartitionTypes ]; ++i )
            allocator_.construct( seeds_ + i, other.seeds_[ i ] );
        }
        else
          clear();
      }
      return *this;
    }

    ~CacheItGridIteratorCache ()
    {
      if( seeds_ )
        deallocate();
    }

    operator bool () const { return bool( seeds_ ); }

    const HostEntitySeed *begin ( PartitionType ptype ) const
    {
      assert( *this );
      unsigned int p = ptype;
      assert( p < numPartitionTypes );
      return seeds_ + offsets_[ p ];
    }

    template< PartitionIteratorType pitype >
    const HostEntitySeed *begin () const
    {
      return begin( PartitionIteratorInfo< pitype >::begin );
    }

    const HostEntitySeed *end ( PartitionType ptype ) const
    {
      assert( *this );
      unsigned int p = ptype;
      assert( p < numPartitionTypes );
      return seeds_ + offsets_[ p+1 ];
    }

    template< PartitionIteratorType pitype >
    const HostEntitySeed *end () const
    {
      return end( PartitionIteratorInfo< pitype >::end );
    }

    std::size_t size ( PartitionType ptype ) const
    {
      unsigned int p = ptype;
      assert( p < numPartitionTypes );
      return (offsets_[ p+1 ] - offsets_[ p ]);
    }

    template< PartitionIteratorType pitype >
    std::size_t size () const
    {
      typedef PartitionIteratorInfo< pitype > Info;
      return (offsets_[ Info::end ] - offsets_[ Info::begin ]);
    }

    std::size_t size () const { return size< All_Partition >(); }

    void clear ()
    {
      if( seeds_ )
      {
        deallocate();
        seeds_ = 0;
        std::fill( offsets_, offsets_ + (numPartitionTypes+1), 0 );
      }
    }

    template< class HostGridView >
    void update ( const HostGridView &hostGridView )
    {
      typedef typename HostGrid::template Codim< codim >::Entity HostEntity;
      typedef typename HostGridView::template Codim< codim >::Iterator HostIterator;

      if( seeds_ )
        deallocate();

      std::size_t cnt[ numPartitionTypes ];
      std::fill( cnt, cnt + numPartitionTypes, 0 );

      // count number of entities for each partition type
      const HostIterator eend = hostGridView.template end< codim >();
      for( HostIterator eit = hostGridView.template begin< codim >(); eit != eend; ++eit )
      {
        const unsigned int ptype = eit->partitionType();
        assert( ptype < numPartitionTypes );
        cnt[ ptype ]++;
      }

      // allocate memory for entity seeds
      offsets_[ 0 ] = 0;
      for( unsigned int p = 0; p < numPartitionTypes; ++p )
        offsets_[ p+1 ] = offsets_[ p ] + cnt[ p ];
      seeds_ = allocator_.allocate ( offsets_[ numPartitionTypes ] );

      // now obtain the entity seeds
      std::fill( cnt, cnt + numPartitionTypes, 0 );
      for( HostIterator eit = hostGridView.template begin< codim >(); eit != eend; ++eit )
      {
        const HostEntity &entity = *eit;

        const unsigned int ptype = entity.partitionType();
        assert( ptype < numPartitionTypes );

        const std::size_t seedIndex = offsets_[ ptype ] + cnt[ ptype ];
        assert( seedIndex < offsets_[ ptype+1 ] );
        allocator_.construct( seeds_ + seedIndex, entity.seed() );
        ++cnt[ ptype ];
      }

#ifndef NDEBUG
      for( unsigned int p = 0; p < numPartitionTypes; ++p )
        assert( cnt[ p ] == offsets_[ p+1 ] - offsets_[ p ] );
#endif
    }

  private:
    void deallocate ()
    {
      for( HostEntitySeed *it = seeds_; it != seeds_ + size(); ++it )
        allocator_.destroy( it );
      allocator_.deallocate( seeds_, size() );
    }

    typename Allocator::template rebind< HostEntitySeed >::other allocator_;
    HostEntitySeed *seeds_;
    std::size_t offsets_[ numPartitionTypes+1 ];
  };

} // namespace Dune

#endif // #ifndef DUNE_CACHEITGRID_ITERATORCACHE_HH
