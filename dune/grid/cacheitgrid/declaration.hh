#ifndef DUNE_CACHEITGRID_DECLARATION_HH
#define DUNE_CACHEITGRID_DECLARATION_HH

namespace Dune
{

  template< class HostGrid >
  class CacheItGrid;

} // namespace Dune

#endif // #ifndef DUNE_CACHEITGRID_DECLARATION_HH
