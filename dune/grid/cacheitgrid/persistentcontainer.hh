#ifndef DUNE_CACHEITGRID_PERSISTENTCONTAINER_HH
#define DUNE_CACHEITGRID_PERSISTENTCONTAINER_HH

#include <dune/grid/cacheitgrid/hostgridaccess.hh>
#include <dune/grid/utility/persistentcontainerwrapper.hh>

#include <dune/grid/cacheitgrid/declaration.hh>

namespace Dune
{

  // PersistentContainer for CacheItGrid
  // -----------------------------------

  template< class HostGrid, class T >
  class PersistentContainer< CacheItGrid< HostGrid >, T >
  : public PersistentContainerWrapper< CacheItGrid< HostGrid >, T >
  {
    typedef PersistentContainerWrapper< CacheItGrid< HostGrid >, T > Base;

  public:
    typedef typename Base::Grid Grid;
    typedef typename Base::Value Value;

    PersistentContainer ( const Grid &grid, int codim, const Value &value = Value() )
    : Base( grid, codim, value )
    {}
  };

} // namespace Dune

#endif // #ifndef DUNE_CACHEITGRID_PERSISTENTCONTAINER_HH
