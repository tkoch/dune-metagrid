#ifndef DUNE_CACHEITGRID_ITERATOR_HH
#define DUNE_CACHEITGRID_ITERATOR_HH

#include <dune/common/typetraits.hh>

#include <dune/geometry/referenceelements.hh>

#include <dune/grid/common/entityiterator.hh>

#include <dune/grid/cacheitgrid/entitypointer.hh>
#include <dune/grid/idgrid/iterator.hh>

namespace Dune
{

  // CacheItGridIterator
  // -------------------

  template< int codim, class Grid >
  class CacheItGridIterator
  {
    typedef CacheItGridIterator< codim, Grid > This;

    typedef CacheItGridEntityPointerTraits< codim, Grid > EntityPointerTraits;
    friend class CacheItGridEntityPointer< EntityPointerTraits >;

    typedef typename remove_const< Grid >::type::Traits Traits;

    typedef typename Traits::HostGrid HostGrid;

  public:
    static const int dimension = Traits::dimension;
    static const int codimension = codim;

    typedef typename Traits::template Codim< codimension >::Entity Entity;

    typedef CacheItGridEntityPointer< EntityPointerTraits > EntityPointerImp;

  protected:
    typedef typename HostGrid::template Codim< codimension >::EntityPointer HostEntityPointer;
    typedef typename HostGrid::template Codim< codimension >::EntitySeed HostEntitySeed;

  private:
    typedef CacheItGridEntity< codimension, dimension, const Grid > EntityImpl;

  public:
    CacheItGridIterator ( const HostGrid &hostGrid, const HostEntitySeed *hostEntitySeed )
    : hostGrid_( &hostGrid ),
      hostEntitySeed_( hostEntitySeed ),
      hostEntityPointer_( 0 ),
      entity_( EntityImpl() )
    {}

    CacheItGridIterator ( const This &other )
    : hostGrid_( other.hostGrid_ ),
      hostEntitySeed_( other.hostEntitySeed_ ),
      hostEntityPointer_( 0 ),
      entity_( EntityImpl() )
    {}

    ~CacheItGridIterator () { releaseEntity(); }

    const This &operator= ( const This &other )
    {
      hostGrid_ = other.hostGrid_;
      hostEntitySeed_ = other.hostEntitySeed_;
      releaseEntity();
      return *this;
    }

    bool equals ( const This &other ) const
    {
      return (hostEntitySeed_ == other.hostEntitySeed_);
    }

    template< class T >
    bool equals ( const CacheItGridEntityPointer< T > &other ) const
    {
      return other.equals( *this );
    }

    Entity &dereference () const
    {
      if( !hostEntityPointer_ )
      {
        hostEntityPointer_ = new (hostEntityPointerMemory) HostEntityPointer( hostGrid().entityPointer( *hostEntitySeed_ ) );
        entityImpl() = EntityImpl( *(*hostEntityPointer_) );
      }
      return entity_;
    }

    int level () const
    {
      return hostIterator().level();
    }

    void increment ()
    {
      ++hostEntitySeed_;
      releaseEntity();
    }

    const HostEntityPointer &hostIterator () const
    {
      dereference();
      return *hostEntityPointer_;
    }

  protected:
    void releaseEntity ()
    {
      if( hostEntityPointer_ )
      {
        // free the host entity pointer
        hostEntityPointer_->~HostEntityPointer();
        hostEntityPointer_ = 0;

        // invalidate the entity
        entityImpl() = EntityImpl();
      }
    }

    const HostGrid &hostGrid () const { return *hostGrid_; }

  private:
    EntityImpl &entityImpl () const
    {
      return Grid::getRealImplementation( entity_ );
    }

    const HostGrid *hostGrid_;
    const HostEntitySeed *hostEntitySeed_;
    mutable HostEntityPointer *hostEntityPointer_;
    mutable Entity entity_;
    mutable char hostEntityPointerMemory[ sizeof( HostEntityPointer ) ];
  };

} // namespace Dune

#endif // #ifndef DUNE_CACHEITGRID_ITERATOR_HH
