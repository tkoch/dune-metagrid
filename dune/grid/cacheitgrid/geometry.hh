#ifndef DUNE_CACHEITGRID_GEOMETRY_HH
#define DUNE_CACHEITGRID_GEOMETRY_HH

#include <dune/grid/common/geometry.hh>

namespace Dune
{

  // Internal Forward Declarations
  // -----------------------------

  template< int, int, class > class CacheItGridGeometry;
  template< int, int, class > class CacheItGridLocalGeometry;



  // CacheItGridBasicGeometry
  // -------------------

  template< class Traits >
  struct CacheItGridBasicGeometry
  {
    typedef typename Traits::HostGeometry HostGeometry;

    static const int dimension = HostGeometry::dimension;
    static const int mydimension = HostGeometry::mydimension;
    static const int coorddimension = HostGeometry::coorddimension;
    static const int dimensionworld = HostGeometry::dimensionworld;

    typedef typename HostGeometry::ctype ctype;
    typedef FieldVector< ctype, mydimension > LocalVector;
    typedef FieldVector< ctype, coorddimension > GlobalVector;

    typedef typename HostGeometry::JacobianTransposed JacobianTransposed;
    typedef typename HostGeometry::JacobianInverseTransposed JacobianInverseTransposed;

    CacheItGridBasicGeometry ( const HostGeometry &hostGeometry )
    : hostGeometry_( hostGeometry )
    {}

    GeometryType type () const { return hostGeometry().type(); }
    bool affine () const { return hostGeometry().affine(); }

    int corners () const { return hostGeometry().corners(); }
    GlobalVector corner ( const int i ) const { return hostGeometry().corner( i ); }
    GlobalVector center () const { return hostGeometry().center(); }

    GlobalVector global ( const LocalVector &local ) const { return hostGeometry().global( local ); }
    LocalVector local ( const GlobalVector &global ) const { return hostGeometry().local( global ); }

    ctype integrationElement ( const LocalVector &local ) const { return hostGeometry().integrationElement( local ); }
    ctype volume () const { return hostGeometry().volume(); }

    const JacobianTransposed &
    jacobianTransposed ( const LocalVector &local ) const
    {
      return hostGeometry().jacobianTransposed( local );
    }

    const JacobianInverseTransposed &
    jacobianInverseTransposed ( const LocalVector &local ) const
    {
      return hostGeometry().jacobianInverseTransposed( local );
    }

  private:
    const HostGeometry &hostGeometry () const
    {
      return hostGeometry_;
    }

    HostGeometry hostGeometry_;
  };



  // CacheItGridGeometryTraits
  // --------------------

  template< int mydim, class Grid >
  struct CacheItGridGeometryTraits
  {
    typedef typename remove_const< Grid >::type::HostGrid HostGrid;
    static const int dimension = HostGrid::dimension;
    static const int mydimension = mydim;
    static const int codimension = dimension - mydimension;
    typedef typename HostGrid::template Codim< codimension >::Geometry HostGeometry;
  };



  // CacheItGridGeometry
  // --------------

  template< int mydim, int cdim, class Grid >
  class CacheItGridGeometry
  : public CacheItGridBasicGeometry< CacheItGridGeometryTraits< mydim, Grid > >
  {
    typedef CacheItGridBasicGeometry< CacheItGridGeometryTraits< mydim, Grid > > Base;

  public:
    typedef typename Base::HostGeometry HostGeometry;

    CacheItGridGeometry ()
    {}

    CacheItGridGeometry ( const HostGeometry &hostGeometry )
    : Base( hostGeometry )
    {}
  };



  // CacheItGridLocalGeometryTraits
  // -------------------------

  template< int mydim, class Grid >
  struct CacheItGridLocalGeometryTraits
  {
    typedef typename remove_const< Grid >::type::HostGrid HostGrid;
    static const int dimension = HostGrid::dimension;
    static const int mydimension = mydim;
    static const int codimension = dimension - mydimension;
    typedef typename HostGrid::template Codim< codimension >::LocalGeometry HostGeometry;
  };



  // CacheItGridLocalGeometry
  // -------------------

  template< int mydim, int cdim, class Grid >
  class CacheItGridLocalGeometry
  : public CacheItGridBasicGeometry< CacheItGridLocalGeometryTraits< mydim, Grid > >
  {
    typedef CacheItGridBasicGeometry< CacheItGridLocalGeometryTraits< mydim, Grid > > Base;

  public:
    typedef typename Base::HostGeometry HostGeometry;

    CacheItGridLocalGeometry ()
    {}

    CacheItGridLocalGeometry ( const HostGeometry &hostGeometry )
    : Base( hostGeometry )
    {}
  };

} // namespace Dune

#endif // #ifndef DUNE_CACHEITGRID_GEOMETRY_HH
