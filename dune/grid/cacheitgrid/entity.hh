#ifndef DUNE_CACHEITGRID_ENTITY_HH
#define DUNE_CACHEITGRID_ENTITY_HH

//- dune-common includes
#include <dune/common/nullptr.hh>

//- dune-grid includes
#include <dune/grid/common/grid.hh>

//- dune-metagrid includes
#include <dune/grid/cacheitgrid/entityseed.hh>
#include <dune/grid/cacheitgrid/hierarchiciterator.hh>

namespace Dune
{

  // External Forward Declarations
  // -----------------------------

  template< class, class > class CacheItGridIntersection;
  template< class, class > class CacheItGridIntersectionIterator;

  template< class > class CacheItGridHierarchicIterator;
  template< class > class CacheItGridHierarchicIteratorTraits;



  // CacheItGridEntity
  // -----------------

  /** \copydoc CacheItGridEntity
   *
   *  \nosubgrouping
   */
  template< int codim, int dim, class Grid >
  class CacheItGridEntity
  {
    typedef typename remove_const< Grid >::type::Traits Traits;

  public:
    /** \name Attributes
     *  \{ */

    //! codimensioon of the entity
    static const int codimension = codim;
    //! dimension of the grid
    static const int dimension = Traits::dimension;
    //! dimension of the entity
    static const int mydimension = dimension - codimension;
    //! dimension of the world
    static const int dimensionworld = Traits::dimensionworld;

    /** \} */

    /** \name Types Required by DUNE
     *  \{ */

    //! coordinate type of the grid
    typedef typename Traits::ctype ctype;

    //! type of corresponding entity seed
    typedef typename Grid::template Codim< codimension >::EntitySeed EntitySeed;
    //! type of corresponding geometry
    typedef typename Traits::template Codim< codimension >::Geometry Geometry;

    /** \} */

  private:
    typedef typename Traits::HostGrid HostGrid;

  public:
    /** \name Host Types
     *  \{ */

    //! type of corresponding host entity
    typedef typename HostGrid::template Codim< codimension >::Entity HostEntity;
    //! type of corresponding host entity pointer
    typedef typename HostGrid::template Codim< codimension >::EntityPointer HostEntityPointer;
    /** \} */

    /** \name Construction, Initialization and Destruction
     *  \{ */

    /** \brief construct a null entity */
    CacheItGridEntity ()
    : hostEntity_( nullptr )
    {}

    /** \brief construct an initialized entity
     *
     *  \param[in]  hostEntity  corresponding entity in the host grid
     *
     *  \note The reference to the host entity must remain valid  as long as
     *        this entity is in use.
     */
    explicit CacheItGridEntity ( const HostEntity &hostEntity )
    : hostEntity_( &hostEntity )
    {}

    /** \} */

    operator bool () const { return bool( hostEntity_ ); }

    /** \name Methods Shared by Entities of All Codimensions
     *  \{ */

    /** \brief obtain the name of the corresponding reference element
     *
     *  This type can be used to access the DUNE reference element.
     */
    GeometryType type () const
    {
      return hostEntity().type();
    }

    /** \brief obtain the level of this entity */
    int level () const
    {
      return hostEntity().level();
    }

    /** \brief obtain the partition type of this entity */
    PartitionType partitionType () const
    {
      return hostEntity().partitionType();
    }

    /** obtain the geometry of this entity */
    Geometry geometry () const
    {
      return Geometry( hostEntity().geometry() );
    }

    /** \brief return EntitySeed of host grid entity */
    EntitySeed seed () const { return typename EntitySeed::Implementation( hostEntity().seed() ); }

    /** \} */


    /** \name Methods Supporting the Grid Implementation
     *  \{ */

    const HostEntity &hostEntity () const
    {
      assert( *this );
      return *hostEntity_;
    }

    /** \} */

  private:
    const HostEntity *hostEntity_;
  };



  // CacheItGridEntity for codimension 0
  // -----------------------------------

  /** \copydoc CacheItGridEntity
   *
   *  \nosubgrouping
   */
  template< int dim, class Grid >
  class CacheItGridEntity< 0, dim, Grid >
  {
    typedef typename remove_const< Grid >::type::Traits Traits;

    typedef typename Traits::HostGrid HostGrid;

  public:
    /** \name Attributes
     *  \{ */

    //! codimensioon of the entity
    static const int codimension = 0;
    //! dimension of the grid
    static const int dimension = Traits::dimension;
    //! dimension of the entity
    static const int mydimension = dimension - codimension;
    //! dimension of the world
    static const int dimensionworld = Traits::dimensionworld;

    /** \} */

    /** \name Host Types
     *  \{ */

    //! type of corresponding host entity
    typedef typename HostGrid::template Codim< codimension >::Entity HostEntity;
    //! type of corresponding host entity pointer
    typedef typename HostGrid::template Codim< codimension >::EntityPointer HostEntityPointer;

    /** \} */

  private:
    typedef CacheItGridIntersectionIterator< Grid, typename HostEntity::LeafIntersectionIterator > LeafIntersectionIteratorImpl;
    typedef CacheItGridIntersectionIterator< Grid, typename HostEntity::LevelIntersectionIterator > LevelIntersectionIteratorImpl;

    typedef CacheItGridIntersection< Grid, typename HostEntity::LeafIntersectionIterator::Intersection > LeafIntersectionImpl;
    typedef CacheItGridIntersection< Grid, typename HostEntity::LevelIntersectionIterator::Intersection > LevelIntersectionImpl;

  public:
    /** \name Types Required by DUNE
     *  \{ */

    //! coordinate type of the grid
    typedef typename Traits::ctype ctype;

    //! type of corresponding entity seed
    typedef typename Grid::template Codim< codimension >::EntitySeed EntitySeed;
    //! type of corresponding geometry
    typedef typename Traits::template Codim< codimension >::Geometry Geometry;
    //! type of corresponding local geometry
    typedef typename Traits::template Codim< codimension >::LocalGeometry LocalGeometry;
    //! type of corresponding entity pointer
    typedef typename Traits::template Codim< codimension >::EntityPointer EntityPointer;

    //! type of hierarchic iterator
    typedef typename Traits::HierarchicIterator HierarchicIterator;
    //! type of leaf intersection iterator
    typedef Dune::IntersectionIterator< Grid, LeafIntersectionIteratorImpl, LeafIntersectionImpl > LeafIntersectionIterator;
    //! type of level intersection iterator
    typedef Dune::IntersectionIterator< Grid, LevelIntersectionIteratorImpl, LevelIntersectionImpl > LevelIntersectionIterator;

    /** \} */

    /** \name Construction, Initialization and Destruction
     *  \{ */

    /** \brief construct a null entity */
    CacheItGridEntity ()
    : hostEntity_( nullptr )
    {}

    /** \brief construct an initialized entity
     *
     *  \param[in]  hostEntity  corresponding entity in the host grid
     *
     *  \note The reference to the host entity must remain valid as long as
     *        this entity is in use.
     */
    explicit CacheItGridEntity ( const HostEntity &hostEntity )
    : hostEntity_( &hostEntity )
    {}

    /** \} */

    operator bool () const { return bool( hostEntity_ ); }

    /** \name Methods Shared by Entities of All Codimensions
     *  \{ */

    /** \brief obtain the name of the corresponding reference element
     *
     *  This type can be used to access the DUNE reference element.
     */
    GeometryType type () const
    {
      return hostEntity().type();
    }

    /** \brief obtain the level of this entity */
    int level () const
    {
      return hostEntity().level();
    }

    /** \brief obtain the partition type of this entity */
    PartitionType partitionType () const
    {
      return hostEntity().partitionType();
    }

    /** obtain the geometry of this entity */
    Geometry geometry () const
    {
      return Geometry( hostEntity().geometry() );
    }

    /** \brief return EntitySeed of host grid entity */
    EntitySeed seed () const { return typename EntitySeed::Implementation( hostEntity().seed() ); }

    /** \} */

    template< int codim >
    int count () const
    {
      return hostEntity().template count< codim >();
    }

    template< int codim >
    typename Grid::template Codim< codim >::EntityPointer
    subEntity ( int i ) const
    {
      typedef typename Traits::template Codim< codim >::EntityPointerImpl EntityPointerImpl;
      return EntityPointerImpl( hostEntity().template subEntity< codim >( i ) );
    }

    LevelIntersectionIterator ilevelbegin () const
    {
      return LevelIntersectionIteratorImpl( hostEntity().ilevelbegin() );
    }

    LevelIntersectionIterator ilevelend () const
    {
      return LevelIntersectionIteratorImpl( hostEntity().ilevelend() );
    }

    LeafIntersectionIterator ileafbegin () const
    {
      return LeafIntersectionIteratorImpl( hostEntity().ileafbegin() );
    }

    LeafIntersectionIterator ileafend () const
    {
      return LeafIntersectionIteratorImpl( hostEntity().ileafend() );
    }

    bool hasBoundaryIntersections () const
    {
      return hostEntity().hasBoundaryIntersections();
    }

    bool isLeaf () const
    {
      return hostEntity().isLeaf();
    }

    EntityPointer father () const
    {
      typedef typename Traits::template Codim< 0 >::EntityPointerImpl EntityPointerImpl;
      return EntityPointerImpl( hostEntity().father() );
    }

    bool hasFather () const
    {
      return hostEntity().hasFather();
    }

    LocalGeometry geometryInFather () const
    {
      return LocalGeometry( hostEntity().geometryInFather() );
    }

    HierarchicIterator hbegin ( int maxLevel ) const
    {
      typedef CacheItGridHierarchicIteratorTraits< Grid > T;
      return CacheItGridHierarchicIterator< T >( hostEntity().hbegin( maxLevel ) );
    }

    HierarchicIterator hend ( int maxLevel ) const
    {
      typedef CacheItGridHierarchicIteratorTraits< Grid > T;
      return CacheItGridHierarchicIterator< T >( hostEntity().hend( maxLevel ) );
    }

    bool isRegular () const
    {
      return hostEntity().isRegular();
    }

    bool isNew () const
    {
      return hostEntity().isNew();
    }

    bool mightVanish () const
    {
      return hostEntity().mightVanish();
    }

    const HostEntity &hostEntity () const
    {
      assert( *this );
      return *hostEntity_;
    }

    /** \} */

  private:
    const HostEntity *hostEntity_;
  };

} // namespace Dune

#endif // #ifndef DUNE_CACHEITGRID_ENTITY_HH
