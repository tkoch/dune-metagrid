#ifndef DUNE_CACHEITGRID_TWISTUTILITY_HH
#define DUNE_CACHEITGRID_TWISTUTILITY_HH

#include <cassert>

#include <dune/grid/cacheitgrid/declaration.hh>

#if HAVE_DUNE_FEM
#include <dune/fem/quadrature/caching/twistutility.hh>

namespace Dune
{
  namespace Fem
  {

    // Specialization for CacheItGrid
    // -------------------------

    template< class HostGrid >
    struct TwistUtility< CacheItGrid< HostGrid > >
    {
      typedef CacheItGrid< HostGrid > GridType;

      typedef typename GridType::Traits::LeafIntersectionIterator  LeafIntersectionIterator;
      typedef typename LeafIntersectionIterator::Intersection  LeafIntersection;
      typedef typename GridType::Traits::LevelIntersectionIterator LevelIntersectionIterator;
      typedef typename LevelIntersectionIterator::Intersection LevelIntersection;

      static const int dimension = GridType::dimension;

      typedef TwistUtility< HostGrid > HostTwistUtilityType;

    public:
      template< class Intersection >
      static int twistInSelf ( const GridType &grid, const Intersection &it )
      {
        return HostTwistUtilityType::twistInSelf( grid.hostGrid(),
            GridType::getRealImplementation( it ).hostIntersection() );
      }

      template< class Intersection >
      static int twistInNeighbor ( const GridType &grid, const Intersection &it )
      {
        return HostTwistUtilityType::twistInNeighbor( grid.hostGrid(),
            GridType::getRealImplementation( it ).hostIntersection() );
      }

      template< class Intersection >
      static inline GeometryType
      elementGeometry ( const Intersection &intersection, const bool inside)
      {
        return HostTwistUtilityType::elementGeometry(
            GridType::getRealImplementation( intersection ).hostIntersection(),
            inside );
      }
    };

  } // end namespace Fem

} // end namespace Dune

#endif // #if HAVE_DUNE_FEM

#endif // #ifndef DUNE_TWISTUTILITY_HH
