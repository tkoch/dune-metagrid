#ifndef DUNE_CACHEITGRID_INTERSECTIONITERATOR_HH
#define DUNE_CACHEITGRID_INTERSECTIONITERATOR_HH

#include <dune/grid/cacheitgrid/entitypointer.hh>
#include <dune/grid/cacheitgrid/intersection.hh>

namespace Dune
{

  // CacheItGridIntersectionIterator
  // -------------------------------

  template< class Grid, class HostIntersectionIterator >
  class CacheItGridIntersectionIterator
  {
    typedef CacheItGridIntersectionIterator< Grid, HostIntersectionIterator > This;

    typedef typename remove_const< Grid >::type::Traits Traits;

    typedef CacheItGridIntersection< Grid, typename HostIntersectionIterator::Intersection > IntersectionImpl;

  public:
    typedef Dune::Intersection< Grid, IntersectionImpl > Intersection;

    typedef typename Grid::template Codim< 0 >::EntityPointer EntityPointer;

    explicit CacheItGridIntersectionIterator ( const HostIntersectionIterator &hostIterator )
    : intersection_( IntersectionImpl() ),
      hostIterator_( hostIterator )
    {}

    CacheItGridIntersectionIterator ( const This &other )
    : intersection_( IntersectionImpl() ),
      hostIterator_( other.hostIterator_ )
    {}

    const CacheItGridIntersectionIterator &operator= ( const This &other )
    {
      intersectionImpl() = IntersectionImpl();
      hostIterator_ = other.hostIterator_;
      return *this;
    }

    bool equals ( const This &other ) const
    {
      return (hostIterator_ == other.hostIterator_);
    }

    void increment ()
    {
      ++hostIterator_;
      intersectionImpl() = IntersectionImpl();
    }

    const Intersection &dereference () const
    {
      if( !intersectionImpl() )
        intersectionImpl() = IntersectionImpl( *hostIterator_ );
      return intersection_;
    }

  private:
    IntersectionImpl &intersectionImpl () const
    {
      return Grid::getRealImplementation( intersection_ );
    }

    mutable Intersection intersection_;
    HostIntersectionIterator hostIterator_;
  };

} // namespace Dune

#endif // #ifndef DUNE_CACHEITGRID_INTERSECTIONITERATOR_HH
