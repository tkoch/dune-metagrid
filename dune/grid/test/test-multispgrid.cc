#include <config.h>

#include <iostream>
#include <string>

#include <dune/common/mpihelper.hh>

#include <dune/grid/multispgrid.hh>
#include <dune/grid/multispgrid/dgfparser.hh>

#include <dune/grid/io/visual/grapegriddisplay.hh>

#include "checkcalls.cc"

int main (int argc , char **argv)
try
{
  MPIHelper::instance( argc, argv );

  if( argc < 2 )
  {
    std::cerr << "Usage: " << argv[ 0 ] << " <dgf file>" << std::endl;
    return 1;
  }

  const int dimension = GRIDDIM;
  typedef Dune::MultiSPGrid< double, dimension > Grid;

  std::string filename( argv[1] );
  GridPtr< Grid > gridPtr( filename );
  Grid &grid = *gridPtr;

  const bool display = (argc > 2);
  checkSerial( grid, 1, display );

  return 0;
}
catch( Dune::Exception &e )
{
  std::cerr << e << std::endl;
  return 1;
}
catch (...)
{
  std::cerr << "Generic exception!" << std::endl;
  return 2;
}
