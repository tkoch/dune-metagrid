#include <iostream>
#include <sstream>
#include <string>

#include <dune/grid/idgrid.hh>
#include <dune/grid/io/file/dgfparser/dgfwriter.hh>

#include <dune/grid/test/gridcheck.hh>

#include <dune/grid/test/checkgeometryinfather.hh>
#include <dune/grid/test/checkintersectionit.hh>
#include <dune/grid/test/checkcommunicate.hh>

using namespace Dune;

template <class GridType>
void makeNonConfGrid(GridType &grid,int level,int adapt)
{
  int myrank = grid.comm().rank();
  grid.loadBalance();
  grid.globalRefine(level);
  grid.loadBalance();
  for (int i=0;i<adapt;i++)
  {
    if (myrank==0)
    {
      typedef typename GridType :: template Codim<0> ::
            template Partition<Interior_Partition> :: LeafIterator LeafIterator;

      LeafIterator endit = grid.template leafend<0,Interior_Partition>   ();
      int nr = 0;
      int size = grid.size(0);
      for(LeafIterator it    = grid.template leafbegin<0,Interior_Partition> ();
          it != endit ; ++it,nr++ )
      {
        grid.mark(1, *it );
        if (nr>size*0.8) break;
      }
    }
    grid.adapt();
    grid.postAdapt();
    grid.loadBalance();
  }
}

template <class GridView>
void writeFile( const GridView& gridView )
{
  DGFWriter< GridView > writer( gridView );
  writer.write( "dump.dgf" );
}

template <class GridType>
void checkSerial(GridType & grid, int mxl = 2, const bool display = false)
{
  // be careful, each global refine create 8 x maxlevel elements
  std::cout << "  CHECKING: Macro" << std::endl;
  gridcheck(grid);
  std::cout << "  CHECKING: Macro-intersections" << std::endl;
  checkIntersectionIterator(grid);

  for(int i=0; i<mxl; i++)
  {
    grid.globalRefine( 1 );//DGFGridInfo<GridType> :: refineStepsForHalf() );
    std::cout << "  CHECKING: Refined" << std::endl;
    gridcheck(grid);
    std::cout << "  CHECKING: intersections" << std::endl;
    checkIntersectionIterator(grid);
  }

  // check also non-conform grids
  makeNonConfGrid(grid,0,1);

  std::cout << "  CHECKING: non-conform" << std::endl;
  gridcheck(grid);
  std::cout << "  CHECKING: twists " << std::endl;

  // check the method geometryInFather()
  std::cout << "  CHECKING: geometry in father" << std::endl;
  checkGeometryInFather(grid);
  // check the intersection iterator and the geometries it returns
  std::cout << "  CHECKING: intersections" << std::endl;
  checkIntersectionIterator(grid);

  std::cout << std::endl << std::endl;
}

template <class GridType>
void checkParallel(GridType & grid, int gref, int mxl = 3, const bool display = false )
{
#if HAVE_MPI
  makeNonConfGrid(grid,gref,mxl);

  // -1 stands for leaf check
  checkCommunication(grid, -1, std::cout);

  mxl = std::min( mxl, grid.maxLevel() );
  for(int l=0; l<= mxl; ++l)
    checkCommunication(grid, l , Dune::dvverb);
#endif
}
