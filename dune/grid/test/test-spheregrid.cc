#include <config.h>

#include <iostream>
#include <sstream>
#include <string>

#include <dune/common/parallel/mpihelper.hh>

#include <dune/grid/spheregrid.hh>
#include <dune/grid/io/file/dgfparser/dgfwriter.hh>

#include <dune/grid/io/visual/grapegriddisplay.hh>

#include "checkcalls.cc"

namespace Dune
{

  template< int dim, int dimworld >
  class AlbertaGrid;

}

template< int dim, int dimworld >
struct EnableLevelIntersectionIteratorCheck< Dune::AlbertaGrid< dim, dimworld > >
{
  static const bool v = false;
};

template< class HostGrid, class MapToSphere >
struct EnableLevelIntersectionIteratorCheck< Dune::SphereGrid< HostGrid, MapToSphere > >
{
  static const bool v = EnableLevelIntersectionIteratorCheck< HostGrid >::v;
};


using namespace Dune;

int main (int argc , char **argv)
try {
  MPIHelper &mpihelper = MPIHelper::instance( argc, argv );
  int myrank = mpihelper.rank();
  int mysize = mpihelper.size();

  /* use grid-file appropriate for dimensions */

  if( argc < 2 )
  {
    std::cerr << "Usage: " << argv[ 0 ] << " <dgf file of hostgrid>" << std::endl;
    return 1;
  }

  const bool display = (argc > 2);

  typedef Dune::GridSelector::GridType HostGrid;
  std::string filename( argv[1] );
  GridPtr< HostGrid > hostGrid( filename );

  SphereGrid< HostGrid > grid( *hostGrid, 0.5 );

  //grid.loadBalance();

  {
    std::cout << "Check serial grid" << std::endl;
    checkSerial(grid,
                   (mysize == 1) ? 1 : 0,
                   (mysize == 1) ? display: false);
  }

  // perform parallel check only when more then one proc
  if(mysize > 1)
  {
    if (myrank == 0) std::cout << "Check conform grid" << std::endl;
    checkParallel(grid,1,0, display );
    if (myrank == 0) std::cout << "Check non-conform grid" << std::endl;
    checkParallel(grid,0,2, display );
  }

  return 0;
}
catch( Dune::Exception &e )
{
  std::cerr << e << std::endl;
  return 1;
}
catch (...)
{
  std::cerr << "Generic exception!" << std::endl;
  return 2;
}
