#if ! DUNE_GRID_EXPERIMENTAL_GRID_EXTENSIONS
#error  FilteredGrid is only available if experimental grid \
        extensions are activated. To enable the new \
        behaviour, build your Dune with \
        --enable-experimental-grid-extensions.
#else
#include<dune/grid/filteredgrid/grid.hh>
#endif
