#ifndef DUNE_HOSTGRID_INOUTSTREAMS_HH
#define DUNE_HOSTGRID_INOUTSTREAMS_HH

#include <type_traits>

#if HAVE_DUNE_ALUGRID
#include <dune/alugrid/common/interfaces.hh>
#endif // #if HAVE_DUNE_ALUGRID

namespace Dune
{

  // HasInOutStreams
  // ---------------

  template< class HostGrid, class SFINAE = void >
  struct HostGridHasInOutStreams
  {};

#if HAVE_DUNE_ALUGRID
  //! if in and out stream are present
  template< class HostGrid >
  struct HostGridHasInOutStreams< HostGrid, std::enable_if_t< std::is_base_of< HasObjectStream, HostGrid >::value > >
    : public HasObjectStream
  {
    //! type of out stream for communication
    typedef typename HostGrid::OutStreamType OutStreamType;

    //! type of in  stream for communication
    typedef typename HostGrid::InStreamType  InStreamType;
  };
#endif // #if HAVE_DUNE_ALUGRID

} // namespace Dune

#endif // #ifndef DUNE_CACHEITGRID_BACKUPRESTORE_HH
