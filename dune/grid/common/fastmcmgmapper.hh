// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FASTMCMGMAPPER_HH
#define DUNE_FASTMCMGMAPPER_HH

#include <iostream>

#include <dune/geometry/referenceelements.hh>
#include <dune/geometry/type.hh>
#include <dune/geometry/typeindex.hh>

#include <dune/grid/common/mcmgmapper.hh>

namespace Dune
{

  template< typename GV, template< int > class Layout >
  class FastMCMGMapper
  : public Mapper< typename GV::Grid, FastMCMGMapper< GV, Layout > >
  {
    typedef Mapper< typename GV::Grid, FastMCMGMapper< GV, Layout > > Base;

  public:
    // the following lines need to be skipped for intel compilers, because they
    // lead to ambiguous calls to methods
#ifndef __INTEL_COMPILER
    using Base::map;
    using Base::contains;
#endif

    FastMCMGMapper ( const GV &gridView, const Layout< GV::dimension > &layout )
    : indexSet_( gridView.indexSet() ),
      offset( GV::dimension+1 ),
      layout_( layout )
    {
      update();
    }

    explicit FastMCMGMapper ( const GV &gridView )
    : indexSet_( gridView.indexSet() ),
      offset( GV::dimension+1 )
    {
      update();
    }

    template< class Entity >
    int map ( const Entity &e ) const
    {
      const GeometryType gt = e.type();
      return indexSet_.index( e ) + offset[ Entity::codimension ][ LocalGeometryTypeIndex::index( gt ) ];
    }

    int map ( const typename GV::template Codim< 0 >::Entity &e, int i, unsigned int codim ) const
    {
      GeometryType gt = ReferenceElements< double, GV::dimension >::general( e.type() ).type( i, codim );
      return indexSet_.index( e ) + offset[ codim ][ LocalGeometryTypeIndex::index( gt ) ];
    }

    int size () const
    {
      return n;
    }

    template< class Entity >
    bool contains ( const Entity &e, int &result ) const
    {
      if( !indexSet_.contains( e ) || !layout_.contains( e.type() ) )
      {
        result = 0;
        return false;
      }
      result = map(e);
      return true;
    }

    bool contains ( const typename GV::template Codim< 0 >::Entity &e, int i, int codim, int &result ) const
    {
      result = this->map( e, i, codim );
      return true;
    }

    void update ()
    {
      // clear all maps, set size to 0
      n = 0;
      for( int codim = 0; codim <= GV::dimension; ++codim )
      {
        const int mydim = GV::dimension - codim;

        // resize offset array to number of different topologies in the codimension
        const size_t size = LocalGeometryTypeIndex::size( mydim );
        offset[ codim ].resize( size );

        // clear offsets (to the invalid value -1)
        std::fill( offset[ codim ].begin(), offset[ codim ].end(), -1 );

        // walk over all geometry types in the codimension
        typedef std::vector< GeometryType > GTV;
        const GTV &gtv = indexSet_.geomTypes( codim );
        for( typename GTV::const_iterator it = gtv.begin(); it != gtv.end(); ++it )
        {
          // if the geometry type is contained in the layout, generate an index
          if( layout_.contains( *it ) )
          {
            int &o = offset[ codim ][ LocalGeometryTypeIndex::index( *it ) ];
            assert( o == -1 ); // make sure we did not already assign an index
            o = n;
            n += indexSet_.size( *it );
          }
        }
      }
    }

  private:
    int n;
    const typename GV::IndexSet &indexSet_;
    std::vector< std::vector< int > > offset;
    Layout< GV::dimension > layout_;
  };

} // namespace Dune

#endif // #ifndef DUNE_FASTMCMGMAPPER_HH
