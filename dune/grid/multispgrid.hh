#if ! HAVE_DUNE_SPGRID
#error MultiSPGrid depends on Dune-SPGrid. See \
       ./configure --help and config.log for reasons \
       why Dune-SPGrid was not found.
#endif // #if ! HAVE_DUNE_SPGRID

#if ! DUNE_GRID_EXPERIMENTAL_GRID_EXTENSIONS
#error  MultiSPGrid is only available if experimental grid \
        extensions are activated. To enable the new \
        behaviour, build your Dune with \
        --enable-experimental-grid-extensions.
#endif // #if ! DUNE_GRID_EXPERIMENTAL_GRID_EXTENSIONS

#include <dune/grid/multispgrid/grid.hh>
