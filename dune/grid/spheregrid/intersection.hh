#ifndef DUNE_SPHEREGRID_INTERSECTION_HH
#define DUNE_SPHEREGRID_INTERSECTION_HH

#include <utility>

#include <dune/grid/spheregrid/declaration.hh>
#include <dune/grid/spheregrid/entity.hh>
#include <dune/grid/spheregrid/geometry.hh>

namespace Dune
{

  // SphereGridIntersection
  // ----------------------

  template< class Grid, class HostIntersection >
  class SphereGridIntersection
  {
    typedef typename std::remove_const_t< Grid >::Traits Traits;

  public:
    typedef typename Traits::ctype ctype;

    typedef typename Traits::MapToSphere MapToSphere;

    static const int dimension = Traits::dimension;
    static const int mydimension = Traits::dimension-1;
    static const int dimensionworld = Traits::dimensionworld;

    typedef typename Traits::template Codim< 0 >::Entity Entity;
    typedef typename Traits::template Codim< 1 >::Geometry Geometry;
    typedef typename Traits::template Codim< 1 >::LocalGeometry LocalGeometry;

  private:
    typedef SphereGridEntity< 0, dimension, Grid > EntityImpl;
    typedef SphereGridGeometry< mydimension, dimensionworld, Grid > GeometryImpl;

    struct Storage
      : public MapToSphere
    {
      Storage () = default;

      Storage ( HostIntersection hostIntersection, MapToSphere mapToSphere )
        : MapToSphere( std::move( mapToSphere ) ), hostIntersection( std::move( hostIntersection ) )
      {}

      HostIntersection hostIntersection;
    };

  public:
    SphereGridIntersection () = default;

    SphereGridIntersection ( HostIntersection hostIntersection, MapToSphere mapToSphere )
      : storage_( std::move( hostIntersection ), std::move( mapToSphere ) )
    {}

    explicit operator bool () const { return bool( storage_.hostIntersection ); }

    Entity inside () const { return Entity( EntityImpl( hostIntersection().inside(), mapToSphere() ) ); }
    Entity outside () const { return Entity( EntityImpl( hostIntersection().outside(), mapToSphere() ) ); }

    bool boundary () const { return hostIntersection().boundary(); }

    bool conforming () const { return hostIntersection().conforming(); }

    bool neighbor () const { return hostIntersection().neighbor(); }

    std::size_t boundarySegmentIndex () const { return hostIntersection().boundarySegmentIndex(); }

    LocalGeometry geometryInInside () const { return hostIntersection().geometryInInside(); }
    LocalGeometry geometryInOutside () const { return hostIntersection().geometryInOutside(); }

    Geometry geometry () const
    {
      const typename Entity::Geometry geo = inside().geometry();
      std::array< FieldVector< ctype, dimensionworld >, mydimension+1 > corners;
      for( int i = 0; i <= mydimension; ++i )
        corners[ i ] = geo.global( geometryInInside().corner( i ) );
      return Geometry( GeometryImpl( corners, mapToSphere().radius() ) );
    }

    GeometryType type () const
    {
      // as only triangular host grids are supported, we overwrite the type for
      // performance reasons
      assert( hostIntersection().type() == GeometryTypes::simplex( mydimension ) );
      return GeometryTypes::simplex( mydimension );
    }

    int indexInInside () const { return hostIntersection().indexInInside(); }
    int indexInOutside () const { return hostIntersection().indexInOutside(); }

    FieldVector< ctype, dimensionworld >
    integrationOuterNormal ( const FieldVector< ctype, dimension-1 > &local ) const
    {
      const ReferenceElement< ctype, dimension > &refElement
        = ReferenceElements< ctype, dimension >::simplex();
      const FieldVector< ctype, dimension > &refNormal = refElement.integrationOuterNormal( indexInInside() );

      const FieldVector< ctype, dimension > x( geometryInInside().global( local ) );

      FieldVector< ctype, dimensionworld > normal;
      const typename Entity::Geometry geo = inside().geometry();
      geo.jacobianInverseTransposed( x ).mv( refNormal, normal );
      normal *= geo.integrationElement( x );
      return normal;
    }

    FieldVector< ctype, dimensionworld >
    outerNormal ( const FieldVector< ctype, dimension-1 > &local ) const
    {
      const ReferenceElement< ctype, dimension > &refElement
        = ReferenceElements< ctype, dimension >::simplex();
      const FieldVector< ctype, dimension > &refNormal = refElement.integrationOuterNormal( indexInInside() );

      const FieldVector< ctype, dimension > x( geometryInInside().global( local ) );

      FieldVector< ctype, dimensionworld > normal;
      const typename Entity::Geometry geo = inside().geometry();
      geo.jacobianInverseTransposed( x ).mv( refNormal, normal );
      return normal;
    }

    FieldVector< ctype, dimensionworld >
    unitOuterNormal ( const FieldVector< ctype, dimension-1 > &local ) const
    {
      FieldVector< ctype, dimensionworld > normal = outerNormal( local );
      normal *= ctype( 1 ) / normal.two_norm();
      return normal;
    }

    FieldVector< ctype, dimensionworld > centerUnitOuterNormal () const
    {
      const auto &refElement = ReferenceElements< ctype, dimension >::simplex();
      const FieldVector< ctype, dimension > &refNormal = refElement.integrationOuterNormal( indexInInside() );

      const FieldVector< ctype, dimension > x( geometryInInside().center() );

      FieldVector< ctype, dimensionworld > normal;
      const typename Entity::Geometry geo = inside().geometry();
      geo.jacobianInverseTransposed( x ).mv( refNormal, normal );
      normal *= ctype( 1 ) / normal.two_norm();
      return normal;
    }

    const HostIntersection &hostIntersection () const { return storage_.hostIntersection; }

    const MapToSphere &mapToSphere () const { return storage_; }

  private:
    Storage storage_;
  };

} // namespace Dune

#endif // #ifndef DUNE_SPHEREGRID_INTERSECTION_HH
