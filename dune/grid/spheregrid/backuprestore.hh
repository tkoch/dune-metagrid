#ifndef DUNE_SPHEREGRID_BACKUPRESTORE_HH
#define DUNE_SPHEREGRID_BACKUPRESTORE_HH

#include <dune/grid/common/backuprestore.hh>

#include <dune/grid/spheregrid/capabilities.hh>

namespace Dune
{

  // BackupRestoreFacility for SphereGrid
  // ------------------------------------

  template< class HostGrid, class MapToSphere >
  struct BackupRestoreFacility< SphereGrid< HostGrid, MapToSphere > >
  {
    typedef SphereGrid< HostGrid, MapToSphere > Grid;
    typedef BackupRestoreFacility< HostGrid > HostBackupRestoreFacility;

    static void backup ( const Grid &grid, const std::string &path, const std::string &fileprefix )
    {
      HostBackupRestoreFacility::backup( grid.hostGrid(), path, fileprefix );
    }

    static void backup ( const Grid &grid, const std::ostream &stream )
    {
      HostBackupRestoreFacility::backup( grid.hostGrid(), stream );
    }

    static Grid *restore ( const std::string &path, const std::string &fileprefix )
    {
      return HostBackupRestoreFacility::restore( path, fileprefix );
    }

    static Grid *restore ( const std::istream &stream )
    {
      return HostBackupRestoreFacility::restore( stream );
    }
  };

} // namespace Dune

#endif // #ifndef DUNE_SPHEREGRID_BACKUPRESTORE_HH
