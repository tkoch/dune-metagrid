#ifndef DUNE_SPHEREGRID_AFFINESIMPLEXGEOMETRY_HH
#define DUNE_SPHEREGRID_AFFINESIMPLEXGEOMETRY_HH

#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>

#include <dune/geometry/type.hh>
#include <dune/geometry/affinegeometry.hh>

namespace Dune
{

  // AffineSimplexGeometryTraits
  // ---------------------------

  template< class ct >
  struct AffineSimplexGeometryTraits
  {
    typedef Dune::Impl::FieldMatrixHelper< ct > MatrixHelper;

    struct UserData {};
  };



  // AffineSimplexGeometry
  // ---------------------

  template< class ct, int mydim, int cdim, class Traits = AffineSimplexGeometryTraits< ct > >
  class AffineSimplexGeometry
  {
    typedef AffineSimplexGeometry< ct, mydim, cdim, Traits > This;

  public:
    typedef ct ctype;

    static const int mydimension= mydim;
    static const int coorddimension = cdim;

    typedef typename Traits::UserData UserData;

    typedef FieldVector< ctype, mydimension > LocalCoordinate;
    typedef FieldVector< ctype, coorddimension > GlobalCoordinate;

    typedef FieldMatrix< ctype, mydimension, coorddimension > JacobianTransposed;
    typedef FieldMatrix< ctype, coorddimension, mydimension > JacobianInverseTransposed;

    // for compatibility, export the type JacobianInverseTransposed as Jacobian
    typedef JacobianInverseTransposed Jacobian;

  protected:
    typedef typename Traits::MatrixHelper MatrixHelper;

  private:
    struct Storage
    : public UserData
    {
      Storage ( const GlobalCoordinate &org, const JacobianTransposed &jt, const UserData &userData )
      : UserData( userData ),
        origin( org ),
        jacobianTransposed( jt )
      {
        integrationElement = MatrixHelper::template rightInvA< mydimension, coorddimension >( jacobianTransposed, jacobianInverseTransposed );
      }

      template< class CoordVector >
      Storage ( const CoordVector &coordVector, const UserData &userData )
      : UserData( userData ),
        origin( coordVector[ 0 ] )
      {
        for( int i = 0; i < mydimension; ++i )
          jacobianTransposed[ i ] = coordVector[ i+1 ] - origin;
        integrationElement = MatrixHelper::template rightInvA< mydimension, coorddimension >( jacobianTransposed, jacobianInverseTransposed );
      }

      GlobalCoordinate origin;
      JacobianTransposed jacobianTransposed;
      JacobianInverseTransposed jacobianInverseTransposed;
      ctype integrationElement;
    };

  public:
    AffineSimplexGeometry ( const GlobalCoordinate &origin, const JacobianTransposed &jt, const UserData &userData = UserData() )
    : storage_( origin, jt, userData )
    {}

    template< class CoordVector >
    AffineSimplexGeometry ( const CoordVector &coordVector, const UserData &userData = UserData() )
    : storage_( coordVector, userData )
    {}

    bool affine () const { return true; }

    GeometryType type () const { return GeometryTypes::simplex( mydimension ); }

    int corners () const { return (mydimension+1); }

    GlobalCoordinate corner ( int i ) const
    {
      GlobalCoordinate corner( storage().origin );
      if( i > 0 )
        corner += storage().jacobianTransposed[ i-1 ];
      return corner;
    }

    GlobalCoordinate center () const
    {
      GlobalCoordinate center( storage().origin );
      for( int i = 0; i < mydimension; ++i )
        center.axpy( ctype( 1 ) / ctype( corners() ), storage().jacobianTransposed[ i ] );
      return center;
    }

    GlobalCoordinate global ( const LocalCoordinate &local ) const
    {
      GlobalCoordinate global( storage().origin );
      storage().jacobianTransposed.umtv( local, global );
      return global;
    }

    LocalCoordinate local ( const GlobalCoordinate &global ) const
    {
      LocalCoordinate local;
      storage().jacobianInverseTransposed.mtv( global - storage().origin, local );
      return local;
    }

    ctype integrationElement ( const LocalCoordinate &local ) const
    {
      return storage().integrationElement;
    }

    ctype volume () const
    {
      return storage().integrationElement / ctype( Factorial< mydimension >::factorial );
    }

    const JacobianTransposed &jacobianTransposed ( const LocalCoordinate &local ) const
    {
      return storage().jacobianTransposed;
    }

    const JacobianInverseTransposed &jacobianInverseTransposed ( const LocalCoordinate &local ) const
    {
      return storage().jacobianInverseTransposed;
    }

    const UserData &userData () const { return storage_; }
    UserData &userData () { return storage_; }

  protected:
    const Storage &storage () const { return storage_; }
    Storage &storage () { return storage_; }

  private:
    Storage storage_;
  };

} // namespace Dune

#endif // #ifndef DUNE_SPHEREGRID_AFFINESIMPLEXGEOMETRY_HH
