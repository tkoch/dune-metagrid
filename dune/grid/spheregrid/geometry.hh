#ifndef DUNE_SPHEREGRID_GEOMETRY_HH
#define DUNE_SPHEREGRID_GEOMETRY_HH

#include <cmath>

#include <dune/geometry/dimension.hh>

#include <dune/grid/common/geometry.hh>

#include <dune/grid/spheregrid/affinesimplexgeometry.hh>

namespace Dune
{

  // Internal Forward Declarations
  // -----------------------------

  template< int, int, class > class SphereGridGeometry;



  // sphericalVolume
  // ---------------

  template< class ctype >
  inline static ctype sphericalVolume ( const FieldVector< ctype, 3 > &org, const FieldMatrix< ctype, 0, 3 > &jt, const ctype radius2 )
  {
    return ctype( 1 );
  }

  template< class ctype >
  inline static ctype sphericalVolume ( const FieldVector< ctype, 3 > &org, const FieldMatrix< ctype, 1, 3 > &jt, const ctype radius2 )
  {
    return std::acos( 1 + org * jt[ 0 ] / radius2 );
  }

  template< class ctype >
  inline static ctype sphericalVolume ( const FieldVector< ctype, 3 > &org, const FieldMatrix< ctype, 2, 3 > &jt, const ctype radius2 )
  {
    // see Oosterom und Strackee (IEEE Trans. Biom. Eng., Vol BME-30, Nr 2, 1983)
    const double det = org[ 0 ] * (jt[ 0 ][ 1 ] * jt[ 1 ][ 2 ] - jt[ 0 ][ 2 ] * jt[ 1 ][ 1 ])
                     + org[ 1 ] * (jt[ 0 ][ 2 ] * jt[ 1 ][ 0 ] - jt[ 0 ][ 0 ] * jt[ 1 ][ 2 ])
                     + org[ 2 ] * (jt[ 0 ][ 0 ] * jt[ 1 ][ 1 ] - jt[ 0 ][ 1 ] * jt[ 1 ][ 0 ]);
    const double denominator = jt[ 0 ]*jt[ 1 ] + ctype( 2 )*(org*(jt[ 0 ] + jt[ 1 ])) + ctype( 4 )*radius2;
    return ctype( 2 ) * std::atan2( std::abs( det ), denominator );
  }

  template< class ctype, int dim >
  inline static ctype sphericalVolume ( const FieldVector< ctype, 3 > &org, const FieldMatrix< ctype, dim, 3 > &jt )
  {
    return sphericalVolume( org, jt, org.two_norm2() );
  }



  // SphereGridGeometry
  // ------------------

  template< int mydim, int cdim, class Grid >
  class SphereGridGeometry
    : public AffineSimplexGeometry< typename std::remove_const_t< Grid >::ctype, mydim, cdim >
  {
    typedef AffineSimplexGeometry< typename std::remove_const_t< Grid >::ctype, mydim, cdim > Base;

    typedef typename std::remove_const_t< Grid >::HostGrid HostGrid;

  public:
    typedef typename HostGrid::template Codim< HostGrid::dimension - mydim >::Entity HostEntity;
    typedef typename HostGrid::template Codim< HostGrid::dimension - mydim >::Geometry HostGeometry;

    static const int dimension = Base::dimension;
    static const int mydimension = Base::mydimension;
    static const int coorddimension = Base::coorddimension;
    static const int dimensionworld = Base::dimensionworld;

    typedef typename Base::ctype ctype;
    typedef typename Base::LocalCoordinate LocalVector;
    typedef typename Base::GlobalCoordinate GlobalVector;

    typedef typename Base::JacobianTransposed JacobianTransposed;
    typedef typename Base::JacobianInverseTransposed JacobianInverseTransposed;

    typedef typename std::remove_const_t< Grid >::Traits::MapToSphere MapToSphere;

  private:
    typedef typename Base::MatrixHelper MatrixHelper;

  public:
    template< class CoordVector >
    SphereGridGeometry ( const CoordVector &coordVector, ctype radius )
    : Base( coordVector ),
      radius_( radius )
    {}

    bool affine () const { return false; }

    GlobalVector center () const
    {
      GlobalVector center( Base::center() );
      center *= radius_ / center.two_norm();
      return center;
    }

    GlobalVector global ( const LocalVector &local ) const
    {
      GlobalVector global( Base::global( local ) );
      global *= radius_ / global.two_norm();
      return global;
    }

    LocalVector local ( const GlobalVector &global ) const
    {
      const GlobalVector normal = planeNormal( Dune::Dim< mydimension >() );
      GlobalVector x( global );
      x *= (normal*storage().origin) / (normal*global);
      return Base::local( x );
    }

    ctype integrationElement ( const LocalVector &local ) const
    {
      return MatrixHelper::template sqrtDetAAT< mydimension, coorddimension >( jacobianTransposed( local ) );
    }

    ctype volume () const
    {
      return sphericalVolume( storage().origin, storage().jacobianTransposed, radius_*radius_ );
    }

    JacobianTransposed jacobianTransposed ( const LocalVector &local ) const
    {
      GlobalVector y = Base::global( local );
      const ctype y_abs = y.two_norm();
      y /= y_abs;

      JacobianTransposed jt = storage().jacobianTransposed;
      for( int i = 0; i < mydimension; ++i )
        jt[ i ].axpy( -(y*jt[ i ]), y );
      jt *= radius_ / y_abs;
      return jt;
    }

    JacobianInverseTransposed jacobianInverseTransposed ( const LocalVector &local ) const
    {
      JacobianInverseTransposed jit;
      MatrixHelper::template rightInvA< mydimension, coorddimension >( jacobianTransposed( local ), jit );
      return jit;
    }

  protected:
    using Base::storage;

    GlobalVector planeNormal ( Dune::Dim< 2 > ) const
    {
      GlobalVector normal;
      const GlobalVector &a = storage().jacobianTransposed[ 0 ];
      const GlobalVector &b = storage().jacobianTransposed[ 1 ];
      for( int i = 0; i < 3; ++i )
        normal[ i ] = a[ (i+1)%3 ]*b[ (i+2)%3 ] - a[ (i+2)%3 ]*b[ (i+1)%3 ];
      return normal;
    }

    GlobalVector planeNormal ( Dune::Dim< 1 > ) const { return center(); }
    GlobalVector planeNormal ( Dune::Dim< 0 > ) const { return storage().origin; }

  private:
    ctype radius_;
  };

} // namespace Dune

#endif // #ifndef DUNE_SPHEREGRID_GEOMETRY_HH
