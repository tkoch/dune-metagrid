#ifndef DUNE_SPHEREGRID_CAPABILITIES_HH
#define DUNE_SPHEREGRID_CAPABILITIES_HH

#include <dune/geometry/type.hh>

#include <dune/grid/common/capabilities.hh>

#include <dune/grid/spheregrid/declaration.hh>

namespace Dune
{

  // Capabilities
  // ------------

  namespace Capabilities
  {

    template< class HostGrid, class MapToSphere >
    struct hasSingleGeometryType< SphereGrid< HostGrid, MapToSphere > >
    {
      static constexpr bool v = true;
      static constexpr unsigned int topologyId = GeometryTypes::simplex( HostGrid::dimension ).id();
    };


    template< class HostGrid, class MapToSphere >
    struct isCartesian< SphereGrid< HostGrid, MapToSphere > >
    {
      static const bool v = false;
    };


    template< class HostGrid, class MapToSphere, int codim >
    struct hasEntity< SphereGrid< HostGrid, MapToSphere >, codim >
    {
      static const bool v = hasEntity< HostGrid, codim >::v;
    };


    template< class HostGrid, class MapToSphere, int codim >
    struct canCommunicate< SphereGrid< HostGrid, MapToSphere >, codim >
    {
      static const bool v = canCommunicate< HostGrid, codim >::v;
    };


    template< class HostGrid, class MapToSphere >
    struct hasBackupRestoreFacilities< SphereGrid< HostGrid, MapToSphere > >
    {
      static const bool v = hasBackupRestoreFacilities< HostGrid >::v;
    };

    template< class HostGrid, class MapToSphere >
    struct isLevelwiseConforming< SphereGrid< HostGrid, MapToSphere > >
    {
      static const bool v = isLevelwiseConforming< HostGrid >::v;
    };

    template< class HostGrid, class MapToSphere >
    struct isLeafwiseConforming< SphereGrid< HostGrid, MapToSphere > >
    {
      static const bool v = isLeafwiseConforming< HostGrid >::v;
    };

    template< class HostGrid, class MapToSphere >
    struct threadSafe< SphereGrid< HostGrid, MapToSphere > >
    {
      static const bool v = false;
    };

    template< class HostGrid, class MapToSphere >
    struct viewThreadSafe< SphereGrid< HostGrid, MapToSphere > >
    {
      static const bool v = false;
    };

  } // namespace Capabilities

} // namespace Dune

#endif // #ifndef DUNE_SPHEREGRID_CAPABILITIES_HH
