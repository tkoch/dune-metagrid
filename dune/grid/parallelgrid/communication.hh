#ifndef DUNE_PARALLELGRID_COMMUNICATION_HH
#define DUNE_PARALLELGRID_COMMUNICATION_HH

#include <vector>

#include <dune/common/applyif.hh>
#include <dune/common/forloop.hh>

#include <dune/grid/common/gridenums.hh>

#include <dune/grid/parallelgrid/capabilities.hh>
#include <dune/grid/parallelgrid/rankmanager.hh>

#if HAVE_DUNE_ALUGRID

namespace Dune
{

  // ParallelGridCommunication
  // -------------------------

  template< class Grid >
  class ParallelGridCommunication
  {
    typedef ParallelGridCommunication< Grid > This;

    // prohibit copying and assignment
    ParallelGridCommunication ( const This & );
    const This &operator= ( const This & );

    typedef ParallelGridRankManager< Grid > RankManager;

    typedef typename RankManager::HostGrid HostGrid;
    typedef typename RankManager::ObjectStream ObjectStream;
    typedef typename RankManager::IdType  IdType;

    typedef ALU3DSPACE ALUMpAccess   MpAccess;

    template< int codim >
    struct CodimAvailable
    {
      static const bool value = Capabilities::hasEntity< Grid, codim >::v;
    };

    template< int codim >
    struct PackData;

    template< int codim >
    struct UnpackData;

  public:
    static const int dimension = Grid::dimension;

    ParallelGridCommunication ( const RankManager &rankManager )
    : rankManager_( rankManager )
    {}

    template< class PackIterator, class UnpackIterator, class DataHandleImp >
    void operator() ( const PackIterator packBegin, const PackIterator packEnd,
                      const UnpackIterator unpackBegin, const UnpackIterator unpackEnd,
                      DataHandleImp &dataHandle,
                      const PartitionType sendType, const PartitionType recvType,
                      const bool packAll ) const
    {
      const int nlinks = mpAccess().nlinks();

      std::vector< ObjectStream > sizeBuffers;
      std::vector< int > count;

#ifndef NDEBUG
      sizeBuffers.resize( nlinks );
      count.resize( nlinks, 0 );
#endif

      // vector of message buffers
      std::vector< ObjectStream > buffer( nlinks );
      for( int link = 0; link < nlinks; ++link )
        buffer[ link ].clear();

      // pack data on send entities
      for( PackIterator it = packBegin; it != packEnd; ++it )
      {
        const typename PackIterator::Entity &entity = *it;
        const PartitionInfo &info = rankManager_.partitionInfo( entity.impl().hostEntity() );

        if( (info.partitionType() == sendType) && info.hasConnectivity() )
        {
          ForLoop< ApplyIf< PackData, CodimAvailable >::template Operation, 0, dimension >
            ::apply( mpAccess(), rankManager_, dataHandle, buffer, entity, count );
        }
      }

      if( packAll )
      {
        // pack data on receive entities
        for( UnpackIterator it = unpackBegin; it != unpackEnd; ++it )
        {
          const typename UnpackIterator::Entity &entity = *it;
          const PartitionInfo &info = rankManager_.partitionInfo( entity.impl().hostEntity() );

          if( (info.partitionType() == recvType) && info.hasConnectivity() )
          {
            ForLoop< ApplyIf< PackData, CodimAvailable >::template Operation, 0, dimension >
              ::apply( mpAccess(), rankManager_, dataHandle, buffer, entity, count );
          }
        }
      }

      std::vector< int > sizes;
#ifndef NDEBUG
      for( int link = 0; link < nlinks; ++link )
      {
        sizeBuffers[ link ].clear();
        sizeBuffers[ link ].write( count[ link ] );
      }

      // exchange data
      sizeBuffers = mpAccess().exchange( sizeBuffers );

      sizes.resize( nlinks, 0 );
      for( int link = 0; link < nlinks; ++link )
      {
        sizeBuffers[ link ].read( sizes[ link ] );
        count[ link ] = 0;
      }
#endif

      // exchange data
      buffer = mpAccess().exchange( buffer );

      // unpack data on receive entities
      for( UnpackIterator it = unpackBegin; it != unpackEnd; ++it )
      {
        const typename UnpackIterator::Entity &entity = *it;
        const PartitionInfo &info = rankManager_.partitionInfo( entity.impl().hostEntity() );

        if( (info.partitionType() == recvType) && info.hasConnectivity() )
        {
          ForLoop< ApplyIf< UnpackData, CodimAvailable >::template Operation, 0, dimension >
            ::apply( mpAccess(), rankManager_, dataHandle, buffer, entity, count, sizes );
        }
      }

      if( packAll )
      {
        // unpack data on send entities
        for( PackIterator it = packBegin; it != packEnd; ++it )
        {
          const typename PackIterator::Entity &entity = *it;
          const PartitionInfo &info = rankManager_.partitionInfo( entity.impl().hostEntity() );

          // if entity is not send type, don't do anything
          if( (info.partitionType() == sendType) && info.hasConnectivity() )
          {
            ForLoop< ApplyIf< UnpackData, CodimAvailable >::template Operation, 0, dimension >
              ::apply( mpAccess(), rankManager_, dataHandle, buffer, entity, count, sizes );
          }
        }
      }

#ifndef NDEBUG
      // make sure that we read exactly the same size as was comitted
      for( int link = 0; link < nlinks; ++link )
      {
        if( count[ link ] != sizes[ link ] )
        {
          std::cout << "Error: count[ " << link << " ] = " << count[ link ]
                    << " != " << sizes[ link ] << " = sizes[ " << link << " ]." << std::endl;
        }
        assert( count[ link ] == sizes[ link ] );
      }
#endif
    }

  private:
    const MpAccess &mpAccess () const { return rankManager_.mpAccess(); }

    const RankManager &rankManager_;
  };



  // ParallelGridCommunication::PackData
  // -----------------------------------

  template< class Grid >
  template< int codim >
  struct ParallelGridCommunication< Grid >::PackData
  {
    typedef typename Grid::template Codim< 0 >::Entity Element;

    typedef typename Grid::template Codim< codim >::EntityPointer EntityPointer;
    typedef typename Grid::template Codim< codim >::Entity Entity;

    template< class DataHandleIF >
    static void apply ( const MpAccess &mpAccess, const RankManager &rankManager,
                        DataHandleIF &dataHandle, std::vector< ObjectStream > &buffer,
                        const Element &element,
                        std::vector< int > &count )
    {
      // if codim is not contained just go on
      if( !dataHandle.contains( HostGrid::dimension, codim ) )
        return;

      const bool variableSize = !dataHandle.fixedsize( HostGrid::dimension, codim );

      const int numSubEntities = element.template count< codim >();
      for( int subEntity = 0; subEntity < numSubEntities; ++subEntity )
      {
        // get subentity
        EntityPointer entityPtr = element.template subEntity< codim >( subEntity );
        const Entity &entity = *entityPtr;

        // get partition info
        const PartitionInfo &info = rankManager.partitionInfo( entity.impl().hostEntity() );

        const typename PartitionInfo::iterator end = info.end();
        for( typename PartitionInfo::iterator it = info.begin(); it != end; ++it )
        {
          const int destRank = (*it);
          const int link = mpAccess.link( destRank );

#ifndef NDEBUG
          ++count[ link ];
#endif

          // if size varies between entities, write it to the stream
          if( variableSize )
            buffer[ link ].write( std::size_t( dataHandle.size( entity ) ) );

          // write data to message buffer using data handle
          dataHandle.gather( buffer[ link ], entity );
        }
      }
    }
  };



  // ParallelGridCommunication::UnpackData
  // -------------------------------------

  template< class Grid >
  template< int codim >
  struct ParallelGridCommunication< Grid >::UnpackData
  {
    typedef typename Grid::template Codim< 0 >::Entity Element;

    typedef typename Grid::template Codim< codim >::EntityPointer EntityPointer;
    typedef typename Grid::template Codim< codim >::Entity Entity;

    template< class DataHandleIF >
    static void apply ( const MpAccess &mpAccess, const RankManager &rankManager,
                        DataHandleIF &dataHandle, std::vector< ObjectStream > &buffer,
                        const Element &element,
                        std::vector< int > &count, const std::vector< int > &sizes )
    {
      // if codim is not contained just go on
      if( !dataHandle.contains( dimension, codim ) )
        return;

      // check for variable size
      const bool variableSize = !dataHandle.fixedsize( dimension, codim );

      // get number of sub entities
      const int numSubEntities = element.template count< codim >();
      for( int subEntity = 0; subEntity < numSubEntities; ++subEntity )
      {
        // get subentity
        EntityPointer entityPtr = element.template subEntity< codim >( subEntity );
        const Entity &entity = *entityPtr;

        // get partition info
        const PartitionInfo &info = rankManager.partitionInfo( entity.impl().hostEntity() );

        const typename PartitionInfo::iterator end = info.end();
        for( typename PartitionInfo::iterator it = info.begin(); it != end; ++it )
        {
          const int srcRank = (*it);
          const int link = mpAccess.link( srcRank );

#ifndef NDEBUG
          ++count[ link ];
          assert( count[ link ] <= sizes[ link ] );
#endif

          // if size varies then read size from stream
          std::size_t size( 0 );
          if( variableSize )
            buffer[ link ].read( size );
          else
            size = dataHandle.size( entity );

          // read data from message buffer using data handle
          dataHandle.scatter( buffer[ link ], entity, size );
        }
      }
    }
  };

} // namespace Dune

#endif // #if HAVE_DUNE_ALUGRID

#endif // #ifndef DUNE_PARALLELGRID_COMMUNICATION_HH
