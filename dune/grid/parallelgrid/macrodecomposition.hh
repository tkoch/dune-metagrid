#ifndef DUNE_PARALLELGRID_MACRODECOMPOSITION_HH
#define DUNE_PARALLELGRID_MACRODECOMPOSITION_HH

#include <algorithm>
#include <fstream>
#include <iostream>
#include <limits>
#include <vector>

#include <dune/common/typetraits.hh>

namespace Dune
{

  template< class Grid >
  class ParallelGridMacroDecomposition
  {
    typedef ParallelGridMacroDecomposition< Grid > This;

    typedef typename remove_const< Grid >::type::Traits Traits;

    typedef typename Traits::HostGrid HostGrid;

    typedef typename HostGrid::LevelGridView HostMacroView;

    typedef typename HostMacroView::IndexSet HostMacroIndexSet;
    typedef typename HostMacroView::template Codim< 0 >::Iterator HostMacroIterator;
    typedef typename HostMacroView::template Codim< 0 >::Entity HostElement;

  public:
    ParallelGridMacroDecomposition ( const HostGrid &hostGrid )
    : hostMacroView_( hostGrid.levelGridView( 0 ) ),
      hostMacroIndexSet_( &hostMacroView_.indexSet() ),
      rank_( hostMacroIndexSet().size( 0 ), 0 )
    {}

    ParallelGridMacroDecomposition ( const This &other )
    : hostMacroView_( other.hostMacroView_ ),
      hostMacroIndexSet_( &hostMacroView_.indexSet() ),
      rank_( hostMacroIndexSet().size( 0 ) )
    {
      assign( other );
    }

    const This &operator= ( const This &other )
    {
      hostMacroView_ = other.hostMacroView_;
      hostMacroIndexSet_ = &hostMacroView_.indexSet();
      rank_.resize( hostMacroIndexSet().size( 0 ) );
      assign( other );
      return *this;
    }

    template< class Decomposition >
    const This &operator= ( const Decomposition &decomposition )
    {
      assign( decomposition );
      return *this;
    }

    const This &operator= ( const int rank )
    {
      std::fill( rank_.begin(), rank_.end(), rank );
      return *this;
    }

    std::size_t index ( const HostElement &hostElement ) const
    {
      assert( hostMacroIndexSet().contains( hostElement ) );
      return hostMacroIndexSet().index( hostElement );
    }

    std::size_t index ( const typename Traits::template Codim< 0 >::Entity &element ) const
    {
      return index( Grid::template getHostEntity< 0 >( element ) );
    }

    int rank ( const HostElement &hostElement ) const
    {
      return rank_[ index( hostElement ) ];
    }

    int rank ( const typename Traits::template Codim< 0 >::Entity &element ) const
    {
      return rank( Grid::template getHostEntity< 0 >( element ) );
    }

    bool backup ( const std::string &filename ) const
    {
      std::ofstream out( filename.c_str() );
      if( !out )
      {
        std::cerr << "Couldn't open file '" << filename << "'." << std::endl;
        return false;
      }
      return backup( out );
    }

    bool backup( std::ostream& out ) const
    {
      unsigned int size = hostMacroIndexSet().size( 0 );
      out << size;
      for( unsigned int i = 0; i < size; ++i )
        out << " " << rank_[ i ];
      if( !out )
      {
        std::cerr << "Error writing stream." << std::endl;
        return false;
      }

      return true;
    }

    bool restore ( const std::string &filename )
    {
      std::ifstream in( filename.c_str() );
      if( !in )
      {
        std::cerr << "Couldn't open file '" << filename << "'." << std::endl;
        return false;
      }
      return restore( in );
    }

    bool restore( std::istream& in )
    {
      unsigned int size( std::numeric_limits< unsigned int >::max() );
      in >> size;
      unsigned int hostSetSize = hostMacroIndexSet().size( 0 );
      if( size != hostSetSize )
      {
        std::cerr << "Wrong number of entries in stream." << std::endl;
        return false;
      }

      for( unsigned int i = 0; i < size; ++i )
        in >> rank_[ i ];
      if( !in )
      {
        std::cerr << "Error reading stream." << std::endl;
        return false;
      }

      return true;
    }

  private:
    template< class Decomposition >
    void assign ( const Decomposition &decomposition )
    {
      const HostMacroIterator end = hostMacroView_.template end< 0 >();
      for( HostMacroIterator it = hostMacroView_.template begin< 0 >(); it != end; ++it )
      {
        const HostElement &hostElement = *it;
        rank_[ hostMacroIndexSet().index( hostElement ) ] = decomposition.rank( hostElement );
      }
    }

    const HostMacroIndexSet &hostMacroIndexSet () const { return *hostMacroIndexSet_; }

    HostMacroView hostMacroView_;
    const HostMacroIndexSet *hostMacroIndexSet_;
    std::vector< int > rank_;
  };

} // namespace Dune

#endif // #ifndef DUNE_PARALLELGRID_MACRODECOMPOSITION_HH
