#ifndef DUNE_PARALLELGRID_SYNC_HH
#define DUNE_PARALLELGRID_SYNC_HH

#include <dune/common/exceptions.hh>

namespace Dune
{

  // ObjectStreamSync
  // ----------------

  template< class ObjectStream >
  struct ObjectStreamSync
  {
    static const unsigned int syncMarker = 0x55AA33CC;

#ifndef NDEBUG
    explicit ObjectStreamSync ( bool sync = true ) : sync_( sync ) {}
#else // #ifndef NDEBUG
    explicit ObjectStreamSync ( bool sync = false ) : sync_( sync ) {}
#endif // #else // #ifndef NDEBUG

    template< class T >
    void write ( ObjectStream &stream, const T &value ) const
    {
      if( sync_ )
        stream.write( value );
    }

    void write ( ObjectStream &stream ) const { write( stream, syncMarker ); }

    template< class T >
    bool check ( ObjectStream &stream, const T &good, const T &bad = T() ) const
    {
      if( sync_ )
      {
        T value( bad );
        stream.read( value );
        return (value == good);
      }
      else
        return true;
    }

    bool check ( ObjectStream &stream ) const { return check( stream, syncMarker, ~syncMarker ); }

    template< class T >
    void ensure ( ObjectStream &stream, const T &good, const T &bad = T() ) const
    {
      if( sync_ )
      {
        T value( bad );
        stream.read( value );
        if( value != good )
          DUNE_THROW( IOError, "Stream is not synchronous (got: " << value << ", expected: " << good << ")." );
      }
    }

    void ensure ( ObjectStream &stream ) const { ensure( stream, syncMarker, ~syncMarker ); }

  private:
    bool sync_;
  };



  // Definitions for ObjectStreamSync
  // --------------------------------

  template< class ObjectStream >
  const unsigned int ObjectStreamSync< ObjectStream >::syncMarker;

} // namespace Dune

#endif // #ifndef DUNE_PARALLELGRID_SYNC_HH
