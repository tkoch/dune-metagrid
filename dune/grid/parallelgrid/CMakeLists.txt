install(
  FILES capabilities.hh
        communication.hh
        declaration.hh
        dgfparser.hh
        entity.hh
        grid.hh
        hostgridaccess.hh
        intersection.hh
        iterator.hh
        macrodecomposition.hh
        mpaccess.hh
        partitioner.hh
        persistentcontainer.hh
        rankmanager.hh
        sfciterator.hh
        sync.hh
        twistutility.hh
  DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dune/grid/parallelgrid)
