#ifndef DUNE_PARALLELGRID_ITERATOR_HH
#define DUNE_PARALLELGRID_ITERATOR_HH

#include <dune/geometry/referenceelements.hh>

#include <dune/grid/idgrid/iterator.hh>

#include <dune/grid/parallelgrid/rankmanager.hh>

#if HAVE_DUNE_ALUGRID

namespace Dune
{

  // ParallelGridIteratorChecked
  // ---------------------------

  template< class Grid, PartitionIteratorType pitype, class HostIterator >
  class ParallelGridIteratorChecked
  : public IdGridIterator< Grid, HostIterator >
  {
    typedef IdGridIterator< Grid, HostIterator > Base;

  protected:
    using Base::hostIterator_;

    using Base::data;
    using Base::releaseEntity;

  protected:
    typedef typename Base::ExtraData ExtraData;

    typedef ParallelGridRankManager< typename remove_const< Grid >::type > RankManager;

    void checkValid ()
    {
      for( ; hostIterator_ != endIterator_; ++hostIterator_ )
      {
        const typename HostIterator::Entity &hostEntity = *hostIterator_;

        if( !rankManager().validEntity( hostEntity ) )
          continue;

        // determine interior and ghost
        const PartitionInfo &info = rankManager().partitionInfo( hostEntity );
        switch( pitype )
        {
        case All_Partition:
          return;

        case Interior_Partition:
          if( info.partitionType() == InteriorEntity )
            return;
          break;

        case InteriorBorder_Partition:
        case Overlap_Partition:
        case OverlapFront_Partition:
          if( info.partitionType() != GhostEntity )
            return;
          break;

        case Ghost_Partition:
          if( info.partitionType() == GhostEntity )
            return;
          break;

        default:
          DUNE_THROW( InvalidStateException, "wrong PartitionIteratorType: " << pitype << "." );
        }
      }
    }

  public:
    // constructor for begin iterator
    ParallelGridIteratorChecked ( ExtraData data, const HostIterator &hostIterator, const HostIterator &endIterator )
    : Base( data, hostIterator ),
      endIterator_( endIterator )
    {
      checkValid();
    }

    // constructor for end iterator
    ParallelGridIteratorChecked ( ExtraData data, const HostIterator &endIterator )
    : Base( data, endIterator ),
      endIterator_( endIterator )
    {}

    void increment ()
    {
      ++hostIterator_;
      checkValid();
      releaseEntity();
    }

  protected:
    const RankManager &rankManager () const { return data()->rankManager(); }

    HostIterator endIterator_;
  };

} // namespace Dune

#endif // #if HAVE_DUNE_ALUGRID

#endif // #ifndef DUNE_PARALLELGRID_ITERATOR_HH
