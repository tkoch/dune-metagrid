#ifndef METAGRID_SFCITERATOR_HH
#define METAGRID_SFCITERATOR_HH

#include <vector>

#include <dune/grid/common/capabilities.hh>

namespace Dune
{

// Iterator following the iterator pattern
template <class GridView>
class SpaceFillingCurveIterator
{
  typedef typename GridView :: template Codim< 0 > :: Iterator      Iterator ;
  typedef typename GridView :: template Codim< 0 > :: EntityPointer EntityPointer ;
  typedef typename GridView :: template Codim< 0 > :: Entity        Entity ;

  typedef typename GridView :: Grid   Grid;

  static const bool isCartesian = Capabilities :: isCartesian< Grid > :: v;

  const GridView& gridView_;

  const Iterator end_;
  Iterator it_;

  EntityPointer current_;
  bool done_;

  std::vector<int> dir_;

public:

  SpaceFillingCurveIterator( const GridView& gridView )
    : gridView_( gridView ),
      end_( gridView.template end<0> () ),
      it_( end_ ),
      current_( end_ ),
      done_( true ),
	    dir_(GridView::dimension, -1)
  {}

  void first ()
  {
    it_ = gridView_.template begin<0> () ;
    if( it_ != end_ )
    {
      current_ = it_;
      if( isCartesian )
      {
        typedef typename GridView :: IntersectionIterator  IntersectionIterator ;
        IntersectionIterator endIit = gridView_.iend( *current_ );
        for( IntersectionIterator iit = gridView_.ibegin( *current_ ); iit != endIit ; ++ iit )
        {
          const typename GridView :: Intersection& intersection = *iit ;
          const int j =intersection.indexInInside();
          const int i = (j - (j%2) )/2;
          if ( ! intersection.boundary() )
          {
            dir_[ i ] = j;
          }
        }
      }
      done_ = false;
    }
    else
      done_ = true ;
  }

  void next()
  {
    if( isCartesian )
    {
      typedef typename GridView :: IntersectionIterator  IntersectionIterator ;
      IntersectionIterator endIit = gridView_.iend( *current_ );
      for( IntersectionIterator iit = gridView_.ibegin( *current_ ); iit != endIit ; ++ iit )
      {
        const typename GridView :: Intersection& intersection = *iit ;
        const int j = intersection.indexInInside();
        const int i = (j - (j%2) ) /2;
        if ( dir_[i] == j && intersection.neighbor() && ! intersection.boundary() )
        {
          for(int k = 0; k<i; ++k)
          {
            dir_[ k ] = (dir_[ k ] == 2*k ) ? 2*k+1 : 2*k ;
          }
          current_ = intersection.outside();
          return;
        }
      }
    }
    else
    {
      ++it_ ;
      if( it_ != end_ )
      {
        current_ = it_;
        return  ;
      }
    }

    done_ = true;
  }

  const Entity& item () const
  {
    assert( ! done() );
    return *current_;
  }

  bool done () const { return done_; }
};

} // end namespace Dune
#endif


