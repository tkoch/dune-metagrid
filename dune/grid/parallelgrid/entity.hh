#ifndef DUNE_PARALLELGRID_ENTITY_HH
#define DUNE_PARALLELGRID_ENTITY_HH

#include <dune/geometry/referenceelements.hh>

#include <dune/grid/idgrid/entity.hh>

namespace Dune
{

  // External Forward Declarations
  // -----------------------------

  template< class, class > class ParallelGridIntersection;
  template< class, class > class ParallelGridIntersectionIterator;

  template< class, PartitionIteratorType, class > class ParallelGridIteratorChecked;



  // ParallelGridEntity
  // ------------------

  /** \copydoc ParallelGridEntity
   *
   *  \nosubgrouping
   */
  template< int codim, int dim, class Grid >
  class ParallelGridEntity : public IdGridEntity< codim, dim, Grid >
  {
    typedef IdGridEntity< codim, dim, Grid > Base ;
  protected:
    typedef typename Base::HostGrid   HostGrid;
    typedef typename Base::ExtraData  ExtraData;

  public:
    using Base :: data ;
    using Base :: hostEntity ;

    /** \name Host Types
     *  \{ */

    //! type of corresponding host entity
    typedef typename Base :: HostEntity HostEntity ;

    /** \} */

  public:
    /** \name Construction, Initialization and Destruction
     *  \{ */

    /** \brief construct a null entity */
    explicit ParallelGridEntity ( ExtraData data )
    : Base( data )
    {}

    /** \brief construct an initialized entity
     *
     *  \param[in]  hostEntity  corresponding entity in the host grid
     *
     *  \note The reference to the host entity must remain valid  as long as
     *        this entity is in use.
     */
    ParallelGridEntity ( ExtraData data,
                         const HostEntity &hostEntity )
    : Base( data, hostEntity )
    {}

    /** \brief copy constructor */
    ParallelGridEntity ( const ParallelGridEntity &other )
    : Base( other )
    {}

    /** \name Methods Shared by Entities of All Codimensions
     *  \{ */

    /** \brief obtain the partition type of this entity */
    PartitionType partitionType () const
    {
      // return partitionType from rank manager
      return data()->rankManager().partitionType( hostEntity() );
    }
    /** \} */
  };

} // namespace Dune

#endif // #ifndef DUNE_PARALLELGRID_ENTITY_HH
