#ifndef DUNE_PARALLELGRID_MPACCESS_HH
#define DUNE_PARALLELGRID_MPACCESS_HH

//- dune-alugrid includes
#if HAVE_DUNE_ALUGRID
#include <dune/alugrid/impl/serial/serialize.h>
#include <dune/alugrid/3d/grid.hh>

namespace ALUGrid
{

  // but without any communication, this is needed to avoid
  // communication during the call of DataBase :: repartition
  class MpAccessNoMPI
#if HAVE_MPI
  : public MpAccessLocal
#endif // #if HAVE_MPI
  {
  public:
    template <class NoComm>
    MpAccessNoMPI( const NoComm& ) {}

    typedef MpAccessGlobal::minmaxsum_t  minmaxsum_t;
    typedef MpAccessLocal::NonBlockingExchange  NonBlockingExchange;

    virtual int psize() const { return 1; }
    virtual int nlinks() const { return 0; }
    int myrank () const { return 0; }

    virtual int barrier () const { return psize(); }
    virtual bool gmax (bool value) const { return value; }
    virtual int gmax (int value) const { return value; }
    virtual int gmin ( int value ) const { return value; }
    virtual int gsum ( int value ) const { return value; }
    virtual long gmax ( long value ) const { return value; }
    virtual long gmin ( long value ) const { return value; }
    virtual long gsum ( long value ) const { return value; }
    virtual double gmax ( double value ) const { return value; }
    virtual double gmin ( double value ) const { return value; }
    virtual double gsum ( double value ) const { return value; }
    virtual void gmax (double* in, int length, double* out) const { std::copy(in, in+length, out ); }
    virtual void gmin (double* in, int length, double* out) const { std::copy(in, in+length, out ); }
    virtual void gsum (double* in, int length, double* out) const { std::copy(in, in+length, out ); }
    virtual void gmax (int*,int,int*) const {  }
    virtual void gmin (int*,int,int*) const {  }
    virtual void gsum (int*,int,int*) const {  }
    virtual minmaxsum_t minmaxsum( double value ) const { return minmaxsum_t( value ); }
    virtual std::pair<double,double> gmax (std::pair<double,double> value ) const { return value; }
    virtual std::pair<double,double> gmin (std::pair<double,double> value ) const { return value; }
    virtual std::pair<double,double> gsum (std::pair<double,double> value ) const { return value; }
    virtual void bcast(int*,int, int) const { }
    virtual void bcast(char*,int, int) const { }
    virtual void bcast(double*,int, int) const { }
    virtual int exscan( int value ) const { return 0; }
    virtual int scan( int value ) const { return value; }

    virtual std::vector< int > gcollect ( int value ) const { return std::vector< int >( psize(), value ); }

    virtual std::vector< double > gcollect ( double value ) const { return std::vector< double >( psize(), value ); }

    virtual std::vector< std::vector< int > > gcollect ( const std::vector< int > &value ) const
    {
      return std::vector< std::vector< int > > (psize(), value);
    }

    virtual std::vector< std::vector< double > > gcollect ( const std::vector< double > &value ) const
    {
      return std::vector< std::vector< double > > ( psize(), value );
    }

    virtual std::vector< ObjectStream > gcollect ( const ObjectStream &os ) const
    {
      return std::vector< ObjectStream >( psize(), os );
    }

#if HAVE_MPI
    // return handle for non-blocking exchange and already do send operation
    virtual NonBlockingExchange* nonBlockingExchange ( const int tag,
                                                       const std::vector < ObjectStream > & ) const { return 0; }
    // return handle for non-blocking exchange
    virtual NonBlockingExchange* nonBlockingExchange ( const int tag ) const { return 0; }
#else // #if HAVE_MPI
    std::vector< int > dest () const { return std::vector< int >(); }
#endif // #else // #if HAVE_MPI

    void printLinkage ( std::ostream & ) const {}
    void removeLinkage () {}
    int link (int) const { return 0; }
    int insertRequestSymetric (std::set < int, std::less < int > >){ return 0; }

    virtual std::vector< std::vector< int > > exchange ( const std::vector< std::vector< int > > &v ) const
    {
      return v;
    }

    virtual std::vector< std::vector< double > > exchange ( const std::vector< std::vector< double > > &v ) const
    {
      return v;
    }

    virtual std::vector< std::vector< char > > exchange ( const std::vector< std::vector< char > > &v ) const
    {
      return v;
    }

    // exchange data and return new std::vector of object streams
    virtual std::vector< ObjectStream > exchange ( const std::vector < ObjectStream > &v ) const
    {
      return v;
    }
  };

#if HAVE_MPI
  // type of communicator
  typedef MpAccessMPI ALUMpAccess;
#else
  // empty communicator
  typedef MpAccessNoMPI ALUMpAccess;
#endif

} // namespace ALUGridSpace

#endif // #if HAVE_DUNE_ALUGRID

#endif // #ifndef DUNE_PARALLELGRID_MPACCESS_HH
