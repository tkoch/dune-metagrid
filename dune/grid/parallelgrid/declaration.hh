#ifndef DUNE_PARALLELGRID_DECLARATION_HH
#define DUNE_PARALLELGRID_DECLARATION_HH

namespace Dune
{

  template< class HostGrid >
  class ParallelGrid;

} // namespace

#endif // #ifndef DUNE_PARALLELGRID_DECLARATION_HH
