#ifndef DUNE_PARALLELGRID_BACKUPRESTORE_HH
#define DUNE_PARALLELGRID_BACKUPRESTORE_HH

#include <dune/grid/common/backuprestore.hh>
#include <dune/grid/utility/grapedataioformattypes.hh>

#include <dune/grid/parallelgrid/capabilities.hh>

#include <dune/grid/io/file/vtk/vtkwriter.hh>

namespace Dune
{

  // BackupRestoreFacilities
  // -----------------------

  template< class Grid, bool hasBackupRestoreFacilities = Capabilities::hasBackupRestoreFacilities< Grid > ::v >
  class ParallelGridBackupRestoreFacilities
  {};

  template< class Grid >
  class ParallelGridBackupRestoreFacilities< Grid, true >
  {
    typedef ParallelGridBackupRestoreFacilities< Grid, true > This;

  protected:
    ParallelGridBackupRestoreFacilities ()
    {}

  private:
    ParallelGridBackupRestoreFacilities ( const This & );
    This &operator= ( const This & );

  public:
    template< GrapeIOFileFormatType type >
    bool writeGrid ( const std::string &filename, double time ) const
    {
      return asImp().hostGrid().template writeGrid< type >( filename, time );
    }

    template< GrapeIOFileFormatType type >
    bool readGrid ( const std::string &filename, double &time )
    {
      const bool success
        = asImp().hostGrid().template readGrid< type >( filename, time );
      asImp().update();
      return success;
    }

  protected:
    const Grid &asImp () const
    {
      return static_cast< const Grid & >( *this );
    }

    Grid &asImp ()
    {
      return static_cast< Grid & >( *this );
    }
  };

  // BackupRestoreFacility for ParallelGrid
  // --------------------------------------

  template< class HostGrid >
  struct BackupRestoreFacility< ParallelGrid< HostGrid > >
  {
    typedef ParallelGrid< HostGrid > Grid;
    typedef BackupRestoreFacility< HostGrid > HostBackupRestoreFacility;

    static void backup ( const Grid &grid, const std::string &filename )
    {
      HostBackupRestoreFacility::backup( grid.hostGrid(), filename );
      std::string newfilename( filename + ".paragrid" );
      std::ofstream out( newfilename.c_str() );
      grid.backup( out );
    }

    static void backup ( const Grid &grid, std::ostream &stream )
    {
      HostBackupRestoreFacility::backup( grid.hostGrid(), stream );
      grid.backup( stream );
    }

    static Grid *restore ( const std::string &filename )
    {
      HostGrid* hostGrid = HostBackupRestoreFacility::restore( filename );
      if( ! hostGrid )
        DUNE_THROW(IOError,"HostGrid was not created!");
      std::string newfilename( filename + ".paragrid" );
      std::ifstream in ( newfilename.c_str() );
      Grid* grid = new Grid( hostGrid );
      grid->restore( in );
      return grid ;
    }

    static Grid *restore ( std::istream &stream )
    {
      HostGrid* hostGrid = HostBackupRestoreFacility::restore( stream );
      if( ! hostGrid )
        DUNE_THROW(IOError,"HostGrid was not created!");
      Grid* grid = new Grid( hostGrid );
      grid->restore( stream );
      return grid ;
    }
  };


} // namespace Dune

#endif // #ifndef DUNE_PARALLELGRID_BACKUPRESTORE_HH
