#ifndef DUNE_PARALLELGRID_GRID_HH
#define DUNE_PARALLELGRID_GRID_HH

#include <string>

#include <dune/common/nullptr.hh>

#include <dune/grid/alugrid/common/defaultindexsets.hh>
#include <dune/grid/common/grid.hh>
#include <dune/grid/common/sizecache.hh>

#include <dune/grid/idgrid/idset.hh>
#include <dune/grid/idgrid/geometry.hh>
#include <dune/grid/idgrid/intersectioniterator.hh>

#include <dune/grid/parallelgrid/backuprestore.hh>
#include <dune/grid/parallelgrid/capabilities.hh>
#include <dune/grid/parallelgrid/declaration.hh>
#include <dune/grid/parallelgrid/entity.hh>
#include <dune/grid/parallelgrid/gridview.hh>
#include <dune/grid/parallelgrid/intersection.hh>
#include <dune/grid/parallelgrid/iterator.hh>
#include <dune/grid/parallelgrid/rankmanager.hh>

#if HAVE_DUNE_ALUGRID

namespace Dune
{

  // ParallelGridExportParams
  // ------------------------

  template< class HG >
  struct ParallelGridExportParams
  {
    typedef HG HostGrid;
  };



  // ParallelGridFamily
  // ------------------

  template< class HostGrid >
  struct ParallelGridFamily
  {
    struct Traits
    : public ParallelGridExportParams< HostGrid >
    {
      typedef ParallelGrid< HostGrid > Grid;

      typedef typename HostGrid::ctype ctype;

      // type of data passed to entities, intersections, and iterators
      typedef const Grid *ExtraData;

      static const int dimension = HostGrid::dimension;
      static const int dimensionworld = HostGrid::dimensionworld;

      typedef ParallelGridIntersection< const Grid, typename HostGrid::LeafIntersection > LeafIntersectionImpl;
      typedef Dune::Intersection< const Grid, LeafIntersectionImpl > LeafIntersection;

      typedef ParallelGridIntersection< const Grid, typename HostGrid::LevelIntersection > LevelIntersectionImpl;
      typedef Dune::Intersection< const Grid, LevelIntersectionImpl > LevelIntersection;

      typedef IdGridIntersectionIterator< const Grid, typename HostGrid::LeafIntersectionIterator > LeafIntersectionIteratorImpl;
      typedef Dune::IntersectionIterator< const Grid, LeafIntersectionIteratorImpl, LeafIntersectionImpl > LeafIntersectionIterator;

      typedef IdGridIntersectionIterator< const Grid, typename HostGrid::LevelIntersectionIterator > LevelIntersectionIteratorImpl;
      typedef Dune::IntersectionIterator< const Grid, LevelIntersectionIteratorImpl, LevelIntersectionImpl > LevelIntersectionIterator;

      typedef ParallelGridIteratorChecked< const Grid, All_Partition, typename HostGrid::HierarchicIterator > HierarchicIteratorImpl;
      typedef Dune::EntityIterator< 0, const Grid, HierarchicIteratorImpl > HierarchicIterator;

      template< int codim >
      struct Codim
      {
        typedef typename HostGrid::template Codim<codim>::Geometry Geometry;
        typedef typename HostGrid::template Codim<codim>::LocalGeometry LocalGeometry;

        typedef IdGridEntityPointer< const Grid, typename HostGrid::template Codim< codim >::EntityPointer > EntityPointerImpl;
        typedef Dune::EntityPointer< const Grid, EntityPointerImpl > EntityPointer;

        typedef ParallelGridEntity< codim, dimension, const Grid > EntityImpl;
        typedef Dune::Entity< codim, dimension, const Grid, ParallelGridEntity > Entity;

        // minimal information to generate entities
        typedef typename HostGrid::template Codim< codim >::EntitySeed EntitySeed;

        template< PartitionIteratorType pitype >
        struct Partition
        {
          typedef ParallelGridIteratorChecked< const Grid, pitype, typename HostGrid::template Codim< codim >::LeafIterator > LeafIteratorImpl;
          typedef Dune::EntityIterator< codim, const Grid, LeafIteratorImpl > LeafIterator;

          typedef ParallelGridIteratorChecked< const Grid, pitype, typename HostGrid::template Codim< codim >::LevelIterator > LevelIteratorImpl;
          typedef Dune::EntityIterator< codim, const Grid, LevelIteratorImpl > LevelIterator;
        };

        typedef typename Partition< All_Partition >::LeafIterator LeafIterator;
        typedef typename Partition< All_Partition >::LevelIterator LevelIterator;
      };

      //! Type of the level index set
      typedef DefaultIndexSet< Grid, typename Codim< 0 >::LevelIterator > LevelIndexSet;
      //! Type of the leaf index set
      typedef DefaultIndexSet< Grid, typename Codim< 0 >::LeafIterator > LeafIndexSet;

      typedef IdGridIdSet< const Grid, typename HostGrid::Traits::GlobalIdSet > GlobalIdSet;
      typedef IdGridIdSet< const Grid, typename HostGrid::Traits::LocalIdSet > LocalIdSet;

#if HAVE_MPI
      typedef Dune::CollectiveCommunication< MPI_Comm > CollectiveCommunication;
#else
      typedef Dune::CollectiveCommunication< No_Comm > CollectiveCommunication;
#endif

      template< PartitionIteratorType pitype >
      struct Partition
      {
        typedef Dune::GridView< ParallelGridLeafViewTraits< HostGrid, pitype > > LeafGridView;
        typedef Dune::GridView< ParallelGridLevelViewTraits< HostGrid, pitype > > LevelGridView;
      };
    };
  };



  // ParallelGrid
  // ------------

  /** \class ParallelGrid
   *  \brief metagrid adding parallelism
   *  \ingroup ParallelGrid
   *
   *  \tparam  HostGrid   DUNE grid to be wrapped (called host grid)
   *
   *  \nosubgrouping
   */
  template< class HostGrid >
  class ParallelGrid
  /** \cond */
  : public GridDefaultImplementation
      < HostGrid::dimension, HostGrid::dimensionworld, typename HostGrid::ctype, ParallelGridFamily< HostGrid > >,
    public ParallelGridExportParams< HostGrid >,
    public ParallelGridBackupRestoreFacilities< ParallelGrid< HostGrid > >,
    public HasObjectStream
  /** \endcond */
  {
    typedef ParallelGrid< HostGrid > Grid;

    typedef GridDefaultImplementation
      < HostGrid::dimension, HostGrid::dimensionworld, typename HostGrid::ctype, ParallelGridFamily< HostGrid > >
      Base;

    template< int, int, class > friend class ParallelGridEntity;
    template< class, class > friend class ParallelGridEntityPointer;
    template< class, class > friend class ParallelGridIntersection;
    template< class, class > friend class ParallelGridIntersectionIterator;
    template< class, PartitionIteratorType > friend class ParallelGridView;
    template< class, class > friend class IdGridIdSet;
    template< class > friend class HostGridAccess;

  public:
    /** \cond */
    typedef ParallelGridFamily< HostGrid > GridFamily;
    /** \endcond */

    /** \name Traits
     *  \{ */

    //! type of the grid traits
    typedef typename GridFamily::Traits Traits;

    /** \brief traits structure containing types for a codimension
     *
     *  \tparam codim  codimension
     *
     *  \nosubgrouping
     */
    template< int codim >
    struct Codim;

    /** \} */

    /** \name Iterator Types
     *  \{ */

    //! iterator over the grid hierarchy
    typedef typename Traits::HierarchicIterator HierarchicIterator;
    //! iterator over intersections with other entities on the leaf level
    typedef typename Traits::LeafIntersectionIterator LeafIntersectionIterator;
    //! iterator over intersections with other entities on the same level
    typedef typename Traits::LevelIntersectionIterator LevelIntersectionIterator;

    /** \} */

    /** \name Grid View Types
     *  \{ */

    /** \brief Types for GridView */
    template< PartitionIteratorType pitype >
    struct Partition
    {
      typedef typename GridFamily::Traits::template Partition< pitype >::LevelGridView LevelGridView;
      typedef typename GridFamily::Traits::template Partition< pitype >::LeafGridView LeafGridView;
    };

    /** \brief View types for All_Partition */
    typedef typename Partition< All_Partition >::LevelGridView LevelGridView;
    typedef typename Partition< All_Partition >::LeafGridView LeafGridView;

    /** \} */

    /** \name Index and Id Set Types
     *  \{ */

    /** \brief type of leaf index set
     *
     *  The index set assigns consecutive indices to the entities of the
     *  leaf grid. The indices are of integral type and can be used to access
     *  arrays.
     *
     *  The leaf index set is a model of Dune::IndexSet.
     */
    typedef typename Traits::LeafIndexSet LeafIndexSet;

    /** \brief type of level index set
     *
     *  The index set assigns consecutive indices to the entities of a grid
     *  level. The indices are of integral type and can be used to access
     *  arrays.
     *
     *  The level index set is a model of Dune::IndexSet.
     */
    typedef typename Traits::LevelIndexSet LevelIndexSet;

    /** \brief type of global id set
     *
     *  The id set assigns a unique identifier to each entity within the
     *  grid. This identifier is unique over all processes sharing this grid.
     *
     *  \note Id's are neither consecutive nor necessarily of an integral
     *        type.
     *
     *  The global id set is a model of Dune::IdSet.
     */
    typedef typename Traits::GlobalIdSet GlobalIdSet;

    /** \brief type of local id set
     *
     *  The id set assigns a unique identifier to each entity within the
     *  grid. This identifier needs only to be unique over this process.
     *
     *  Though the local id set may be identical to the global id set, it is
     *  often implemented more efficiently.
     *
     *  \note Ids are neither consecutive nor necessarily of an integral
     *        type.
     *  \note Local ids need not be compatible with global ids. Also, no
     *        mapping from local ids to global ones needs to exist.
     *
     *  The global id set is a model of Dune::IdSet.
     */
    typedef typename Traits::LocalIdSet LocalIdSet;

    /** \} */

    /** \name Miscellaneous Types
     * \{ */

    //! type of vector coordinates (e.g., double)
    typedef typename Traits::ctype ctype;

    //! communicator with all other processes having some part of the grid
    typedef typename Traits::CollectiveCommunication CollectiveCommunication;

    /** \} */

    typedef Dune::SizeCache< Grid > SizeCache;

    //! type or rank manager making grid parallel
    typedef ParallelGridRankManager< Grid > RankManager;

    // we nend to specify the correct out stream (for writing)
    typedef typename RankManager::ObjectStream OutStreamType;

    // we nend to specify the correct in stream (for reading)
    typedef typename RankManager::ObjectStream InStreamType;

    /** \name Construction and Destruction
     *  \{ */

    /** \brief constructor
     *
     *  The references to host grid and coordinate function are stored in the
     *  grid. Therefore, they must remain valid until the grid is destroyed.
     *
     *  \param[in]  hostGrid       reference to the grid to wrap
     */
    explicit ParallelGrid ( HostGrid &hostGrid )
    : hostGrid_( &hostGrid ),
      owner_( false ),
      comm_( MPIHelper::getCommunicator() ),
      maxLevel_( hostGrid.maxLevel() ),
      levelIndexSets_( hostGrid.maxLevel()+1, (LevelIndexSet *)0 ),
      leafIndexSet_( 0 ),
      localIdSet_( hostGrid.localIdSet() ),
      rankManager_( *this ),
      geomTypes_(),
      sizeCache_( *this ),
      adaptMarkers_( hostGrid.leafIndexSet().size( 0 ), std::numeric_limits< int >::min() )
    {
      if( hostGrid.comm().size() > 1 )
        DUNE_THROW( InvalidStateException, "ParallelGrid can only be used with serial host grids." );

      for( int i = 0; i <= HostGrid::dimension; ++i )
        geomTypes_.push_back( hostGrid.levelGridView( 0 ).indexSet().geomTypes( i ) );

      sizeCache_.reset();
    }

    /** \brief constructor
     *
     *  The references to host grid and coordinate function are stored in the
     *  grid. Therefore, they must remain valid until the grid is destroyed.
     *
     *  \param[in]  hostGrid       pointer to the grid to wrap (ownership is taken)
     */
    explicit ParallelGrid ( HostGrid *hostGrid )
    : hostGrid_( hostGrid ),
      owner_( true ),
      comm_( MPIHelper::getCommunicator() ),
      maxLevel_( hostGrid->maxLevel() ),
      levelIndexSets_( hostGrid->maxLevel()+1, (LevelIndexSet *)0 ),
      leafIndexSet_( 0 ),
      localIdSet_( hostGrid->localIdSet() ),
      rankManager_( *this ),
      geomTypes_(),
      sizeCache_( *this ),
      adaptMarkers_( hostGrid->leafIndexSet().size( 0 ), std::numeric_limits< int >::min() )
    {
      if( hostGrid->comm().size() > 1 )
        DUNE_THROW( InvalidStateException, "ParallelGrid can only be used with serial host grids." );

      for( int i = 0; i <= HostGrid::dimension; ++i )
        geomTypes_.push_back( hostGrid->levelGridView( 0 ).indexSet().geomTypes( i ) );

      sizeCache_.reset();
    }

    /** \brief destructor */
    ~ParallelGrid ()
    {
      if( leafIndexSet_ )
        delete leafIndexSet_;

      const std::size_t numLevels = levelIndexSets_.size();
      for( std::size_t level = 0; level < numLevels; ++level )
      {
        if( levelIndexSets_[ level ] )
          delete( levelIndexSets_[ level ] );
      }

      if( owner_ )
        delete hostGrid_;
    }

    /** \} */

    /** \name Size Methods
     *  \{ */

    /** \brief obtain maximal grid level
     *
     *  Grid levels are numbered 0, ..., L, where L is the value returned by
     *  this method.
     *
     *  \returns maximal grid level
     */
    int maxLevel () const
    {
      return maxLevel_;
    }

    /** \brief obtain number of entites on a level
     *
     *  \param[in]  level  level to consider
     *  \param[in]  codim  codimension to consider
     *
     *  \returns number of entities of codimension \em codim on grid level
     *           \em level.
     */
    int size ( int level, int codim ) const
    {
      return sizeCache_.size( level, codim );
    }

    /** \brief obtain number of leaf entities
     *
     *  \param[in]  codim  codimension to consider
     *
     *  \returns number of leaf entities of codimension \em codim
     */
    int size ( int codim ) const
    {
      return sizeCache_.size( codim );
    }

    /** \brief obtain number of entites on a level
     *
     *  \param[in]  level  level to consider
     *  \param[in]  type   geometry type to consider
     *
     *  \returns number of entities with a geometry of type \em type on grid
     *           level \em level.
     */
    int size ( int level, GeometryType type ) const
    {
      return sizeCache_.size( level, type );
    }

    /** \brief returns the number of boundary segments within the macro grid
     *
     *  \returns number of boundary segments within the macro grid
     */
    int size ( GeometryType type ) const
    {
      return sizeCache_.size( type );
    }

    /** \brief obtain number of leaf entities
     *
     *  \param[in]  type   geometry type to consider
     *
     *  \returns number of leaf entities with a geometry of type \em type
     */
    size_t numBoundarySegments () const
    {
      return hostGrid().numBoundarySegments();
    }
    /** \} */

    template< int codim >
    typename Codim< codim >::LevelIterator lbegin ( int level ) const
    {
      return lbegin< codim, All_Partition >( level );
    }

    template< int codim >
    typename Codim< codim >::LevelIterator lend ( int level ) const
    {
      return lend< codim, All_Partition >( level );
    }

    template< int codim, PartitionIteratorType pitype >
    typename Codim< codim >::template Partition< pitype >::LevelIterator
    lbegin ( int level ) const
    {
      typedef typename Traits::template Codim< codim >::template Partition< pitype >::LevelIteratorImpl Impl;
      return Impl( extraData(), hostGrid().template lbegin< codim >( level ), hostGrid().template lend< codim >( level ) );
    }

    template< int codim, PartitionIteratorType pitype >
    typename Codim< codim >::template Partition< pitype >::LevelIterator
    lend ( int level ) const
    {
      typedef typename Traits::template Codim< codim >::template Partition< pitype >::LevelIteratorImpl Impl;
      return Impl( extraData(), hostGrid().template lend< codim >( level ) );
    }

    template< int codim >
    typename Codim< codim >::LeafIterator leafbegin () const
    {
      return leafbegin< codim, All_Partition >();
    }

    template< int codim >
    typename Codim< codim >::LeafIterator leafend () const
    {
      return leafend< codim, All_Partition >();
    }

    template< int codim, PartitionIteratorType pitype >
    typename Codim< codim >::template Partition< pitype >::LeafIterator
    leafbegin () const
    {
      typedef typename Traits::template Codim< codim >::template Partition< pitype >::LeafIteratorImpl Impl;
      return Impl( extraData(), hostGrid().template leafbegin< codim >(), hostGrid().template leafend< codim >() );
    }

    template< int codim, PartitionIteratorType pitype >
    typename Codim< codim >::template Partition< pitype >::LeafIterator
    leafend () const
    {
      typedef typename Traits::template Codim< codim >::template Partition< pitype >::LeafIteratorImpl Impl;
      return Impl( extraData(), hostGrid().template leafend< codim >() );
    }

    const GlobalIdSet &globalIdSet () const
    {
      if( !globalIdSet_ )
        globalIdSet_ = GlobalIdSet( hostGrid().globalIdSet() );
      assert( globalIdSet_ );
      return globalIdSet_;
    }

    const LocalIdSet &localIdSet () const
    {
      return localIdSet_;
    }

    const LevelIndexSet &levelIndexSet ( int level ) const
    {
      assert( levelIndexSets_.size() == std::size_t( maxLevel()+1 ) );
      if( (level < 0) || (level > maxLevel()) )
        DUNE_THROW( GridError, "LevelIndexSet for nonexisting level " << level << " requested." );

      LevelIndexSet *&levelIndexSet = levelIndexSets_[ level ];
      if( !levelIndexSet )
        levelIndexSet = new LevelIndexSet( *this, lbegin< 0 >( level ), lend< 0 >( level ), level );

      assert( levelIndexSet );
      return *levelIndexSet;
    }

    const LeafIndexSet &leafIndexSet () const
    {
      if( !leafIndexSet_ )
        leafIndexSet_ = new LeafIndexSet( *this, leafbegin< 0 >(), leafend< 0 >() );

      assert( leafIndexSet_ );
      return *leafIndexSet_;
    }

    void globalRefine ( int refCount )
    {
      if( refCount > 0 )
      {
        hostGrid().globalRefine( refCount );
        update();
      }
    }

    bool mark ( int refCount, const typename Codim< 0 >::Entity &entity )
    {
      assert( adaptMarkers_.size() == std::size_t( hostGrid().leafIndexSet().size( 0 ) ) );


      if( !hostGrid().leafIndexSet().contains( getHostEntity< 0 >( entity ) ) )
        return false;

      const std::size_t index = hostGrid().leafIndexSet().index( getHostEntity< 0 >( entity ) );
      if( !(adaptMarkers_[ index ] < refCount) )
        return false;

      adaptMarkers_[ index ] = refCount;
      return true;
    }

    int getMark ( const typename Codim< 0 >::Entity &entity ) const
    {
      assert( adaptMarkers_.size() == std::size_t( hostGrid().leafIndexSet().size( 0 ) ) );

      if( !hostGrid().leafIndexSet().contains( getHostEntity< 0 >( entity ) ) )
        return 0;

      const std::size_t index = hostGrid().leafIndexSet().index( getHostEntity< 0 >( entity ) );
      const int marker = adaptMarkers_[ index ];
      return (marker > std::numeric_limits< int >::min() ? marker : 0);
    }

    bool preAdapt ()
    {
      typedef typename HostGrid::LeafGridView HostLeafView;
      typedef typename HostGrid::LeafGridView::template Codim< 0 >::Iterator HostIterator;

      HostLeafView hostLeafView = hostGrid().leafGridView();

      // communicate adaptation markers globally
      if( comm().size() > 1 )
      {
        const std::size_t leafSize = hostLeafView.indexSet().size( 0 );
        std::vector< int > globalMarkers( leafSize );
        rankManager().mpAccess().gmax( &(adaptMarkers_[ 0 ]), leafSize, &(globalMarkers[ 0 ]) );
        std::swap( adaptMarkers_, globalMarkers );
      }

      // actually mark the host grid
      const HostIterator hostEnd = hostLeafView.template end< 0 >();
      for( HostIterator hostIt = hostLeafView.template begin< 0 >(); hostIt != hostEnd; ++hostIt )
      {
        const typename HostIterator::Entity &hostElement = *hostIt;
        int marker= adaptMarkers_[hostLeafView.indexSet().index( hostElement )];
        marker = marker > std::numeric_limits< int >::min() ? marker : 0;
        hostGrid().mark( marker , hostElement );
      }

      // finally, we call preAdapt on the host grid
      return hostGrid().preAdapt();
    }

    bool adapt ()
    {
      bool ret = hostGrid().adapt();
      update();
      return ret;
    }

    template< class RestrictProlongHandle >
    bool adapt ( RestrictProlongHandle& rphandle )
    {
      DUNE_THROW(NotImplemented,"adpat with restrictprolong handle not implemented!");
      return false;
    }

    void postAdapt ()
    {
      hostGrid().postAdapt();
    }

    /** \name Parallel Data Distribution and Communication Methods
     *  \{ */

    /** \brief obtain size of overlap region for the leaf grid
     *
     *  \param[in]  codim  codimension for with the information is desired
     */
    int overlapSize ( int codim ) const
    {
      return 0;
    }

    /** \brief obtain size of ghost region for the leaf grid
     *
     *  \param[in]  codim  codimension for with the information is desired
     */
    int ghostSize( int codim ) const
    {
      return (codim == 0 ? 1 : 0);
    }

    /** \brief obtain size of overlap region for a grid level
     *
     *  \param[in]  level  grid level (0, ..., maxLevel())
     *  \param[in]  codim  codimension (0, ..., dimension)
     */
    int overlapSize ( int level, int codim ) const
    {
      return 0;
    }

    /** \brief obtain size of ghost region for a grid level
     *
     *  \param[in]  level  grid level (0, ..., maxLevel())
     *  \param[in]  codim  codimension (0, ..., dimension)
     */
    int ghostSize ( int level, int codim ) const
    {
      return ghostSize( codim );
    }

    //! return geometry types for codim
    const std::vector< GeometryType > &geomTypes ( const int codim ) const
    {
      return geomTypes_[ codim ];
    }

    /** \brief communicate information on a grid level
     *
     *  \param      datahandle  communication data handle (user defined)
     *  \param[in]  interface   communication interface (one of
     *                          InteriorBorder_InteriorBorder_Interface,
     *                          InteriorBorder_All_Interface,
     *                          Overlap_OverlapFront_Interface,
     *                          Overlap_All_Interface,
     *                          All_All_Interface)
     *  \param[in]  direction   communication direction (one of
     *                          ForwardCommunication or BackwardCommunication)
     *  \param[in]  level       grid level to communicate
     */
    template< class DataHandle, class Data >
    void communicate ( CommDataHandleIF< DataHandle, Data > &datahandle,
                       InterfaceType interface,
                       CommunicationDirection direction,
                       int level ) const
    {
      levelGridView( level ).communicate( datahandle, interface, direction );
    }

    /** \brief communicate information on leaf entities
     *
     *  \param      datahandle  communication data handle (user defined)
     *  \param[in]  interface   communication interface (one of
     *                          InteriorBorder_InteriorBorder_Interface,
     *                          InteriorBorder_All_Interface,
     *                          Overlap_OverlapFront_Interface,
     *                          Overlap_All_Interface,
     *                          All_All_Interface)
     *  \param[in]  direction   communication direction (one of
     *                          ForwardCommunication, BackwardCommunication)
     */
    template< class DataHandle, class Data >
    void communicate ( CommDataHandleIF< DataHandle, Data > &datahandle,
                       InterfaceType interface,
                       CommunicationDirection direction ) const
    {
      leafGridView().communicate( datahandle, interface, direction );
    }

    //! backup macro decomposition
    void backup( std::ostream& out ) const
    {
      rankManager_.backup( out );
    }

    //! restore macro decomposition and update
    void restore( std::istream& in )
    {
      rankManager_.restore( in );
      update();
    }

    /** \brief obtain CollectiveCommunication object
     *
     *  The CollectiveCommunication object should be used to globally
     *  communicate information between all processes sharing this grid.
     *
     *  \note The CollectiveCommunication object returned is identical to the
     *        one returned by the host grid.
     */
    const CollectiveCommunication &comm () const
    {
      return comm_;
    }

    // data handle interface different between geo and interface

    /** \brief rebalance the load each process has to handle
     *
     *  A parallel grid is redistributed such that each process has about
     *  the same load (e.g., the same number of leaf entites).
     *
     *  \note DUNE does not specify, how the load is measured.
     *
     *  \returns \b true, if the grid has changed.
     */
    bool loadBalance ()
    {
      if( comm_.size() <= 1 )
        return false;

      const bool gridChanged = rankManager().loadBalance();
      if( gridChanged )
        update();
      return gridChanged;
    }

    /** \brief rebalance the load each process has to handle
     *
     *  A parallel grid is redistributed such that each process has about
     *  the same load (e.g., the same number of leaf entites).
     *
     *  The data handle is used to communicate the data associated with
     *  entities that move from one process to another.
     *
     *  \note DUNE does not specify, how the load is measured.
     *
     *  \param  dataHandle  communication data handle (user defined)
     *
     *  \returns \b true, if the grid has changed.
     */
    template< class DataHandle, class Data >
    bool loadBalance ( CommDataHandleIF< DataHandle, Data > &dataHandle )
    {
      if( comm_.size() <= 1 )
        return false;

      MetaGridDataHandle< DataHandle, GridFamily > wrappedDataHandle( extraData(), static_cast<DataHandle&>(dataHandle) );

      const bool gridChanged = rankManager().loadBalance( wrappedDataHandle );
      if( gridChanged )
        update();
      return gridChanged;
    }

    /** \brief View for a grid level */
    template< PartitionIteratorType pitype >
    typename Partition< pitype >::LevelGridView levelGridView ( int level ) const
    {
      typedef typename Partition< pitype >::LevelGridView View;
      typedef typename View::GridViewImp ViewImp;
      return View( ViewImp( *this, level ) );
    }

    /** \brief View for the leaf grid */
    template< PartitionIteratorType pitype >
    typename Partition< pitype >::LeafGridView leafGridView () const
    {
      typedef typename Traits::template Partition< pitype >::LeafGridView View;
      typedef typename View::GridViewImp ViewImp;
      return View( ViewImp( *this ) );
    }

    /** \brief View for a grid level for All_Partition */
    LevelGridView levelGridView ( int level ) const
    {
      typedef typename LevelGridView::GridViewImp ViewImp;
      return LevelGridView( ViewImp( *this, level ) );
    }

    /** \brief View for the leaf grid for All_Partition */
    LeafGridView leafGridView () const
    {
      typedef typename LeafGridView::GridViewImp ViewImp;
      return LeafGridView( ViewImp( *this ) );
    }


    /** \brief obtain EntityPointer from EntitySeed. */
    template< class EntitySeed >
    typename Traits::template Codim< EntitySeed::codimension >::EntityPointer
    entityPointer ( const EntitySeed &seed ) const
    {
      typedef typename Traits::template Codim< EntitySeed::codimension >::EntityPointerImpl EntityPointerImpl;
      return EntityPointerImpl( extraData(), hostGrid().entityPointer( seed ) );
    }

    /** \} */

    /** \name Miscellaneous Methods
     *  \{ */

    const HostGrid &hostGrid () const
    {
      return *hostGrid_;
    }

    HostGrid &hostGrid ()
    {
      return *hostGrid_;
    }

    /** \brief update grid caches
     *
     *  This method has to be called whenever the underlying host grid changes.
     *
     *  \note If you adapt the host grid through this geometry grid's
     *        adaptation or load balancing methods, update is automatically
     *        called.
     */
    void update ()
    {
      // get max level first
      maxLevel_ = hostGrid().maxLevel();
      assert( maxLevel_ == comm().max( maxLevel_ ) );

      // reset cached sizes
      sizeCache_.reset();

      // update rank manager
      rankManager().update();

      // the rest
      const int newNumLevels = maxLevel()+1;
      const int oldNumLevels = levelIndexSets_.size();

      // we need to change all index sets,
      // since during load balance also the macro level of
      // the grid can change
      for( int level = 0; level < oldNumLevels; ++level )
      {
        if( levelIndexSets_[ level ] )
          levelIndexSets_[ level ]->calcNewIndex( lbegin< 0 >( level ), lend< 0 >( level ) );
      }
      levelIndexSets_.resize( newNumLevels, nullptr );

      if( leafIndexSet_ )
        leafIndexSet_->calcNewIndex( leafbegin< 0 >(), leafend< 0 >() );

      std::vector< int > adaptMarkers( hostGrid().leafIndexSet().size( 0 ), std::numeric_limits< int >::min() );
      std::swap( adaptMarkers_, adaptMarkers );
    }

    void dumpVTK( const std::string &filename ) const
    {
      typedef typename HostGrid::LeafGridView GridView;
      VTKWriter< GridView > vtk( hostGrid().leafGridView() );
      vtk.write( filename, VTK::ascii, comm().rank(), comm().size() );
    }

    /** \} */

    RankManager &rankManager () { return rankManager_; }
    const RankManager &rankManager () const { return rankManager_; }

    // make these methods public
    using Base::getRealImplementation;

    template< int codim >
    static const typename HostGrid::template Codim< codim >::Entity &
    getHostEntity ( const typename Codim< codim >::Entity &entity )
    {
      return getRealImplementation( entity ).hostEntity();
    }

  protected:
    typedef typename Traits :: ExtraData ExtraData;
    //! return extra data passed to entities (here const Grid*)
    ExtraData extraData () const  { return this; }

    HostGrid *const hostGrid_;
    const bool owner_;
    CollectiveCommunication comm_;
    int maxLevel_;
    mutable std::vector< LevelIndexSet * > levelIndexSets_;
    mutable LeafIndexSet *leafIndexSet_;
    mutable GlobalIdSet globalIdSet_;
    mutable LocalIdSet localIdSet_;
    RankManager rankManager_ ;

    std::vector< std::vector< GeometryType > > geomTypes_;
    SizeCache sizeCache_;

    std::vector< int > adaptMarkers_;
  };



  // ParallelGrid::Codim
  // -------------------

  template< class HostGrid >
  template< int codim >
  struct ParallelGrid< HostGrid >::Codim
  : public Base::template Codim< codim >
  {
    /** \name Entity and Entity Pointer Types
     *  \{ */

    /** \brief type of entity
     *
     *  The entity is a model of Dune::Entity.
     */
    typedef typename Traits::template Codim< codim >::Entity Entity;

    /** \brief type of entity pointer
     *
     *  The entity pointer is a model of Dune::EntityPointer.
     */
    typedef typename Traits::template Codim< codim >::EntityPointer EntityPointer;

    /** \} */

    /** \name Geometry Types
     *  \{ */

    /** \brief type of world geometry
     *
     *  Models the geomtry mapping of the entity, i.e., the mapping from the
     *  reference element into world coordinates.
     *
     *  The geometry is a model of Dune::Geometry, implemented through the
     *  generic geometries provided by dune-grid.
     */
    typedef typename Traits::template Codim< codim >::Geometry Geometry;

    /** \brief type of local geometry
     *
     *  Models the geomtry mapping into the reference element of dimension
     *  \em dimension.
     *
     *  The local geometry is a model of Dune::Geometry, implemented through
     *  the generic geometries provided by dune-grid.
     */
    typedef typename Traits::template Codim< codim >::LocalGeometry LocalGeometry;

    /** \} */

    /** \name Iterator Types
     *  \{ */

    template< PartitionIteratorType pitype >
    struct Partition
    {
      typedef typename Traits::template Codim< codim >
        ::template Partition< pitype >::LeafIterator
        LeafIterator;
      typedef typename Traits::template Codim< codim >
        ::template Partition< pitype >::LevelIterator
        LevelIterator;
    };

    /** \brief type of level iterator
     *
     *  This iterator enumerates the entites of codimension \em codim of a
     *  grid level.
     *
     *  The level iterator is a model of Dune::LevelIterator.
     */
    typedef typename Partition< All_Partition >::LeafIterator LeafIterator;

    /** \brief type of leaf iterator
     *
     *  This iterator enumerates the entites of codimension \em codim of the
     *  leaf grid.
     *
     *  The leaf iterator is a model of Dune::LeafIterator.
     */
    typedef typename Partition< All_Partition >::LevelIterator LevelIterator;

    /** \} */
  };

} // namespace Dune

#endif // #if HAVE_DUNE_ALUGRID

#include <dune/grid/parallelgrid/hostgridaccess.hh>
#include <dune/grid/parallelgrid/persistentcontainer.hh>
#include <dune/grid/parallelgrid/twistutility.hh>

#endif // #ifndef DUNE_PARALLELGRID_GRID_HH
