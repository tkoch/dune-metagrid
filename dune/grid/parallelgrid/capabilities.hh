#ifndef DUNE_PARALLELGRID_CAPABILITIES_HH
#define DUNE_PARALLELGRID_CAPABILITIES_HH

#include <dune/grid/common/capabilities.hh>

#include <dune/grid/parallelgrid/declaration.hh>

namespace Dune
{

  // Capabilities
  // ------------

  namespace Capabilities
  {

    // Capabilities from dune-grid
    // ---------------------------

    template<class HostGrid>
    struct hasSingleGeometryType< ParallelGrid< HostGrid > >
    {
      static const bool v = hasSingleGeometryType< HostGrid >::v;
      static const unsigned int topologyId = hasSingleGeometryType< HostGrid >::topologyId;
    };

    template< class HostGrid >
    struct isCartesian< ParallelGrid< HostGrid > >
    {
      static const bool v = isCartesian< HostGrid >::v;
    };


    template< class HostGrid, int codim >
    struct hasEntity< ParallelGrid< HostGrid >, codim >
    {
      static const bool v = hasEntity< HostGrid, codim >::v;
    };


    template< class HostGrid >
    struct isParallel< ParallelGrid< HostGrid > >
    {
      static const bool v = isParallel< HostGrid >::v;
    };


    template< class HostGrid, int codim >
    struct canCommunicate< ParallelGrid< HostGrid >, codim >
    {
      static const bool v = canCommunicate< HostGrid, codim >::v;
    };

    template< class HostGrid >
    struct canCommunicate< ParallelGrid< HostGrid >, 0 >
    {
      static const bool v = true;
    };

    template< class HostGrid >
    struct hasBackupRestoreFacilities< ParallelGrid< HostGrid > >
    {
      static const bool v = hasBackupRestoreFacilities< HostGrid >::v;
    };

    template< class HostGrid >
    struct isLevelwiseConforming< ParallelGrid< HostGrid > >
    {
      static const bool v = isLevelwiseConforming< HostGrid >::v;
    };

    template< class HostGrid >
    struct isLeafwiseConforming< ParallelGrid< HostGrid > >
    {
      static const bool v = isLeafwiseConforming< HostGrid >::v;
    };

    template< class HostGrid >
    struct threadSafe< ParallelGrid< HostGrid > >
    {
      static const bool v = false;
    };

    template< class HostGrid >
    struct viewThreadSafe< ParallelGrid< HostGrid > >
    {
      static const bool v = false;
    };

  } // namespace Capabilities

} // namespace Dune

#endif // #ifndef DUNE_PARALLELGRID_CAPABILITIES_HH
