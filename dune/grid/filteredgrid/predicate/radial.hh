#ifndef DUNE_GRID_FILTEREDGRID_PREDICATE_RADIAL_HH
#define DUNE_GRID_FILTEREDGRID_PREDICATE_RADIAL_HH

#include <dune/common/fvector.hh>

namespace Dune
{

  // RadialPredicate
  // ---------------

  template< class HostGrid >
  class RadialPredicate
  {
    struct Ball;

  public:
    typedef typename HostGrid::ctype ctype;
    typedef Dune::FieldVector< ctype, HostGrid::dimensionworld > GlobalCoordinate;

    RadialPredicate ( const GlobalCoordinate &center, ctype radius )
    : ball_( center, radius )
    {}

    bool operator() ( const typename HostGrid::template Codim< 0 >::Entity &entity ) const
    {
      typedef typename HostGrid::template Codim< 0 >::Geometry Geometry;
      const Geometry geometry = entity.geometry();
      const int corners = geometry.corners();
      for( int i = 0; i < corners; ++i )
      {
        if( !ball().inside( geometry.corner( i ) ) )
          return false;
      }
      return true;
    }

  protected:
    const Ball &ball () const { return ball_; }

  private:
    Ball ball_;
  };



  // Implementation of RadialPredicate< HostGrid >::Ball
  // ---------------------------------------------------

  template< class HostGrid >
  struct RadialPredicate< HostGrid >::Ball
  {
    typedef typename RadialPredicate< HostGrid >::GlobalCoordinate GlobalCoordinate;

    typedef typename GlobalCoordinate::value_type ctype;
    static const int dimension = GlobalCoordinate::dimension;

    Ball ( const GlobalCoordinate &center, ctype radius )
    : center_( center ),
      radius_( radius )
    {}

    const GlobalCoordinate &center () const { return center_; }

    ctype radius () const { return radius_; }

    bool inside ( const GlobalCoordinate &x ) const
    {
      return ((x - center_).two_norm2() < radius()*radius());
    }

  private:
    GlobalCoordinate center_;
    ctype radius_;
  };

} // namespace Dune

#endif // #ifndef DUNE_GRID_FILTEREDGRID_PREDICATE_RADIAL_HH
