#ifndef DUNE_GRID_FILTEREDGRID_PREDICATE_KEEPALL_HH
#define DUNE_GRID_FILTEREDGRID_PREDICATE_KEEPALL_HH

namespace Dune
{

  // KeepAllPredicate
  // ----------------

  template< class HostGrid >
  struct KeepAllPredicate
  {
    bool operator() ( const typename HostGrid::template Codim< 0 >::Entity & ) const
    {
      return true;
    }
  };

}; // end namespace Dune

#endif // #ifndef DUNE_GRID_FILTEREDGRID_PREDICATE_KEEPALL_HH
