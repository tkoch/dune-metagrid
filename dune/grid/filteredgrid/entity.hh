#ifndef DUNE_GRID_FILTEREDGRID_ENTITY_HH
#define DUNE_GRID_FILTEREDGRID_ENTITY_HH

//- C++ includes
#include <cassert>

//- dune-common includes
#include <dune/common/nullptr.hh>
#include <dune/common/exceptions.hh>
#include <dune/common/typetraits.hh>

//- dune-grid includes
#include <dune/grid/common/entity.hh>


namespace Dune
{

  // FilteredEntityBase
  // ------------------

  template< int codim, int dim, class Grid >
  class FilteredEntityBase
  {
    typedef FilteredEntityBase< codim, dim, Grid > This;

  protected:
    typedef typename remove_const< Grid >::type::Traits Traits;

  public:
    /** \brief codimension */
    static const int codimension = codim;
    /** \brief grid dimension */
    static const int dimension = Traits::dimension;
    /** \brief my dimension */
    static const int mydimension = dimension - codimension;
    /** \brief world dimension */
    static const int dimensionworld = Traits::dimensionworld;

    /** \brief single coordinate type */
    typedef typename Traits::ctype ctype;

    /** \brief entity seed */
    typedef typename Traits::template Codim< codimension >::EntitySeed EntitySeed;

    /** \brief geometry type */
    typedef typename Traits::template Codim< codimension >::Geometry Geometry;

    /** \brief host grid view type */
    typedef typename Traits::HostGridView HostGridView;

    /** \brief type of host entity */
    typedef typename HostGridView::template Codim< codimension >::Entity HostEntity;

    /** \brief constructor */
    explicit FilteredEntityBase ( const Grid &grid )
    : hostEntity_( nullptr ),
      grid_( &grid )
    {}

    /** \brief constructor */
    explicit FilteredEntityBase ( const HostEntity &hostEntity, const Grid &grid )
    : hostEntity_( &hostEntity ),
      grid_( &grid )
    {}

    /** \brief return name of reference element */
    GeometryType type () const { return hostEntity().type(); }

    /** \brief return level */
    int level () const { return 0; }

    /** \brief return partition type */
    PartitionType partitionType () const { return hostEntity().partitionType(); }

    /** \brief return geometry */
    Geometry geometry () const { return hostEntity().geometry(); }

    /** \brief return seed */
    EntitySeed seed () const { return hostEntity().seed(); }

  public:
    /** \brief cast to bool */
    operator bool () const { return bool( hostEntity_ ); }

    /** \brief return host entity */
    const HostEntity &hostEntity () const
    {
      assert( *this );
      return *hostEntity_;
    }

    /** \brief return grid */
    const Grid &grid () const
    {
      assert( grid_ );
      return *grid_;
    }

    /** \brief reset internal pointer */
    void reset () { hostEntity_ = nullptr; }

    /** \brief reset internal pointer */
    void reset ( const HostEntity &hostEntity ) { hostEntity_ = &hostEntity; }

  private:
    const HostEntity *hostEntity_;
    const Grid *grid_;
  };



  // FilteredEntity
  // --------------

  template< int codim, int dim, class Grid >
  class FilteredEntity
  : public FilteredEntityBase< codim, dim, Grid >
  {
    typedef FilteredEntity< codim, dim, Grid > This;
    typedef FilteredEntityBase< codim, dim, Grid > Base;

  public:
    /** \brief type of host entity */
    typedef typename Base::HostEntity HostEntity;

    /** \brief constructor */
    explicit FilteredEntity ( const Grid &grid )
    : Base( grid )
    {}

    /** \brief constructor */
    FilteredEntity ( const HostEntity &hostEntity, const Grid &grid )
    : Base( hostEntity, grid )
    {}
  };



  // FilteredEntity for codimension 0
  // --------------------------------

  template< int dim, class Grid >
  class FilteredEntity< 0, dim, Grid >
  : public FilteredEntityBase< 0, dim, Grid >
  {
    typedef FilteredEntity< 0, dim, Grid > This;
    typedef FilteredEntityBase< 0, dim, Grid > Base;

  protected:
    typedef typename Base::Traits Traits;

  public:
    /** \brief local geometry type */
    typedef typename Traits::template Codim< 0 >::LocalGeometry LocalGeometry;
    /** \brief hierarchic iterator type */
    typedef typename Traits::HierarchicIterator HierarchicIterator;
    /** \brief leaf intersection type */
    typedef typename Traits::LeafIntersectionIterator LeafIntersectionIterator;
    /** \brief level intersection type */
    typedef typename Traits::LevelIntersectionIterator LevelIntersectionIterator;

    /** \brief type of host entity */
    typedef typename Base::HostEntity HostEntity;

  public:
    using Base::hostEntity;
    using Base::grid;
    using Base::level;

    /** \brief constructor */
    explicit FilteredEntity ( const Grid &grid )
    : Base( grid )
    {}

    /** \brief constructor */
    FilteredEntity ( const HostEntity &hostEntity, const Grid &grid )
    : Base( hostEntity, grid )
    {}

    /** \brief return number of sub entities */
    template< int codim >
    int count () const
    {
      return hostEntity().template count< codim >();
    }

    /** \brief return i-th sub entity */
    template< int codim >
    typename Traits::template Codim< codim >::EntityPointer
    subEntity ( int i ) const
    {
      typedef typename Traits::template Codim< codim >::EntityPointerImpl EntityPointerImpl;
      return EntityPointerImpl( hostEntity().template subEntity< codim >( i ), grid() );
    }

    /** \brief will throw an exception */
    LevelIntersectionIterator ilevelbegin () const
    {
      assert( level() == 0 );
      return ileafbegin();
    }

    /** \brief will throw an exception */
    LevelIntersectionIterator ilevelend () const
    {
      assert( level() == 0 );
      return ileafend();
    }

    /** \brief will throw an exception */
    LeafIntersectionIterator ileafbegin () const
    {
      typedef typename LeafIntersectionIterator::Implementation IteratorImpl;
      return IteratorImpl::begin( *this, grid() );
    }

    /** \brief will throw an exception */
    LeafIntersectionIterator ileafend () const
    {
      typedef typename LeafIntersectionIterator::Implementation IteratorImpl;
      return IteratorImpl::end( *this, grid() );
    }

    bool hasBoundaryIntersections () const
    {
      return grid().boundarySegmentIndexSet().hasBoundaryIntersections( hostEntity() );
    }

    bool isLeaf () const { return true; }

    typename Traits::template Codim< 0 >::EntityPointer father () const
    {
      DUNE_THROW( InvalidStateException, "FilteredGrid does not implement method father()" );
    }

    bool hasFather () const { return false; }

    /** \brief will throw an exception */
    LocalGeometry geometryInFather () const
    {
      DUNE_THROW( InvalidStateException, "FilteredGrid does not implement method geometryInFather()" );
    }

    HierarchicIterator hbegin ( int maxLevel ) const { return hend( maxLevel ); }
    HierarchicIterator hend ( int maxLevel ) const
    {
      typedef typename Traits::HierarchicIteratorImpl IteratorImpl;
      return IteratorImpl( hostEntity().hend( level() + maxLevel ), grid() );
    }

    bool isRegular () const { return true; }
    bool isNew () const { return false; }
    bool mightVanish () const { return false; }
  };

} // namespace Dune

#endif // #ifndef DUNE_GRID_FILTEREDGRID_ENTITY_HH
