#ifndef DUNE_GRID_FILTEREDGRID_CAPABILITIES_HH
#define DUNE_GRID_FILTEREDGRID_CAPABILITIES_HH

//- dune-grid includes
#include <dune/grid/common/capabilities.hh>

//- dune-metagrid includes
#include <dune/grid/filteredgrid/declaration.hh>

namespace Dune
{

  namespace Capabilities
  {

    // Capabilities from dune-grid
    // ---------------------------

    template< class HostGrid >
    struct hasSingleGeometryType< FilteredGrid< HostGrid > >
    {
      static const bool v = hasSingleGeometryType< HostGrid >::v;
      static const unsigned int topologyId = hasSingleGeometryType< HostGrid >::topologyId;
    };


    template< class HostGrid >
    struct isCartesian< FilteredGrid< HostGrid > >
    {
      static const bool v = isCartesian< HostGrid >::v;
    };


    template< class HostGrid, int codim >
    struct hasEntity< FilteredGrid< HostGrid >, codim >
    {
      static const bool v = hasEntity< HostGrid, codim >::v;
    };


    template< class HostGrid >
    struct isParallel< FilteredGrid< HostGrid > >
    {
      static const bool v = isParallel< HostGrid >::v;
    };


    template< class HostGrid, int codim >
    struct canCommunicate< FilteredGrid< HostGrid >, codim >
    {
      static const bool v = canCommunicate< HostGrid, codim >::v;
    };


    template< class HostGrid >
    struct hasBackupRestoreFacilities< FilteredGrid< HostGrid > >
    {
      static const bool v = false; // hasBackupRestoreFacilities< HostGrid >::v;
    };


    template< class HostGrid >
    struct isLevelwiseConforming< FilteredGrid< HostGrid > >
    {
      static const bool v = isLeafwiseConforming< HostGrid >::v;
    };


    template< class HostGrid >
    struct isLeafwiseConforming< FilteredGrid< HostGrid > >
    {
      static const bool v = isLeafwiseConforming< HostGrid >::v;
    };


    template< class HostGrid >
    struct threadSafe< FilteredGrid< HostGrid > >
    {
      static const bool v = false;
    };


    template< class HostGrid >
    struct viewThreadSafe< FilteredGrid< HostGrid > >
    {
      static const bool v = false;
    };

  } // namespace Capabilities

} // namespace Dune

#endif // #ifndef DUNE_GRID_FILTEREDGRID_CAPABILITIES_HH
