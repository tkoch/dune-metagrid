#ifndef DUNE_GRID_FILTEREDGRID_INTERSECTION_HH
#define DUNE_GRID_FILTEREDGRID_INTERSECTION_HH

//- C++ includes
#include <cassert>

//- dune-common includes
#include <dune/common/nullptr.hh>
#include <dune/common/typetraits.hh>

//- dune-grid includes
#include <dune/grid/common/intersection.hh>


namespace Dune
{

  // FilteredIntersection
  // --------------------

  template< class Grid >
  class FilteredIntersection
  {
    typedef FilteredIntersection< Grid > This;

    typedef typename remove_const< Grid >::type::Traits Traits;

  public:
    /** \brief host grid view type */
    typedef typename Traits::HostGridView HostGridView;

    /** \brief entity type */
    typedef typename Traits::template Codim< 0 >::Entity Entity;

    /** \brief entity pointer type */
    typedef typename Traits::template Codim< 0 >::EntityPointer EntityPointer;

    /** \brief type of host intersecton */
    typedef typename HostGridView::Intersection HostIntersection;

    /** \brief geometry type */
    typedef typename HostIntersection::Geometry Geometry;
    /** \brief local geometry type */
    typedef typename HostIntersection::LocalGeometry LocalGeometry;
    /** \brief local coordinate type */
    typedef typename HostIntersection::LocalCoordinate LocalCoordinate;
    /** \brief global coordinate type */
    typedef typename HostIntersection::GlobalCoordinate GlobalCoordinate;

  private:
    typedef typename Traits::template Codim< 0 >::EntityPointerImpl EntityPointerImpl;

    typedef typename Traits::BoundarySegmentIndexSet BoundarySegmentIndexSet;

  public:
    typedef typename BoundarySegmentIndexSet::IndexType BoundarySegmentIndex;

    /** \brief constructor */
    FilteredIntersection ( const Grid &grid, const BoundarySegmentIndex *boundarySegmentIndices )
    : grid_( &grid ),
      boundarySegmentIndices_( boundarySegmentIndices ),
      hostIntersection_( nullptr )
    {}

    /** \brief return true, if intersection is at boundary */
    bool boundary () const
    {
      return (boundarySegmentIndices_[ indexInInside() ] != BoundarySegmentIndexSet::invalidIndex());
    }

    /** \brief return true, if we have a neighbor */
    bool neighbor () const
    {
      return (!boundary() && hostIntersection().neighbor());
    }

    /** \brief return boundaryId */
    int boundaryId () const { return (boundary() ? 1 : 0); }

    /** \brief return index for this intersection */
    std::size_t boundarySegmentIndex () const
    {
      assert( boundary() );
      return boundarySegmentIndices_[ indexInInside() ];
    }

    /** \brief return inside entity */
    EntityPointer inside () const
    {
      return EntityPointerImpl( hostIntersection().inside(), grid() );
    }

    /** \brief return outside entity */
    EntityPointer outside () const
    {
      assert( neighbor() );
      return EntityPointerImpl( hostIntersection().outside(), grid() );
    }

    /** \brief return true if this intersection is conforming */
    bool conforming () const { return boundary() || hostIntersection().conforming(); }

    LocalGeometry geometryInInside () const { return hostIntersection().geometryInInside(); }
    LocalGeometry geometryInOutside () const { return hostIntersection().geometryInOutside(); }

    Geometry geometry () const { return hostIntersection().geometry(); }

    GeometryType type () const { return hostIntersection().type(); }

    int indexInInside () const { return hostIntersection().indexInInside(); }
    int indexInOutside () const { return hostIntersection().indexInOutside(); }

    GlobalCoordinate outerNormal ( const LocalCoordinate &local ) const
    {
      return hostIntersection().outerNormal( local );
    }

    GlobalCoordinate integrationOuterNormal ( const LocalCoordinate &local ) const
    {
      return hostIntersection().integrationOuterNormal( local );
    }

    GlobalCoordinate unitOuterNormal ( const LocalCoordinate &local ) const
    {
      return hostIntersection().unitOuterNormal( local );
    }

    GlobalCoordinate centerUnitOuterNormal () const
    {
      return hostIntersection().centerUnitOuterNormal( );
    }

  public:
    operator bool () const { return hostIntersection_; }

    /** \brief set internal pointer to host intersection */
    void reset ( const HostIntersection &hostIntersection )
    {
      hostIntersection_ = &hostIntersection;
    }

    /** \brief invalidate intersection after iterator increment */
    void invalidate () { hostIntersection_ = nullptr; }

    /** \brief return grid */
    const Grid &grid () const { return *grid_; }

    const HostIntersection &hostIntersection () const
    {
      assert( hostIntersection_ );
      return *hostIntersection_;
    }

  private:
    const Grid *grid_;
    const BoundarySegmentIndex *boundarySegmentIndices_;
    const HostIntersection *hostIntersection_;
  };

} // namespace Dune

#endif // #ifndef DUNE_GRID_FILTEREDGRID_INTERSECTION_HH
