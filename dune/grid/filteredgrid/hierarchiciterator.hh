#ifndef DUNE_GRID_FILTEREDGRID_HIERARCHICITERATOR_HH
#define DUNE_GRID_FILTEREDGRID_HIERARCHICITERATOR_HH

#include <dune/grid/common/entityiterator.hh>

#include <dune/grid/filteredgrid/entitypointer.hh>


namespace Dune
{

  // FilteredHierarchicIterator
  // --------------------------

  template< class Grid >
  class FilteredHierarchicIterator
  : public FilteredEntityPointer< Grid, typename remove_const< Grid >::type::Traits::HostGrid::HierarchicIterator >
  {
    typedef FilteredHierarchicIterator< Grid > This;
    typedef FilteredEntityPointer< Grid, typename remove_const< Grid >::type::Traits::HostGrid::HierarchicIterator > Base;

    typedef typename remove_const< Grid >::type::Traits::HostGrid::HierarchicIterator HostIterator;

  public:
    FilteredHierarchicIterator ( const HostIterator &hostIterator, const Grid &grid )
    : Base( hostIterator, grid )
    {}

    /** \brief increment */
    void increment ()
    {
      DUNE_THROW( InvalidStateException, "Trying to increment hierarchic iterator." );
    }
  };

} // namespace Dune

#endif // #ifndef DUNE_GRID_FILTEREDGRID_HIERARCHICITERATOR_HH
