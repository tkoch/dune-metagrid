#ifndef DUNE_FILTEREDGRID_DATAHANDLE_HH
#define DUNE_FILTEREDGRID_DATAHANDLE_HH

//- C++ includes
#include <cassert>

//- dune-common includes
#include <dune/common/typetraits.hh>

//- dune-grid includes
#include <dune/grid/common/datahandleif.hh>


namespace Dune
{

  // FilteredGridDataHandle
  // ----------------------

  template< class WrappedHandle, class Grid >
  class FilteredGridDataHandle
  : public CommDataHandleIF< FilteredGridDataHandle< WrappedHandle, Grid >, typename WrappedHandle::DataType >
  {
    typedef FilteredGridDataHandle< WrappedHandle, Grid > This;

    // type of traits
    typedef typename remove_const< Grid >::type::Traits Traits;

    template< int codim >
    struct Codim
    {
      // type of entity
      typedef typename Traits::template Codim< codim >::Entity Entity;
    };

  public:
    // type of data to be communicated
    typedef typename WrappedHandle::DataType DataType;

    typedef CommDataHandleIF< This, DataType > DataHandleIF;

  private:
    // prohibit copying
    FilteredGridDataHandle ( const This & );

  public:
    FilteredGridDataHandle ( WrappedHandle &wrappedHandle, const Grid &grid )
    : wrappedHandle_( wrappedHandle ),
      grid_( grid )
    {}

    bool contains ( int dim, int codim ) const
    {
      return wrappedHandle_.contains( dim, codim );
    }

    bool fixedsize ( int dim, int codim ) const
    {
      return wrappedHandle_.fixedsize( dim, codim );
    }

    template< class HostEntity >
    size_t size ( const HostEntity &hostEntity ) const
    {
      typedef typename Codim< HostEntity::codimension >::Entity Entity;
      if( grid_.contains( hostEntity ) )
      {
        const Entity entity( typename Entity::Implementation( hostEntity, grid_ ) );
        return wrappedHandle_.size( entity );
      }
      else
        return 0;
    }

    template< class MessageBuffer, class HostEntity >
    void gather ( MessageBuffer &buffer, const HostEntity &hostEntity ) const
    {
      typedef typename Codim< HostEntity::codimension >::Entity Entity;
      if( grid_.contains( hostEntity ) )
      {
        const Entity entity( typename Entity::Implementation( hostEntity, grid_ ) );
        wrappedHandle_.gather( buffer, entity );
      }
    }

    template< class MessageBuffer, class HostEntity >
    void scatter ( MessageBuffer &buffer, const HostEntity &hostEntity, size_t size )
    {
      typedef typename Codim< HostEntity::codimension >::Entity Entity;
      if( grid_.contains( hostEntity ) )
      {
        const Entity entity( typename Entity::Implementation( hostEntity, grid_ ) );
        wrappedHandle_.scatter( buffer, entity, size );
      }
      else
        assert( size == size_t( 0 ) );
    }

  private:
    WrappedHandle &wrappedHandle_;
    const Grid &grid_;
  };



  // FilteredGridWrappedDofManager
  // -----------------------------

  template< class WrappedDofManager, class Grid >
  class FilteredGridWrappedDofManager
  {
    typedef FilteredGridWrappedDofManager< WrappedDofManager, Grid > This;

    typedef typename remove_const< Grid >::type::Traits Traits;

    // type of element (i.e., entity of codimension 0)
    typedef typename Traits::template Codim< 0 >::Entity Element;

    // type of host element (i.e., host entity of codimension 0)
    typedef typename Traits::HostGrid::template Codim< 0 >::Entity HostElement;

  private:
    // prohibit copy constructor
    FilteredGridWrappedDofManager ( const This & );

  public:
    FilteredGridWrappedDofManager ( WrappedDofManager &wrappedDofManager, const Grid &grid )
    : wrappedDofManager_( wrappedDofManager ),
      grid_( grid )
    {}

    template< class MessageBuffer >
    void inlineData ( MessageBuffer &buffer, const HostElement &hostElement )
    {
      if( grid_.contains( hostElement ) )
      {
        const Element element( typename Element::Implementation( hostElement, grid_ ) );
        wrappedDofManager_.inlineData( buffer, element );
      }
    }

    template< class MessageBuffer >
    void xtractData ( MessageBuffer &buffer, const HostElement &hostElement, std::size_t newElements )
    {
      if( grid_.contains( hostElement ) )
      {
        const Element element( typename Element::Implementation( hostElement, grid_ ) );
        wrappedDofManager_.xtractData( buffer, element, newElements );
      }
    }

    void compress () { wrappedDofManager_.compress(); }

  private:
    WrappedDofManager &wrappedDofManager_;
    const Grid &grid_;
  };

} // namespace Dune

#endif // #ifndef DUNE_FILTEREDGRID_DATAHANDLE_HH
