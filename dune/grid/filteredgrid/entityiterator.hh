#ifndef DUNE_GRID_FILTEREDGRID_ITERATOR_HH
#define DUNE_GRID_FILTEREDGRID_ITERATOR_HH

//- system includes
#include <cassert>

//- dune-common includes
#include <dune/common/typetraits.hh>

//- dune-grid includes
#include <dune/grid/common/gridenums.hh>

//- dune-metagrid includes
#include <dune/grid/filteredgrid/entitypointer.hh>

namespace Dune
{

  // FilteredEntityIterator
  // ----------------------

  template< class Grid, class HostIterator >
  class FilteredEntityIterator
  : public FilteredEntityPointer< Grid, HostIterator >
  {
    typedef FilteredEntityIterator< Grid, HostIterator > This;
    typedef FilteredEntityPointer< Grid, HostIterator > Base;

  public:
    FilteredEntityIterator ( const HostIterator &hostIterator, const HostIterator &hostEnd, const Grid &grid )
    : Base( hostIterator, grid ),
      hostEnd_( hostEnd )
    {
      if( proceed() )
        increment();
    }

    /** \brief increment */
    void increment ()
    {
      assert( !done() );
      do
      {
        ++hostIterator_;
      } while( proceed() );
      releaseEntity();
    }

  protected:
    using Base::grid;
    using Base::hostIterator_;
    using Base::releaseEntity;

  private:
    bool proceed () const { return (!done() && !(grid().contains( *hostIterator_ ))); }
    bool done () const { return (hostIterator_ == hostEnd_); }

    HostIterator hostEnd_;
  };

} // namespace Dune

#endif // #ifndef DUNE_GRID_FILTEREDGRID_ITERATOR_HH
