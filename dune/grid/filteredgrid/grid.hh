#ifndef DUNE_GRID_FILTEREDGRID_GRID_HH
#define DUNE_GRID_FILTEREDGRID_GRID_HH

//- C++ includes
#include <cassert>
#include <vector>

//- dune-common includes
#include <dune/common/exceptions.hh>

//- dune-grid includes
#include <dune/grid/common/grid.hh>
#include <dune/grid/utility/grapedataioformattypes.hh>

//- dune-metagrid includes
#include <dune/grid/common/hostgridinoutstreams.hh>
#include <dune/grid/filteredgrid/capabilities.hh>
#include <dune/grid/filteredgrid/declaration.hh>
#include <dune/grid/filteredgrid/gridfamily.hh>


namespace Dune
{

  // FilteredGrid
  // ------------

  template< class HG >
  class FilteredGrid
  : public GridDefaultImplementation< HG::dimension, HG::dimensionworld, typename HG::ctype, FilteredGridFamily< HG > >,
    public HostGridHasInOutStreams< HG, Conversion< HG, HasObjectStream >::exists >
  {
    typedef FilteredGrid< HG > This;
    typedef GridDefaultImplementation< HG::dimension, HG::dimensionworld, typename HG::ctype, FilteredGridFamily< HG > > Base;

    template< class, class > class FilteredEntityPointer;
    template< PartitionIteratorType, class > friend class FilteredIndexSet;
    template< class, class > friend class IdGridIdSet;

  public:
    /** \brief grid family */
    typedef FilteredGridFamily< HG > GridFamily;

    /** \brief traits */
    typedef typename GridFamily::Traits Traits;

    /** \brief host grid type */
    typedef typename Traits::HostGrid HostGrid;
    /** \brief host grid view */
    typedef typename Traits::HostGridView HostGridView;

    /** \brief grid dimension */
    static const int dimension = Traits::dimension;
    /** \brief world dimension */
    static const int dimensionworld = Traits::dimensionworld;
    /** \brief single coordinate type */
    typedef typename Traits::ctype ctype;

    /** \brief level index set */
    typedef typename Traits::LevelIndexSet LevelIndexSet;
    /** \brief leaf index set */
    typedef typename Traits::LeafIndexSet LeafIndexSet;

    /** \brief global id set */
    typedef typename Traits::GlobalIdSet GlobalIdSet;
    /** \brief local id set */
    typedef typename Traits::LocalIdSet LocalIdSet;
    /** \brief collective communication */
    typedef typename Traits::CollectiveCommunication CollectiveCommunication;

    template< int codim >
    struct Codim
    : public Traits::template Codim< codim >
    {};

    template< PartitionIteratorType pitype >
    struct Partition
    : public Traits::template Partition< pitype >
    {};

    /** \brief level grid view */
    typedef typename Partition< All_Partition >::LevelGridView LevelGridView;
    /** \brief leaf grid view */
    typedef typename Partition< All_Partition >::LeafGridView LeafGridView;

    /** \brief type of boundary segment index set */
    typedef typename Traits::BoundarySegmentIndexSet BoundarySegmentIndexSet;

  public:
    /** \brief constructor */
    template< class Predicate >
    FilteredGrid ( HostGrid &hostGrid, const Predicate &predicate )
    : hostGrid_( hostGrid ),
      hostGridView_( hostGrid_.leafGridView() ),
      leafIndexSet_( hostGridView_, predicate ),
      boundarySegmentIndexSet_( hostGridView_, leafIndexSet_ )
    {}

    /** \brief return max level */
    int maxLevel () const { return 0; }

    /** \brief return size per level and codim */
    int size ( int level, int codim ) const
    {
      assert( level == 0 );
      return size( codim );
    }

    /** \brief return leaf grid view size per codim */
    int size ( int codim ) const
    {
      return leafIndexSet().size( codim );
    }

    /** \brief return size per level and geometry type */
    int size ( int level, GeometryType type ) const
    {
      assert( level == 0 );
      return size( type );
    }

    /** \brief return leaf grid view size per geometry type */
    int size ( GeometryType type ) const
    {
      return leafIndexSet().size( type );
    }

    /** \brief return number of boundary segments */
    size_t numBoundarySegments () const
    {
      return boundarySegmentIndexSet().size();
    }

    /** \brief return level iterator */
    template< int codim >
    typename Codim< codim >::template Partition< All_Partition >::LevelIterator
    lbegin ( int level ) const
    {
      return lbegin< codim, All_Partition >( level );
    }

    /** \brief return level iterator */
    template< int codim, PartitionIteratorType pitype >
    typename Codim< codim >::template Partition< pitype >::LevelIterator
    lbegin ( int level ) const
    {
      assert( level == 0 );
      return leafbegin< codim, pitype >();
    }

    /** \brief return level end iterator */
    template< int codim >
    typename Codim< codim >::template Partition< All_Partition >::LevelIterator
    lend ( int level ) const
    {
      return lend< codim, All_Partition >( level );
    }

    /** \brief return level end iterator */
    template< int codim, PartitionIteratorType pitype >
    typename Codim< codim >::template Partition< pitype >::LevelIterator
    lend ( int level ) const
    {
      assert( level == 0 );
      return leafend< codim, pitype >();
    }

    /** \brief return leaf level iterator */
    template< int codim >
    typename Codim< codim >::template Partition< All_Partition >::LeafIterator
    leafbegin () const
    {
      return leafbegin< codim, All_Partition >();
    }

    /** \brief return leaf level iterator */
    template< int codim, PartitionIteratorType pitype >
    typename Codim< codim >::template Partition< pitype >::LeafIterator
    leafbegin () const
    {
      typedef typename Codim< codim >::template Partition< pitype >::IteratorImpl IteratorImpl;
      return IteratorImpl( hostGridView().template begin< codim, pitype >(), hostGridView().template end< codim, pitype >(), *this );
    }

    /** \brief return leaf level end iterator */
    template< int codim >
    typename Codim< codim >::template Partition< All_Partition >::LeafIterator
    leafend () const
    {
      return leafend< codim, All_Partition >();
    }

    /** \brief return leaf level end iterator */
    template< int codim, PartitionIteratorType pitype >
    typename Codim< codim >::template Partition< pitype >::LeafIterator
    leafend () const
    {
      typedef typename Codim< codim >::template Partition< pitype >::IteratorImpl IteratorImpl;
      return IteratorImpl( hostGridView().template end< codim, pitype >(), hostGridView().template end< codim, pitype >(), *this );
    }

    /** \brief return global id set */
    const GlobalIdSet &globalIdSet () const
    {
      if( !globalIdSet_ )
        globalIdSet_ = GlobalIdSet( hostGrid().globalIdSet() );
      return globalIdSet_;
    }

    /** \brief return local id set */
    const LocalIdSet &localIdSet () const
    {
      if( !localIdSet_ )
        localIdSet_ = LocalIdSet( hostGrid().localIdSet() );
      return localIdSet_;
    }

    /** \brief return leaf index set */
    const LeafIndexSet &leafIndexSet () const
    {
      return leafIndexSet_;
    }

    /** \brief return level index set */
    const LevelIndexSet &levelIndexSet ( int level ) const
    {
      assert( level == 0 );
      return leafIndexSet();
    }

    /** \brief refine mesh globally by one refcount levels */
    void globalRefine ( int refCount ) {}

    /** \brief marks an entity to be refined/coarsened in a subsequent adapt */
    bool mark ( int refCount, const typename Codim< 0 >::Entity &entity )
    {
      return false;
    }

    /** \brief returns adaptation mark for given entity */
    int getMark ( const typename Codim< 0 >::Entity &entity ) const { return 0; }

    /** \brief to be called after entities have been marked and
     *         before adapt() is called.
     */
    bool preAdapt () { return false; }

    /** \brief refine all positive marked leaf entities, coarsen
     *         all negative marked entities if possible.
     */
    bool adapt () { return false; }

    /** \brief to be called after grid has been adapted and information
     *         left over by the adaptation has been processed.
     */
    void postAdapt () {}

    /** \brief return size of overlap for a given codim on a given level */
    int overlapSize ( int level, int codim ) const
    {
      assert( level == 0 );
      return overlapSize( codim );
    }

    /** \brief return size of overlap region for a given codim on the leaf grid */
    int overlapSize ( int codim ) const
    {
      DUNE_THROW( NotImplemented, "Method overlapSize() not implemented yet" );
    }

    /** \brief return size of ghost region for a given codim on a given level */
    int ghostSize ( int level, int codim ) const
    {
      assert( level == 0 );
      return ghostSize( codim );
    }

    /** \brief return size of ghost region for a given codim on the leaf grid */
    int ghostSize ( int codim ) const
    {
      DUNE_THROW( NotImplemented, "Method ghostSize() not implemented yet" );
    }

    /** \brief communicate information on distributed entities on a given level */
    template< class DataHandle, class Data >
    void communicate ( CommDataHandleIF< DataHandle, Data > &dataHandle,
                       InterfaceType iftype,
                       CommunicationDirection dir,
                       int level ) const
    {
      levelGridView( level ).communicate( dataHandle, iftype, dir );
    }

    /** \brief communicate information on distributed entities on the leaf grid */
    template< class DataHandle, class Data >
    void  communicate ( CommDataHandleIF< DataHandle, Data > &dataHandle,
                        InterfaceType iftype,
                        CommunicationDirection dir ) const
    {
      leafGridView().communicate( dataHandle, iftype, dir );
    }

    /** \brief call comm on host grid */
    const CollectiveCommunication &comm () const { return hostGrid().comm(); }

    /** \brief re-balances the load each process has to handle for a parallel grid */
    bool loadBalance () { return hostGrid().loadBalance(); }

    /** \brief re-balances the load each process has to handle for a parallel grid */
    template< class DataHandle, class Data >
    bool loadBalance ( CommDataHandleIF< DataHandle, Data > &dataHandle )
    {
      typedef CommDataHandleIF< DataHandle, Data > HostDataHandle;
      FilteredGridDataHandle< HostDataHandle, This > dataHandleWrapper( dataHandle, *this );
      return hostGrid().loadBalance( dataHandleWrapper );
    }


    /** \brief re-balances the load each process has to handle for a parallel grid */
    template< class DofManager >
    bool loadBalance ( DofManager &dofManager )
    {
      FilteredGridWrappedDofManager< DofManager, This > dofManagerWrapper( dofManager, *this );
      return hostGrid().loadBalance( dofManagerWrapper );
    }

    /** \brief View for a grid level */
    template< PartitionIteratorType pitype >
    typename Partition< pitype >::LevelGridView levelGridView ( int level ) const
    {
      assert( level == 0 );
      return leafGridView< pitype >();
    }

    /** \brief View for the leaf grid */
    template< PartitionIteratorType pitype >
    typename Partition< pitype >::LeafGridView leafGridView () const
    {
      typedef typename Traits::template Partition< pitype >::LeafGridView View;
      typedef typename View::GridViewImp ViewImp;
      return View( ViewImp( *this, hostGrid().leafGridView() ) );
    }

    /** \brief View for a grid level for All_Partition */
    LevelGridView levelGridView ( int level ) const
    {
      assert( level == 0 );
      return leafGridView();
    }

    /** \brief View for the leaf grid for All_Partition */
    LeafGridView leafGridView () const
    {
      typedef typename LeafGridView::GridViewImp ViewImp;
      return LeafGridView( ViewImp( *this, hostGrid().leafGridView() ) );
    }

    /** \brief return entity pointer */
    template< class EntitySeed >
    typename Codim< EntitySeed::codimension >::EntityPointer
    entityPointer ( const EntitySeed &seed ) const
    {
      typedef typename Codim< EntitySeed::codimension >::EntityPointer::Implementation Implementation;
      return Implementation( hostGrid().entityPointer( seed ), extraData() );
    }

    template < GrapeIOFileFormatType ftype >
    bool writeGrid ( const std::string filename, ctype time ) const
    {
      DUNE_THROW( NotImplemented, "Method writeGrid() not implemented yet" );
    }

    template < GrapeIOFileFormatType ftype >
    bool readGrid ( const std::string filename, ctype &time )
    {
      DUNE_THROW( NotImplemented, "Method readGrid() not implemented yet" );
    }

  public:
    /** \brief return const reference to host grid */
    const HostGrid &hostGrid () const
    {
      return hostGrid_;
    }

    /** \brief return host grid view */
    const HostGridView hostGridView () const
    {
      return hostGridView_;
    }

    /** \brief convenience method -- calls contains() on index set */
    template< class Entity >
    bool contains ( const Entity &entity ) const
    {
      assert( hostGridView().indexSet().contains( entity ) );
      return leafIndexSet().contains( entity );
    }

    /** \brief return boundary segment index set */
    const BoundarySegmentIndexSet &boundarySegmentIndexSet () const
    {
      return boundarySegmentIndexSet_;
    }

  protected:
    typedef typename Traits :: ExtraData ExtraData;
    //! return extra data passed to entities (here const Grid*)
    ExtraData extraData() const  {  return *this;  }

    using Base::getRealImplementation;

    template< int codim >
    static const typename HostGrid::template Codim< codim >::Entity &
    getHostEntity( const typename Codim< codim >::Entity &entity )
    {
      return getRealImplementation( entity ).hostEntity();
    }

  private:
    // prohibit copying and assignment
    FilteredGrid ( const This & );
    const This &operator = ( const This & );

    HostGrid &hostGrid_;
    HostGridView hostGridView_;

    LeafIndexSet leafIndexSet_;
    BoundarySegmentIndexSet boundarySegmentIndexSet_;

    mutable GlobalIdSet globalIdSet_;
    mutable LocalIdSet localIdSet_;
  };

} // namespace Dune

#endif // #ifndef DUNE_GRID_FILTEREDGRID_GRID_HH
