#ifndef DUNE_CARTESIANGRID_DECLARATION_HH
#define DUNE_CARTESIANGRID_DECLARATION_HH

namespace Dune
{

  template< class HostGrid >
  class CartesianGrid;

} // namespace Dune

#endif // #ifndef DUNE_CARTESIANGRID_DECLARATION_HH
