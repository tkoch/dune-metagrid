#ifndef DUNE_CARTESIANGRID_BACKUPRESTORE_HH
#define DUNE_CARTESIANGRID_BACKUPRESTORE_HH

#include <fstream>

#include <dune/grid/common/forwardbackuprestore.hh>
#include <dune/grid/cartesiangrid/capabilities.hh>

namespace Dune
{
  /** \copydoc Dune::BackupRestoreFacility */
  template< class HostGrid >
  struct BackupRestoreFacility< CartesianGrid< HostGrid > >
    : public ForwardBackupRestoreFacility< CartesianGrid< HostGrid > >
  {
  };

} // namespace Dune

#endif // #ifndef DUNE_CARTESIANGRID_BACKUPRESTORE_HH
