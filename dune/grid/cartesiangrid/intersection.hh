#ifndef DUNE_CARTESIANGRID_INTERSECTION_HH
#define DUNE_CARTESIANGRID_INTERSECTION_HH

#include <dune/grid/cartesiangrid/declaration.hh>
#include <dune/grid/cartesiangrid/geometry.hh>
#include <dune/grid/cartesiangrid/hostgridinfo.hh>
#include <dune/grid/idgrid/intersection.hh>

#if HAVE_DUNE_SPGRID

namespace Dune
{

  // CartesianGridIntersection
  // -------------------------

  template< class Grid, class HostIntersection >
  class CartesianGridIntersection : public IdGridIntersection< Grid, HostIntersection >
  {
    typedef CartesianGridIntersection< Grid, HostIntersection > This;
    typedef IdGridIntersection< Grid, HostIntersection >        Base;

    typedef typename HostIntersection::Geometry HostGeometry;

    typedef typename remove_const< Grid >::type::Traits Traits;

  protected:
    typedef typename Traits::HostGrid  HostGrid;
    typedef typename Traits::ExtraData ExtraData;

    typedef CartesianGridHostGridInfo< HostGrid > HostGridInfo;

  public:
    typedef typename Traits::ctype ctype;

    static const int dimension = Traits::dimension;
    static const int dimensionworld = Traits::dimensionworld;
    static const int mydimension = dimension-1;

    typedef typename Traits::template Codim< 0 >::Entity Entity;
    typedef typename Traits::template Codim< 1 >::Geometry Geometry;
    typedef typename Traits::template Codim< 1 >::LocalGeometry LocalGeometry;

    typedef SPNormalVector< ctype, dimension > NormalVector;

    using Base :: data ;
    using Base :: neighbor ;
    using Base :: indexInInside ;
    using Base :: hostIntersection ;

  protected:

    typedef CartesianGridGeometry< mydimension, dimensionworld, Grid > GeometryImpl;
    typedef typename remove_const< Grid >::type::GridLevel GridLevel;

  public:
    explicit CartesianGridIntersection ()
    : Base( ),
      insideLevel_( -1 ),
      outsideLevel_( -1 ),
      gridLevel_( nullptr )
    {}

    explicit CartesianGridIntersection ( ExtraData data )
    : Base( data ),
      insideLevel_( -1 ),
      outsideLevel_( -1 ),
      gridLevel_( &(data->geometricLevel( 0 )) )
    {}

    CartesianGridIntersection ( ExtraData data, const HostIntersection &hostIntersection )
    : Base( data, hostIntersection ),
      insideLevel_( HostGridInfo::insideLevel( hostIntersection ) ),
      outsideLevel_( HostGridInfo::outsideLevel( hostIntersection, insideLevel_ ) ),
      gridLevel_( &(data->geometricLevel( level()) ) )
    {}

    bool conforming () const
    {
      assert( !neighbor() || ((insideLevel_ == outsideLevel_) == hostIntersection().conforming()) );
      return hostIntersection().conforming();
    }

    LocalGeometry geometryInInside () const
    {
      return localGeometry( indexInInside(),
                            HostGridInfo::childIndexInInside( hostIntersection(),
                                                              insideLevel_,
                                                              outsideLevel_ ) );
    }

    LocalGeometry geometryInOutside () const
    {
      return localGeometry( indexInOutside(),
                            HostGridInfo::childIndexInOutside( hostIntersection(),
                                                               insideLevel_,
                                                               outsideLevel_ ) );
    }

    Geometry geometry () const
    {
      return Geometry( GeometryImpl( gridLevel(),
                                     HostGridInfo::direction( indexInInside(), dimension ),
                                     HostGridInfo::originIntersection( hostIntersection() ) ) );
    }

    int indexInOutside () const
    {
      assert( hostIntersection().indexInOutside() == (indexInInside()^1) );
      return hostIntersection().indexInOutside();
    }

    NormalVector
    integrationOuterNormal ( const FieldVector< ctype, dimension-1 > &local ) const
    {
      return gridLevel().faceVolume( indexInInside() ) * centerUnitOuterNormal();
    }

    NormalVector
    outerNormal ( const FieldVector< ctype, dimension-1 > &local ) const
    {
      return centerUnitOuterNormal();
    }

    NormalVector
    unitOuterNormal ( const FieldVector< ctype, dimension-1 > &local ) const
    {
      return centerUnitOuterNormal();
    }

    NormalVector centerUnitOuterNormal () const
    {
      return NormalVector( SPNormalId< dimension >( indexInInside() ) );
    }

    const GridLevel &gridLevel () const
    {
      assert( gridLevel_ );
      return *gridLevel_;
    }

  protected:
    int level () const
    {
      return std::max( insideLevel_, outsideLevel_ );
    }

    LocalGeometry localGeometry ( int faceIndex, int childIndex ) const
    {
      if( childIndex < 0 )
        return LocalGeometry( *(data()->localFaceGeometry_[ faceIndex ]) );
      else
      {
        const int index = faceIndex*(1 << mydimension) + childIndex;
        return LocalGeometry( *(data()->localRefinedFaceGeometry_[ index ]) );
      }
    }

    int insideLevel_, outsideLevel_;
    const GridLevel *gridLevel_;
  };

} // namespace Dune

#endif // #if HAVE_DUNE_SPGRID

#endif // #ifndef DUNE_CARTESIANGRID_INTERSECTION_HH
